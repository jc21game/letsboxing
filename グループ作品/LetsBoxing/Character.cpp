#include "Character.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"
#include "PlayScene.h"
#include <time.h>
#include "NonPlayerCharacter.h"
#include "PlayerCharacter.h"
#include <math.h>
#include "Engine/Direct3D.h"
#include "Engine/Model.h"
#include "Engine/Audio.h"
#include <string>

//#define PUNCH_COLLIDER_MOVE 0.5
//#define PUNCH_COLLIDER_MOVE_MAX 1
//#define PUNCH_COLLIDER_INIT_POS 0.5

#define PUNCH_COLLIDER_MOVE 4
#define PUNCH_COLLIDER_MOVE_MAX 8
#define PUNCH_COLLIDER_INIT_POS 1

#define PUNCH_DAMAGE 2
#define STRONG_PUNCH_DAMAGE 4 
#define COUNTER_DAMAGE 2

#define HIT_DISTANCE 7 //2

#define WALL_POSITION 27

//MAYAのモーション定数
enum MOTION_STANCE
{
	M_UP,
	M_DOWN,
	M_STANCE_MAX
};

enum MOTION_TIME
{
	M_START,
	M_END,
	M_TIME_MAX
};

const float M_STANDARD[M_STANCE_MAX][M_TIME_MAX] = { {0.0f, 59.0f} , {70.0f, 129.0f} };
const float M_PUNCH[M_STANCE_MAX][M_TIME_MAX] = { {140.0f, 199.0f} , {210.0f, 269.0f} };
const float M_CHARG[M_STANCE_MAX][M_TIME_MAX] = { {280.0f, 339.0f} , {350.0f, 409.0f} };
const float M_STRONG_PUNCH[M_STANCE_MAX][M_TIME_MAX] = { {420.0f, 479.0f} , {490.0f, 549.0f} };
const float M_COUNTER[M_TIME_MAX] = {560.0f, 619.0f};
const float M_WEAK[M_TIME_MAX] = {630.0f, 689.0f};
const float M_COUNTER_PUNCH[M_STANCE_MAX][M_TIME_MAX] = { {700.0f, 759.0f} , {770.0f, 829.0f} };
const float M_KNOCK_BACK[M_TIME_MAX] = {840.0f, 899.0f};
const float M_MOVE_FRONT[M_STANCE_MAX][M_TIME_MAX] = { {910.0f, 969.0f} , {1190.0f, 1249.0f} };
const float M_MOVE_BACK[M_STANCE_MAX][M_TIME_MAX] = { {980.0f, 1039.0f} , {1260.0f, 1319.0f} };
const float M_MOVE_LEFT[M_STANCE_MAX][M_TIME_MAX] = { {1050.0f, 1109.0f} , {1330.0f, 1389.0f} };
const float M_MOVE_RIGHT[M_STANCE_MAX][M_TIME_MAX] = { {1120.0f, 1179.0f} , {1400.0f, 1459.0f} };
const float M_ROUND_GET[M_TIME_MAX] = {1470.0f, 1529.0f};
const float M_GAME_WIN[M_TIME_MAX] = {1540.0f, 1599.0f};

//余分なフレームの数
//これはモーションを止める際に使用される
const float EXTRA_FRAME = 10.0f;

//パンチした後のフレームに使う
const float PUNCH_AFTER_FRAME = 1.0f;

//MAYAのモーションスピード定数
const float M_BASIC_SPEED = 2.0f;
const float M_CHARG_SPEED = 0.55f;

//コンストラクタ
//名前を付けないと名前がCharacterになった状態で下のコンストラクタが呼ばれる
Character::Character(IGameObject * parent)
	:Character(parent, "Character")
{
}

Character::Character(IGameObject * parent, std::string name)
	: IGameObject(parent, name), hModel_(-1), hp_(0), pp_(0), invincibleModelTime(0), timeFlag_(false), time_(0), invincibleTimeFlag_(false), invincibleTime_(0),
	nextBehState_(STANDARD), nextStance_(DOWN), myState_(NORMAL), currentBehState_(STANDARD), currentStance_(DOWN), opponent_(""), moveFlag_(false), counterPunchFlag_(false),
	knockBackFlag_(false), positionCamera_(D3DXVECTOR3(10, 15, 5)), targetCamera_(D3DXVECTOR3(0, 10, 0))
{
	pPlayScene_ = (PlayScene*)pParent_;

	position_.y = POSITION_Y;
	position_.z = 15;

	//ボクサーの色
	string boxerColor;

	//iP側
	if (name_ == "PlayerCharacter")
	{
		//赤
		boxerColor = "Red";
	}
	//2P側
	else if (name_ == "NonPlayerCharacter")
	{
		//青
		boxerColor = "Blue";
	}

	hModel_ = Model::Load("data/Boxer_" + boxerColor + ".fbx");
	assert(hModel_ >= 0);

	Model::SetAnimFrame(hModel_, M_STANDARD[M_DOWN][M_START], M_STANDARD[M_DOWN][M_END], M_BASIC_SPEED);
}

//デストラクタ
Character::~Character()
{
}

//初期化
void Character::Initialize()
{
}

//更新
void Character::Update()
{
	//試合中
	if (pPlayScene_->GetSceneState() == STATE_PLAY)
	{
		//試合用の2画面用カメラを使用
		SetPlayCamera();
	}
	//カウントが1秒の際
	else if (pPlayScene_->GetStartCountProcess() == START_ONE)
	{
		//カウント1秒時の2画面用カメラを使用
		SetCountOneCamera();
	}

	//かどわき作：モデルのアニメーション
	ModelChangeActivity();


	//画面分割するための処理
	//if ((name_ == "PlayerCharacter") || (name_ == "NonPlayerCharacter"))
	//{
	//	CameraSet();
	//}

	//敵の方向を見る
	if (pPlayScene_->GetSceneState() == STATE_PLAY)
	{
		TurnForward((enemyCharacter_->GetPosition() - position_));
	}

	//下で動いていた場合moveFlag_をtrueにする処理をするため一旦ここでfalseにする
	moveFlag_ = false;

	//OnCollisionでstate_変更フラグが立っていた場合state_を変更する
	if (standardFlag_ == true)
	{
		standardFlag_ = false;
		nextBehState_ = STANDARD;
	}
	if (coolTimeFlag_ == true)
	{
		coolTimeFlag_ = false;
		nextBehState_ = COOLTIME;
	}
	if (counterPunchFlag_ == true)
	{
		counterPunchFlag_ = false;
		nextBehState_ = COUNTER_PUNCH;
	}
	if (knockBackFlag_ == true)
	{
		knockBackFlag_ = false;
		nextBehState_ = KNOCK_BACK;
	}

	//状態によって処理を分ける
	switch (nextBehState_)
	{
	case STANDARD:	//全ての行動ができる

		//試合中は下記の操作が可能
		if (pPlayScene_->GetSceneState() == STATE_PLAY)
		{
			PunchCommand();
			StrongPunchCommand();
			CounterCommand();
			StanceChangeCommand();
			MoveCommand();
		}
		break;

	case PUNCH:	//パンチ中はパンチが終わるまで他の行動ができない
		Punch();
		break;

	case CHARG:	//強攻撃のチャージ中はほかの行動ができない
		Charg();
		break;

	case STRONG_PUNCH:	//強攻撃中は他の行動ができない
		StrongPunch();
		break;

	case COOLTIME:	//攻撃のクールタイム中はカウンターと構えの上下と移動ができる
		CoolTime();

		//試合中は下記の操作が可能
		if (pPlayScene_->GetSceneState() == STATE_PLAY)
		{
			CounterCommand();
			StanceChangeCommand();
			MoveCommand();
		}
		break;

	case COUNTER:	//カウンター中は他の行動ができない
		Counter();
		break;

	case WEAK:	//カウンターが失敗すると他の行動ができない
		Weak();
		break;

	case COUNTER_PUNCH:	//カウンターパンチ中は他の行動ができない
		CoutnterPunch();
		break;

	case KNOCK_BACK:	//のけぞっている間は他の行動ができない
		KnockBack();
		break;
	}

	//無敵中
	if (myState_ == INVINCIBLE)
	{
		Invincible();
	}

	//ぶつかったときの処理
	returnPosition(enemyCharacter_->GetPosition(), enemyCharacter_->GetMoveFlag());

	if (hp_ <= 0)
	{
		KillMe();
	}
}


//描画
void Character::Draw()
{
	//無敵中
	if (myState_ == INVINCIBLE)
	{
		//点滅のためのタイムを増やす
		invincibleModelTime++;

		//割った時の余りが0以外の際に表示する
		//割る数字は、奇数じゃないと何故か表示がおかしくなる…。
		if (invincibleModelTime % 5 != 0)
		{
			Model::SetMatrix(hModel_, worldMatrix_);
			Model::Draw(hModel_);
		}
	}
	//それ以外は普通に表示
	else
	{
		Model::SetMatrix(hModel_, worldMatrix_);
		Model::Draw(hModel_);
	}

	//試合中ならHPが見えるようになる
	if (pPlayScene_->GetSceneState() == STATE_PLAY)
	{
		//継承先でoverrideして使う
		ChildDraw();
	}
}

//開放
void Character::Release()
{
}

void Character::OnCollision(IGameObject * pTarget)
{
	//当たったコライダーが対戦相手の持っているコライダーだった場合
	if (pTarget->GetName() == opponent_)
	{
		for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
		{
			//当たっているコライダーの場合
			if ((*itr)->GetHitCollider() != NULL)
			{
				//自分の攻撃コライダーが相手の胴体コライダーに当たった時
				//ここで自分のstate_や相手のstate_変更やダメージの処理などを全て行う
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				{
					//無敵状態ではない
					if (((Character*)pTarget)->GetMyState() != INVINCIBLE)
					{
						if (nextBehState_ == PUNCH)
						{
							switch (((Character*)pTarget)->GetBehaviorState())
							{
							case STANDARD:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case PUNCH:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								knockBackFlag_ = true;
								break;

							case CHARG:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);

								break;

							case STRONG_PUNCH:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case COOLTIME:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case COUNTER:
								hp_ -= COUNTER_DAMAGE;
								knockBackFlag_ = true;
								((Character*)pTarget)->SetCounterPunchFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case WEAK:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;
							}
						}

						else if (nextBehState_ == STRONG_PUNCH)
						{
							//再生
							Audio::Play("Kill");
							switch (((Character*)pTarget)->GetBehaviorState())
							{
							case STANDARD:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case PUNCH:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case CHARG:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case STRONG_PUNCH:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case COOLTIME:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case COUNTER:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case WEAK:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;
							}
							//pp0にする
							pp_ = 0;
						}
					}

					if (nextBehState_ != KNOCK_BACK)
					{
						coolTimeFlag_ = true;
					}
				}

				//本当は↓で攻撃を受けたときのstate_変更やダメージの処理をしようと思ったが上のやつをもう一回書かなきゃいけない
				////自分の胴体コライダーが相手の攻撃コライダーに当たった時
				//if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				//{
				//}

				//自分の攻撃コライダーが相手の攻撃コライダーに当たった時
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				{
					//攻撃コライダー同士が当たっている時はお互いのstate_がPUNCHかSTRONG_PUNCHの時だけ
					if (nextBehState_ == ((Character*)pTarget)->GetBehaviorState())

					{
						coolTimeFlag_ = true;
					}
					else if(nextBehState_ == STRONG_PUNCH && ((Character*)pTarget)->GetBehaviorState() == PUNCH)
					{
						((Character*)pTarget)->SetCoolTimeFlag(true);
					}
				}

				//通り抜けないようにする処理はUpDateでやることにしたのでここでの処理は必要ない
				////自分の胴体コライダーが相手の胴体コライダーに当たった時
				//if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				//{
				//	//通り抜けないようにする処理
				//}
			}
		}
	}
}

void Character::Punch()
{
	//攻撃用コライダーのオブジェクト作成　攻撃の初めに一回だけ処理される
	bool punchColliderCreateFlag_ = true;
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			punchColliderCreateFlag_ = false;
		}
	}
	if (punchColliderCreateFlag_)
	{
		//構えの位置によって攻撃の生成位置が変わる
		if (nextStance_ == UP)
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, 13/*2*/, 0), 1/*0.5f*/);
			AddCollider(punchCollider_);
		}
		else
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, 10/*-2*/, 0), 1/*0.5f*/);
			AddCollider(punchCollider_);
		}

		//攻撃用コライダーが作られた時にポジション用の変数も攻撃発動初期位置にする
		punchColliderForwardPosition_ = PUNCH_COLLIDER_INIT_POS;
	}

	//攻撃開始
	if (punchColliderForwardPosition_ <= PUNCH_COLLIDER_MOVE_MAX)
	{
		D3DXMATRIX m;
		punchColliderForwardPosition_ += PUNCH_COLLIDER_MOVE;

		D3DXMatrixRotationY(&m, D3DXToRadian(rotate_.y));
		//構えの位置によって攻撃の生成位置が変わる
		if (nextStance_ == UP)
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, 13/*2*/, punchColliderForwardPosition_), &m);
		}
		else
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, 10/*-2*/, punchColliderForwardPosition_), &m);
		}

		punchCollider_->SetCenter(punchColliderPosition_);
	}
	//攻撃終了
	else
	{
		//強攻撃だった場合ppを0にする
		if (nextBehState_ == STRONG_PUNCH)
		{
			pp_ = 0;
		}

		//クールタイム状態に変更
		nextBehState_ = COOLTIME;
	}
	
}

void Character::Charg()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1000)
	{
		nextBehState_ = STRONG_PUNCH;
		timeFlag_ = false;
	}
}

void Character::StrongPunch()
{
	Punch();
}

void Character::CoolTime()
{
	//攻撃用コライダーの削除
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end();)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			SAFE_DELETE((*itr));
			itr = ColliderList_.erase(itr);
			continue;
		}
		itr++;
	}

	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 250)
	{
		nextBehState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::Counter()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1500)
	{
		nextBehState_ = WEAK;
		timeFlag_ = false;
	}
}

void Character::Weak()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 500)
	{
		nextBehState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::StanceChange(STANCE stance)
{
	nextStance_ = stance;
}

void Character::CoutnterPunch()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 250)
	{
		nextBehState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::KnockBack()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 300)
	{
		nextBehState_ = STANDARD;
		myState_ = INVINCIBLE;
		timeFlag_ = false;
	}
}

void Character::Invincible()
{
	if (invincibleTimeFlag_ == false)
	{
		invincibleTime_ = clock();
		invincibleTimeFlag_ = true;
	}

	if (clock() - invincibleTime_ >= 500)
	{
		myState_ = NORMAL;
		invincibleTimeFlag_ = false;
	}
}

void Character::returnPosition(D3DXVECTOR3 enemyPosition, bool enemyMoveFlag)
{
	//自分と相手の距離を求める
	double distance;
	distance = sqrt(pow(position_.x - enemyPosition.x, 2) + pow(position_.z - enemyPosition.z, 2));

	//距離がHIT_DISTANCEより短ければ当たっているとみなしポジションを戻す
	if (distance < HIT_DISTANCE)
	{
		//戻すときに使う変数
		D3DXVECTOR3 returnPosition = D3DXVECTOR3(0, 0, 0);

		//動いている側のポジションを戻す
		if(moveFlag_)
		{
			//ベクトルの計算
			returnPosition.x = position_.x - enemyPosition.x;
			returnPosition.z = position_.z - enemyPosition.z;
			D3DXVec3Normalize(&returnPosition, &returnPosition);
			returnPosition = returnPosition * HIT_DISTANCE;
			//戻す
			//一旦相手と同じ位置にポジションを移動してから…
			position_ = enemyPosition;
			//returnPositionベクトルの方向に離す
			position_ += returnPosition;
		}
	}
}

//進行方向を向ける
void Character::TurnForward(D3DXVECTOR3 &move)
{
	//縦方向の移動は考えない
	move.y = 0;

	//速度が0じゃない　＝　移動していたら
	//if (D3DXVec3Length(&move) > 0)
	{
		//移動ベクトルの長さを１にする
		D3DXVec3Normalize(&move, &move);

		//デフォルトの向いている方向
		D3DXVECTOR3 front = D3DXVECTOR3(0, 0, 1);

		//この後のacos関数に1未満
		if (D3DXVec3Dot(&move, &front) < 1 && D3DXVec3Dot(&move, &front) > -1)
		{
			//デフォルトの向きから見て、現在の進行方向への角度
			float angle = acos(D3DXVec3Dot(&move, &front));


			//デフォルトの向きから見て、現在の進行方向への角度

			if (D3DXVec3Dot(&move, &front) < 1 && D3DXVec3Dot(&move, &front) > -1)
			{
				float angle = acos(D3DXVec3Dot(&move, &front));
				//外積を使って、左右どちらに回転させればいいか求める
				D3DXVECTOR3 cross;
				D3DXVec3Cross(&cross, &move, &front);

				//逆向き
				if (cross.y > 0)
				{
					angle *= -1;
				}

				//回転させる（angleは単位がラジアンなので注意）
				rotate_.y = D3DXToDegree(angle);
			}
		}
	}
}

void Character::TargetMove(D3DXVECTOR3& moveX, D3DXVECTOR3& moveZ)
{
	//Y軸回転に合わせて移動出来るようにする
	D3DXMATRIX mRotZ, mRotX;
	D3DXMatrixRotationY(&mRotZ, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationY(&mRotX, D3DXToRadian(rotate_.y));

	//Z軸移動ベクトル
	D3DXVec3TransformCoord(&moveZ, &D3DXVECTOR3(0, 0, 0.1f), &mRotZ);

	//X軸移動ベクトル
	D3DXVec3TransformCoord(&moveX, &D3DXVECTOR3(0.1f, 0, 0), &mRotX);
}

//void Character::CameraSet()
//{

//}

void Character::MovementRestrictions()
{
	//奥移動制限
	if (position_.z >= WALL_POSITION)
	{
		position_.z = WALL_POSITION;
	}

	//前移動制限
	if (position_.z <= -WALL_POSITION)
	{
		position_.z = -WALL_POSITION;
	}

	//右移動制限
	if (position_.x >= WALL_POSITION)
	{
		position_.x = WALL_POSITION;
	}

	//左移動制限
	if (position_.x <= -WALL_POSITION)
	{
		position_.x = -WALL_POSITION;
	}
}

//かどわき作：モデルのアニメーション
void Character::ModelChangeActivity()
{
	//スタンス（ガードの上下）が変更された時の関数
	ModelChangeStance();

	//ステート（行動時の状態）が変更された時の関数
	ModelChangeState();

	//ムーブステート（移動時の状態）が変更された時の関数
	ModelChangeMoveState();

	//かどわき作：アニメーションのストップ
	StopMotion();
}

//ムーブステート（移動時の状態）が変更された時の関数
void Character::ModelChangeMoveState()
{
	//移動時のアニメーションを変更
	//何もしてない状態にだけ入る
	if (currentMoveState_ != nextMoveState_ && nextBehState_ == STANDARD)
	{
		//かどわき作：移動時のアニメーション
		//この時は「移動時」
		MoveMotion();

		//移動ステートを統一
		currentMoveState_ = nextMoveState_;
	}
}

//ステート（行動時の状態）が変更された時の関数
void Character::ModelChangeState()
{
	//今の状態と次の状態が違うとき
	//要するに、もし今が「立ち」なら
	//次が「立ち以外」の行動の時にif文に入る
	if (currentBehState_ != nextBehState_)
	{
		//動作が終了する際の処理
		ModelEndBehavior();

		//何かしらの動作をする場合の処理
		ModelSomeBehavior();

		//チャージパンチする時
		ModelStartStrongPunch();

		//カウンターパンチをする時
		ModelStartCounterPunch();

		//カウンター失敗して弱点を晒す時
		ModelStartWeak();

		//攻撃を受けてのけぞる時
		ModelStartKnockBack();

		//ラウンド先取した時
		ModelStartRoundGet();

		//ゲームに勝利した時
		ModelStartGameWin();

		//今の状態に次の状態を入れる
		currentBehState_ = nextBehState_;
	}
}

//ゲームに勝利した時
void Character::ModelStartGameWin()
{
	//次の状態が「ゲームに勝利」の時
	if (nextBehState_ == GAME_WIN)
	{
		//ゲームに勝ったぞー！
		Model::SetAnimFrame(hModel_, M_GAME_WIN[M_START], M_GAME_WIN[M_END], M_BASIC_SPEED);
	}
}

//ラウンド先取した時
void Character::ModelStartRoundGet()
{
	//次の状態が「ラウンド先取」の時
	if (nextBehState_ == ROUND_GET)
	{
		//ラウンドゲットー！
		Model::SetAnimFrame(hModel_, M_ROUND_GET[M_START], M_ROUND_GET[M_END], M_BASIC_SPEED);
	}
}

//攻撃を受けてのけぞる時
void Character::ModelStartKnockBack()
{
	//次の状態がのけぞりの時
	if (nextBehState_ == KNOCK_BACK)
	{
		//のけぞりモーション
		Model::SetAnimFrame(hModel_, M_KNOCK_BACK[M_START], M_KNOCK_BACK[M_END] + EXTRA_FRAME, M_BASIC_SPEED);
	}
}

//カウンター失敗して弱点を晒す時
void Character::ModelStartWeak()
{
	//今の状態が「カウンター」次の状態が「弱点」の時
	//カウンター失敗！反動！
	if (currentBehState_ == COUNTER && nextBehState_ == WEAK)
	{
		//隙だらけだお☆
		Model::SetAnimFrame(hModel_, M_WEAK[M_START], M_WEAK[M_END], M_BASIC_SPEED);
	}
}

//カウンターパンチをする時
void Character::ModelStartCounterPunch()
{
	//今の状態が「カウンター」次の状態が「カウンターパンチ」の時
	//カウンター成功！パンチします！
	if (currentBehState_ == COUNTER && nextBehState_ == COUNTER_PUNCH)
	{
		//カウンターパンチ！
		ModelChangeStanceProcess(M_COUNTER_PUNCH[M_UP][M_START], M_COUNTER_PUNCH[M_UP][M_END] + EXTRA_FRAME,
			M_COUNTER_PUNCH[M_DOWN][M_START], M_COUNTER_PUNCH[M_DOWN][M_END] + EXTRA_FRAME, M_BASIC_SPEED);
	}
}

//チャージパンチする時
void Character::ModelStartStrongPunch()
{
	//今の状態が「チャージ中」次の状態が「チャージパンチ」の時
	//チャージ完了！パンチします！
	if (currentBehState_ == CHARG && nextBehState_ == STRONG_PUNCH)
	{
		//ストロングチャージパンチ！
		ModelChangeStanceProcess(M_STRONG_PUNCH[M_UP][M_START], M_STRONG_PUNCH[M_UP][M_START],
			M_STRONG_PUNCH[M_DOWN][M_START], M_STRONG_PUNCH[M_DOWN][M_START], M_BASIC_SPEED);
	}
}

//何かしらの動作をする場合の処理
void Character::ModelSomeBehavior()
{
	//今の状態が「立ち」か「クールタイム」か「無敵状態」
	//この時は、次に何かしらの動作を起こしている
	if (currentBehState_ == STANDARD || currentBehState_ == COOLTIME)
	{
		//何の技を使うか
		switch (nextBehState_)
		{
		case PUNCH:	//パンチ！
			ModelChangeStanceProcess(M_PUNCH[M_UP][M_START], M_PUNCH[M_UP][M_START],
				M_PUNCH[M_DOWN][M_START], M_PUNCH[M_DOWN][M_END], M_BASIC_SPEED);
			break;

		case CHARG:	//チャージ！
			ModelChangeStanceProcess(M_CHARG[M_UP][M_START], M_CHARG[M_UP][M_END] + EXTRA_FRAME,
				M_CHARG[M_DOWN][M_START], M_CHARG[M_DOWN][M_END] + EXTRA_FRAME, M_CHARG_SPEED);
			break;

		case COUNTER: //カウンター！
			Model::SetAnimFrame(hModel_, M_COUNTER[M_START], M_COUNTER[M_END], M_BASIC_SPEED);
			break;
		}
	}
}

//動作が終了する際の処理
void Character::ModelEndBehavior()
{
	//操作時一連の行動が終わり、立ちに戻る時
	//現在の移動状態に応じて処理が変更される
	if (nextBehState_ == STANDARD)
	{
		//かどわき作：移動時のアニメーション
		//この時は「動作終了時」
		MoveMotion();
	}
	//立ちではなくクールタイム
	//パンチを戻す時の処理
	else if (nextBehState_ == COOLTIME)
	{
		//どのパンチを戻すか
		switch (currentBehState_)
		{
		case PUNCH:			//パンチを戻す
			ModelChangeStanceProcess(M_PUNCH[M_UP][M_START] + PUNCH_AFTER_FRAME, M_PUNCH[M_UP][M_END] + EXTRA_FRAME,
				M_PUNCH[M_DOWN][M_START] + PUNCH_AFTER_FRAME, M_PUNCH[M_DOWN][M_END] + EXTRA_FRAME, M_BASIC_SPEED);
			break;

		case STRONG_PUNCH:	//チャージパンチを戻す
			ModelChangeStanceProcess(M_STRONG_PUNCH[M_UP][M_START] + PUNCH_AFTER_FRAME, M_STRONG_PUNCH[M_UP][M_END] + EXTRA_FRAME,
				M_STRONG_PUNCH[M_DOWN][M_START] + PUNCH_AFTER_FRAME, M_STRONG_PUNCH[M_DOWN][M_END] + EXTRA_FRAME, M_BASIC_SPEED);
			break;
		}
	}
}

//スタンス（ガードの上下）が変更された時の関数
void Character::ModelChangeStance()
{
	//ガードの位置を変えた時
	//移動無し、動作無しの場合は、こちらのif文のみ適用される
	if (currentStance_ != nextStance_)
	{
		//かどわき作：移動時のアニメーション
		//この時は「スタンス変更時」
		MoveMotion();

		//今の状態に次の状態を入れる
		currentStance_ = nextStance_;
	}
}

//かどわき作：移動時のアニメーション
//移動時、スタンス変更時、動作終了時に通る
void Character::MoveMotion()
{
	//どの方向に動いているか
	switch (nextMoveState_)
	{
	case MOVE_FRONT:	//前に移動
		ModelChangeStanceProcess(M_MOVE_FRONT[M_UP][M_START], M_MOVE_FRONT[M_UP][M_END],
			M_MOVE_FRONT[M_DOWN][M_START], M_MOVE_FRONT[M_DOWN][M_END], M_BASIC_SPEED);
		break;

	case MOVE_BACK:		//後ろに移動
		ModelChangeStanceProcess(M_MOVE_BACK[M_UP][M_START], M_MOVE_BACK[M_UP][M_END],
			M_MOVE_BACK[M_DOWN][M_START], M_MOVE_BACK[M_DOWN][M_END], M_BASIC_SPEED);
		break;

	case MOVE_LEFT:		//左に移動
		ModelChangeStanceProcess(M_MOVE_LEFT[M_UP][M_START], M_MOVE_LEFT[M_UP][M_END],
			M_MOVE_LEFT[M_DOWN][M_START], M_MOVE_LEFT[M_DOWN][M_END], M_BASIC_SPEED);
		break;

	case MOVE_RIGHT:	//右に移動
		ModelChangeStanceProcess(M_MOVE_RIGHT[M_UP][M_START], M_MOVE_RIGHT[M_UP][M_END],
			M_MOVE_RIGHT[M_DOWN][M_START], M_MOVE_RIGHT[M_DOWN][M_END], M_BASIC_SPEED);
		break;

	case MOVE_STANDARD:	//立ち止まってる
		ModelChangeStanceProcess(M_STANDARD[M_UP][M_START], M_STANDARD[M_UP][M_END],
			M_STANDARD[M_DOWN][M_START], M_STANDARD[M_DOWN][M_END], M_BASIC_SPEED);
		break;
	}
}

//かどわき作：アニメーションのストップ
void Character::StopMotion()
{
	//現在のフレーム
	const float nowFrame = Model::GetAnimFrame(hModel_);

	//上パンチ終了時…
	StopFrame(nowFrame, M_PUNCH[M_UP][M_END]);

	//下パンチ終了時…
	StopFrame(nowFrame, M_PUNCH[M_DOWN][M_END]);

	//上チャージ終了時…
	StopFrame(nowFrame, M_CHARG[M_UP][M_END]);

	//下チャージ終了時…
	StopFrame(nowFrame, M_CHARG[M_DOWN][M_END]);

	//上チャージパンチ終了時…
	StopFrame(nowFrame, M_STRONG_PUNCH[M_UP][M_END]);

	//下チャージパンチ終了時…
	StopFrame(nowFrame, M_STRONG_PUNCH[M_DOWN][M_END]);

	//上カウンターパンチ終了時…
	StopFrame(nowFrame, M_COUNTER_PUNCH[M_UP][M_END]);

	//下カウンターパンチ終了時…
	StopFrame(nowFrame, M_COUNTER_PUNCH[M_DOWN][M_END]);

	//のけぞり終了時…
	StopFrame(nowFrame, M_KNOCK_BACK[M_END]);

	/* ↓この文をきれいに書く方法が思い浮かばない…。↓
	//上パンチ終了時…
	if ( (M_PUNCH[M_UP][M_END] < nowFrame) && (nowFrame < M_PUNCH[M_UP][M_END] + 5) )
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, M_PUNCH[M_UP][M_END], M_PUNCH[M_UP][M_END], M_SPEED);
	}
	//下パンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 270 &&
		Model::GetAnimFrame(hModel_) <= 275)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 270, 270, 1.0f);
	}
	//上チャージ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 340 &&
		Model::GetAnimFrame(hModel_) <= 345)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 340, 340, 1.0f);
	}
	//上チャージパンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 410 &&
		Model::GetAnimFrame(hModel_) <= 415)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 410, 410, 1.0f);
	}
	//下チャージ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 480 &&
		Model::GetAnimFrame(hModel_) <= 485)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 480, 480, 1.0f);
	}
	//下チャージパンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 550 &&
		Model::GetAnimFrame(hModel_) <= 555)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 550, 550, 1.0f);
	}
	//カウンター終了時…
	else if (Model::GetAnimFrame(hModel_) >= 620 &&
		Model::GetAnimFrame(hModel_) <= 625)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 620, 620, 1.0f);
	}
	//ウィーク終了時…
	else if (Model::GetAnimFrame(hModel_) >= 690 &&
		Model::GetAnimFrame(hModel_) <= 695)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 690, 690, 1.0f);
	}
	//のけぞり終了時…
	else if (Model::GetAnimFrame(hModel_) >= 760 &&
		Model::GetAnimFrame(hModel_) <= 765)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 760, 760, 1.0f);
	}*/
}

//ストップする際のフレーム
//引数１：nowFrame 現在のフレーム
//引数２：stopFrame 止める際のフレーム
void Character::StopFrame(const float nowFrame, const float stopFrame)
{
	//終了時…
	if ((stopFrame < nowFrame) && (nowFrame < stopFrame + EXTRA_FRAME))
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, stopFrame, stopFrame, M_BASIC_SPEED);
	}
}

//スタンス（ガードの上下）が変更される際の処理
//引数１、２：上ガード時の開始フレーム、終了フレーム
//引数３，４：下ガード時の開始フレーム、終了フレーム
//引数５：speed モーションの速度
void Character::ModelChangeStanceProcess(const float upStartFrame, const float upEndFrame, const float downStartFrame, const float downEndFrame, const float speed)
{
	//ガードが上の時
	if (nextStance_ == UP)
	{
		//ガード上の行動
		Model::SetAnimFrame(hModel_, upStartFrame, upEndFrame, speed);
	}
	//ガードが下の時
	else
	{
		//ガード下の行動
		Model::SetAnimFrame(hModel_, downStartFrame, downEndFrame, speed);
	}
}

void Character::SetPlayCamera()
{
	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &positionCamera_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &targetCamera_, &worldMatrix_);

	//ビュー行列
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);
}

void Character::SetCountOneCamera()
{
	//カメラのワールド位置と視点
	D3DXVECTOR3 worldPosition, worldTarget;

	//移動
	positionCamera_.z -= 15.0f / 60.0f;
	targetCamera_.z += 5.0f / 60.0f;

	//ワールド行列を使う
	D3DXVec3TransformCoord(&worldPosition, &positionCamera_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &targetCamera_, &worldMatrix_);

	//ビュー行列
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);
}