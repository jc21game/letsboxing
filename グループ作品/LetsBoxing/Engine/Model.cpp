#include "Model.h"
#include <vector>

//3Dモデル（FBXファイル）を管理する
namespace Model
{
	//ロード済みのモデルデータ一覧
	std::vector<ModelData*>	dataList;

	//初期化
	void Initialize()
	{
		AllRelease();
	}

	//モデルをロード
	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;


		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			//すでに開いている場合
			if (dataList[i] != nullptr && dataList[i]->fileName == fileName)
			{
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}

		//新たにファイルを開く
		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			if (FAILED(pData->pFbx->Load(fileName)))
			{
				//開けなかった
				SAFE_DELETE(pData->pFbx);
				SAFE_DELETE(pData);
				return -1;
			}

			//無事開けた
			pData->fileName = fileName;
		}


		//使ってない番号が無いか探す
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] == nullptr)
			{
				dataList[i] = pData;
				return i;
			}
		}

		//新たに追加
		dataList.push_back(pData);
		return dataList.size() - 1;
	}



	//描画
	void Draw(int handle)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size() || dataList[handle] == nullptr)
		{
			return;
		}

		//アニメーションを進める
		dataList[handle]->nowFrame += dataList[handle]->animSpeed;

		//最後までアニメーションしたら戻す
		if (dataList[handle]->nowFrame > dataList[handle]->endFrame)
			dataList[handle]->nowFrame = dataList[handle]->startFrame;



		if (dataList[handle]->pFbx)
		{
			dataList[handle]->pFbx->Draw(dataList[handle]->matrix, (int)dataList[handle]->nowFrame);
		}
	}


	//任意のモデルを開放
	void Release(int handle)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size() || dataList[handle] == nullptr)
		{
			return;
		}

		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			//すでに開いている場合
			if (dataList[i] != nullptr && i != handle && dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		//使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}


		SAFE_DELETE(dataList[handle]);
	}


	//全てのモデルを解放
	void AllRelease()
	{
		for (int i = 0; (unsigned int)i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}


	//アニメーションのフレーム数をセット
	void SetAnimFrame(int handle, float startFrame, float endFrame, float animSpeed)
	{
		dataList[handle]->SetAnimFrame(startFrame, endFrame, animSpeed);
	}


	//現在のアニメーションのフレームを取得
	float GetAnimFrame(int handle)
	{
		return dataList[handle]->nowFrame;
	}


	//任意のボーンの位置を取得
	D3DXVECTOR3 GetBonePosition(int handle, std::string boneName)
	{
		D3DXVECTOR3 pos = dataList[handle]->pFbx->GetBonePosition(boneName);
		D3DXVec3TransformCoord(&pos, &pos, &dataList[handle]->matrix);
		return pos;
	}


	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX matrix)
	{
		if (handle < 0 || (unsigned int)handle >= dataList.size())
		{
			return;
		}

		dataList[handle]->matrix = matrix;
	}


	//ワールド行列の取得
	D3DXMATRIX GetMatrix(int handle)
	{
		return dataList[handle]->matrix;
	}


	//レイキャスト（レイを飛ばして当たり判定）
	void RayCast(int handle, RayCastData *data)
	{
		D3DXVECTOR3 target = data->start + data->dir;
		D3DXMATRIX matInv;
		D3DXMatrixInverse(&matInv, 0, &dataList[handle]->matrix);
		D3DXVec3TransformCoord(&data->start, &data->start, &matInv);
		D3DXVec3TransformCoord(&target, &target, &matInv);
		data->dir = target - data->start;

		dataList[handle]->pFbx->RayCast(data);
	}
}