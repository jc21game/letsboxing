#pragma once
#include "Engine/Global.h"

class PictDisplay;

//メニューシーンを管理するクラス
class MenuScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		MATCH_LOGO,
		OPERATION_MATCH_LOGO,
		MAX
	};
	PictDisplay* pPictArray_[MAX];

	//状態の種類
	enum STATE {
		STATE_MATCH,			//試合
		STATE_OPERATION_MEMO	//操作説明
	} state_;

	int hPict_;		//背景画像番号

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MenuScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};