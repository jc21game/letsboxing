#pragma once
#include "Engine/Global.h"

class PictDisplay;

//ギブアップシーンを管理するクラス
class GiveUpScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		LOGO_YES,
		LOGO_NO,
		MAX
	};
	PictDisplay* pPictArray_[MAX];

	//状態の種類
	enum STATE {
		STATE_YES,	//YES
		STATE_NO	//NO
	} state_;

	int hPict_;				//画像番号
	bool possible_;			//誤動作防止用フラグ

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GiveUpScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};