#include "Sprite.h"
#include "Direct3D.h"


Sprite::Sprite() :
	pSprite_(nullptr), pTexture_(nullptr)
{
	//初期化処理
}


Sprite::~Sprite()
{
	//解放処理
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

void Sprite::Load(const char* FileName)
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);
	assert(pSprite_ != nullptr);

	//画像サイズ取得
	D3DXIMAGE_INFO iinfo;
	D3DXGetImageInfoFromFile(FileName, &iinfo);

	//構造体に代入
	imageSize_.height = iinfo.Height;
	imageSize_.width = iinfo.Width;


	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, FileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Sprite::Draw(const D3DXMATRIX & matrix, D3DXCOLOR color)
{
	//行列をセット
	pSprite_->SetTransform(&matrix);


	//ゲーム画面の描画
	pSprite_->Begin(0);	//D3DXSPRITE_ALPHABLEND
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, color);
	pSprite_->End();
}
