#pragma once
#include <string>
#include "Sprite.h"

namespace Image
{
	//構造体モデルデータ
	struct ImageData
	{
		std::string fileName;	//STLの文字列を管理する変数
		Sprite*	pSprite;			//FBXファイルを入れる変数
		D3DXMATRIX matrix;		//行列変数

		//コンストラクタ
		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//ファイル名を受け取る関数
	//引数：fileName	ファイルの名前
	//戻値：int			ファイル番号を返す
	int Load(std::string fileName);

	//ファイルを読み込む関数
	//引数：handle		ファイル番号
	//戻値：なし
	void Draw(int handle);
	//画像の透明度を使いたい場合はこっちを使う
	//第二引数に透明度を0~1の間で入れる
	void Draw(int handle, float alphaBlend);

	void Draw(int handle, D3DXCOLOR color);

	void DrawSub(int handle, D3DXCOLOR color);

	//行列をセットする関数
	//引数：handle　ファイル番号、matrix　行列
	//戻値：なし
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//解放処理
	//引数：handle　ファイル番号
	//戻値：なし
	void Release(int handle);

	//ゲーム終了時に呼び出される解放処理
	//引数：なし
	//戻値：なし
	void AllRelease();
	
	//画像の幅を調べて、その幅を返す
	//引数：hPict 画像番号
	//戻値：int 画像の幅
	int GetImageSizeWidth(int hPict);
	
	//画像の高さを調べて、その高さを返す
	//引数：hPict 画像番号
	//戻値：int 画像の高さ
	int GetImageSizeHeight(int hPict);
};


