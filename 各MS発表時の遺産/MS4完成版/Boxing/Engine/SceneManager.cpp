#include "SceneManager.h"
#include "../SplashScene.h"
#include "../TitleScene.h"
#include "../MenuScene.h"
#include "../OperationMemoScene.h"
#include "../PlayScene.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_SPLASH;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_SPLASH;
IGameObject* SceneManager::pCurrentScene_ = nullptr;
bool SceneManager::sceneReload_ = false; //初期値はfalse

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<SplashScene>(this);
}

//更新
void SceneManager::Update()
{

	if (currentSceneID_ != nextSceneID_ || sceneReload_ == true)
	{
		//SceneManager以下に複数のシーンがある場合に開放する
		for (auto scene = childList_.begin(); scene != childList_.end(); scene++)
		{
			(*scene)->ReleaseSub();
			SAFE_DELETE(*scene);
		}
		childList_.clear();		//リストの開放
		switch (nextSceneID_)
		{
		case SCENE_ID_SPLASH:
			pCurrentScene_ = CreateGameObject<SplashScene>(this);
			break;
		case SCENE_ID_TITLE:
			pCurrentScene_ = CreateGameObject<TitleScene>(this);
			break;
		case SCENE_ID_MENU:
			pCurrentScene_ = CreateGameObject<MenuScene>(this);
			break;
		case SCENE_ID_OPERATION_MEMO:
			pCurrentScene_ = CreateGameObject<OperationMemoScene>(this);
			break;
		case SCENE_ID_PLAY:
			pCurrentScene_ = CreateGameObject<PlayScene>(this);
			break;
		}
		//childList_.erase(scene);
		currentSceneID_ = nextSceneID_;

		//リロードしないようにfalse
		sceneReload_ = false;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}

void SceneManager::ReloadScene()
{
	sceneReload_ = true;
};
