#include <time.h>
#include "ResultScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"
#include "Engine/Audio.h"

// unko = WIN_REMATCH 

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), winGame_(WIN_1P_ONRY), state_(STATE_WIN_DISPLAY)
	, timeFlag_(false), time_(0)
{
	for (int i = 0; i < WIN_MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//初期化
void ResultScene::Initialize()
{
	const std::string fileName[WIN_MAX]{
		"data/1PWin.png",
		"data/1PWinRematch.png",
		"data/2PWin.png",
		"data/2PWinRematch.png"
	};

	for (int i = 0; i < WIN_MAX; i++)
	{
		//画像データのロード
		hPict_[i] = Image::Load(fileName[i]);
		assert(hPict_[i] >= 0);
	}
	

	//ファイル名
	const std::string logoFileName[LOGO_MAX]{
		"data/YesLogo.png",
		"data/NoLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < LOGO_MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, logoFileName[i]);
	}

	//ここで初期の時に移らないように画面外に出している
	pPictArray_[LOGO_YES]->SetPosition(D3DXVECTOR3(2000, 0, 0)); 
	pPictArray_[LOGO_NO]->SetPosition(D3DXVECTOR3(2000, 0, 0));
}


//更新
void ResultScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//Win!が表示中
	case STATE_WIN_DISPLAY:
		//1Pの勝ち
		if (winFlag_)
		{
			winGame_ = WIN_1P_ONRY;
		}
		//2Pの勝ち
		else
		{
			winGame_ = WIN_2P_ONRY;
		}

		//クロック関数を使ってタイム変数を初期化する
		TimeClockReset();

		//2秒くらい経った時
		if (clock() - time_ >= 2000)
		{
			//ここで表示したい画像を切り替えている
			//1Pの勝ち
			if (winFlag_)
			{
				winGame_ = WIN_1P_REMATCH;
			}
			//2Pの勝ち
			else
			{
				winGame_ = WIN_2P_REMATCH;
			}

			//止めたタイミングで出したいからここでまた位置をいじる
			pPictArray_[LOGO_YES]->SetPosition(D3DXVECTOR3(80, 480, 0));
			pPictArray_[LOGO_NO]->SetPosition(D3DXVECTOR3(600, 480, 0));

			//YESを選択になる
			state_ = STATE_YES;
		}
		break;

	//YESロゴ選択時
	case STATE_YES:
		//ロゴが黄色くなる
		pPictArray_[LOGO_YES]->Selection();
		
		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			//リロード
			SceneManager* pSceneManager;
			pSceneManager->ReloadScene();

			g.playerOneWinNum = NULL;
			g.playerTwoWinNum = NULL;

			//停止
			Audio::Stop("BGM4");
		}
		//左キー右キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_NO;
			pPictArray_[LOGO_YES]->UnSelection();
		}
		
		break;

	//NOロゴ選択時
	case STATE_NO:
		//ロゴが黄色くなる
		pPictArray_[LOGO_NO]->Selection();

		//Enterでメニュー画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);

			g.playerOneWinNum = NULL;
			g.playerTwoWinNum = NULL;

			//停止
			Audio::Stop("BGM4");
		}
		//左キー右キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_YES;
			pPictArray_[LOGO_NO]->UnSelection();
		}
		
		break;
	}
}

//クロック関数を使ってタイム変数を初期化する
void ResultScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_[winGame_], worldMatrix_);
	Image::Draw(hPict_[winGame_]);
}

//開放
void ResultScene::Release()
{
}