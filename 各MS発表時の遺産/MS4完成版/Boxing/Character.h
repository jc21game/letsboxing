#pragma once
#include "Engine/IGameObject.h"

class BoxCollider;
class SphereCollider;
class PlayScene;

#define SPEED 0.8
#define HP 20
#define HP_YELLOW 12
#define HP_RED 6

const float POSITION_Y = 10.5f;

//STATEがSTANDARDの際に使用。MAYAのアニメーション専用
enum MOVE_STATE	//動いてる際の状態
{
	MOVE_FRONT,	//前
	MOVE_BACK,	//後
	MOVE_LEFT,	//左
	MOVE_RIGHT,	//右
	MOVE_STANDARD, //初期値
};

//キャラクターを管理するクラス
class Character : public IGameObject
{
	
protected:
	int hModel_;	//モデル番号
	
	int hp_;		//体力
	int pp_;		//強攻撃のゲージ　とりあえずPowerPointでppにした

	unsigned int invincibleModelTime;	//無敵の際、モデルを点滅させるための時間

	bool timeFlag_;	//カウンターの時間やパンチのクールタイムを計っている時true
	double time_;	//カウンターの時間やパンチのクールタイムを計る時に使う

	//上のタイム変数で無敵時間の処理をしてしまうと、永遠無敵になってしまう
	bool invincibleTimeFlag_;	//無敵中の時間を計っている時true
	double invincibleTime_;		//無敵中の時間を計る時に使う

	enum STANCE		//構え　0で中段1で上段の構えになる
	{
		UP,
		DOWN
	}nextStance_;

	enum STATE
	{
		STANDARD = 0,
		PUNCH,
		CHARG,				//STRONG_PUNCHのチャージ状態
		STRONG_PUNCH,
		COOLTIME,			//パンチのクールタイム状態
		COUNTER,
		WEAK,				//カウンターが失敗した時の隙が出来ている状態
		COUNTER_PUNCH,		//カウンターが成功したときパンチする状態
		KNOCK_BACK,				//パンチ食らったときののけぞり
	} nextState_;

	enum MY_STATE	//私の状態
	{
		NORMAL,		//普通はコレ
		INVINCIBLE	//無敵状態
	} myState_;

	MOVE_STATE nextMoveState_;	//動いてる時の状態

	//現在の状態
	STANCE currentStance_;
	STATE currentState_;
	MOVE_STATE currentMoveState_;

	BoxCollider* bodyCollider_;			//体の当たり判定用コライダー
	SphereCollider* punchCollider_;		//パンチ用のコライダー
	D3DXVECTOR3 punchColliderPosition_;	//攻撃用コライダーを動かすときに使う
	double punchColliderForwardPosition_;	//攻撃用コライダーがどれくらい前方に出ているか

	std::string opponent_;	//対戦相手の名前　PlayerCharacterから見ればNonPlayerCharacter

	bool standardFlag_;		//攻撃が当たった時などに次のフレームでstate_をSTANDATDにするフラグ
	bool coolTimeFlag_;		//standardFlag_と同じ
	bool counterPunchFlag_;	//上に同じ
	bool knockBackFlag_;	//上に同じ

	//胴体同士が接触していた時めり込まないようにする処理をする時に使う
	bool moveFlag_;			//動いている時true
	
	PlayScene* pPlayScene_;	//プレイシーンのポインタ

	Character* enemyCharacter_;	//相手のポインタを入れる

	D3DXVECTOR3 positionCamera_;		//カメラの位置
	D3DXVECTOR3 targetCamera_;		//カメラの向き
	D3DXMATRIX view_;				//カメラの視点

public:
	//コンストラクタ
	Character(IGameObject* parent);
	Character(IGameObject* parent, std::string name);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	//描画
	void Draw() override;
	virtual void ChildDraw() = 0;
	//開放
	void Release() override;

	//何かに当たった
//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	void Punch();			//パンチ
	void Charg();			//強パンチ
	void StrongPunch();		//強パンチ
	void CoolTime();		//攻撃のクールタイム
	void Counter();			//カウンター
	void Weak();			//カウンターが失敗したときの隙
	void StanceChange(STANCE stance);	//ガードする位置変更
	void CoutnterPunch();				//カウンターパンチ
	void KnockBack();					//のけぞり状態
	void Invincible();					//無敵状態

	virtual void PunchCommand() = 0;		//パンチを発動する条件　プレイヤーならキーボードでの処理になるしNPCならAIが処理する
	virtual void StrongPunchCommand() = 0;	//強パンチを発動する条件　パンチ同様
	virtual void CounterCommand() = 0;		//カウンターを発動する条件　パンチ同様
	virtual void StanceChangeCommand() = 0;		//構え変更条件　パンチ同様
	virtual void MoveCommand() = 0;				//移動　パンチ同様

	STATE GetState()
	{
		return nextState_;
	}

	void SetState(STATE state)
	{
		nextState_ = state;
	}

	STANCE GetStance()
	{
		return nextStance_;
	}

	MY_STATE GetMyState()
	{
		return myState_;
	}

	void SetStandardFlag(bool standardFlag)
	{
		standardFlag_ = standardFlag;
	}

	void SetCoolTimeFlag(bool coolTimeFlag)
	{
		coolTimeFlag_ = coolTimeFlag;
	}

	void SetCounterPunchFlag(bool counterPunchFlag)
	{
		counterPunchFlag_ = counterPunchFlag;
	}

	void SetKnockBackFlag(bool knockBackFlag)
	{
		knockBackFlag_ = knockBackFlag;
	}

	bool GetMoveFlag()
	{
		return moveFlag_;
	}


	int GetHp()
	{
		return hp_;
	}

	D3DXMATRIX GetView()
	{
		return view_;
	}

	void SetTimeFlag(bool timeFlag)
	{
		timeFlag_ = timeFlag;
	}

	void SetEnemyCharacter(Character* enemyCharacter)
	{
		enemyCharacter_ = enemyCharacter;
	}

	void returnPosition(D3DXVECTOR3 enemyPosition, bool enemyMoveFlag);

	//進行方向を向く
	void TurnForward(D3DXVECTOR3 &move);
	//移動ベクトル
	void TargetMove(D3DXVECTOR3& moveX, D3DXVECTOR3& moveZ);
	
	void SetPlayCamera();

	void SetCountOneCamera();

	void MovementRestrictions();

	//動いてる時の状態のセッター
	void SetMoveState(MOVE_STATE nextMoveState)
	{
		nextMoveState_ = nextMoveState;
	}

	//かどわき作：モデルのアニメーション
	void ModelChangeActivity();

	//かどわき作：移動時のアニメーション
	void MoveMotion();

	//かどわき作：アニメーションのストップ
	void StopMotion();
};