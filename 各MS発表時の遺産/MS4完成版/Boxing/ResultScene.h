#pragma once
#include "Engine/Global.h"

class PictDisplay;

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		LOGO_YES,
		LOGO_NO,
		LOGO_MAX
	};
	PictDisplay* pPictArray_[LOGO_MAX];

	enum GAME {
		WIN_1P_ONRY,
		WIN_1P_REMATCH,
		WIN_2P_ONRY,
		WIN_2P_REMATCH,
		WIN_MAX
	} winGame_;

	//状態の種類
	enum STATE {
		STATE_WIN_DISPLAY,	//Win!の表示
		STATE_YES,	//YES
		STATE_NO	//NO
	} state_;

	int hPict_[WIN_MAX];	//画像番号
	bool winFlag_;			//勝ったかフラグ

	bool timeFlag_;		//タイムリセットフラグ
	double time_;		//クロック関数で時間を保存

public:

	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//クロック関数を使ってタイム変数を初期化する
	void TimeClockReset();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//フラグのセット
	void SetWinFlag(bool winFlag)
	{
		winFlag_ = winFlag;
	}
};