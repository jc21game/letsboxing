#include <ctime>
#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), sceneStartTime_(NULL),
	gameStartFlag_(false)
{
}

//初期化
void TitleScene::Initialize()
{
	//ファイル名
	const std::string fileName[MAX]
	{
		"data/LetsBoxing.png",
		"data/StartLogo.png",
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}

	//初期位置変更
	pPictArray_[LETS_BOXING]->SetPosition(D3DXVECTOR3(ORIGIN, (FLOAT)g.screenHeight, ORIGIN));
	pPictArray_[START_LOGO]->SetPosition(D3DXVECTOR3(HIDDEN, HIDDEN, ORIGIN));
	
	//シーン切り替え時の時間を取得
	time(&sceneStartTime_);
}

//更新
void TitleScene::Update()
{
	//経過時間測定
	double difference = ElapsedTime();

	//タイトルロゴの位置を取得
	D3DXVECTOR3 logoPos = pPictArray_[LETS_BOXING]->GetPosition();

	//ロゴの移動
	LogoMove(logoPos);

	//ゲームスタート描画
	GameStartDraw(logoPos);

	//秒数の処理でスプラッシュ画面に戻る
	SecondsSplashBack(difference);

	//Enterキーの処理
	EnterProcess();

	//Escキーでゲーム終了
	EscapeGameEnd();
}

//Escキーでゲーム終了
void TitleScene::EscapeGameEnd()
{
	//ゲーム終了
	if (Input::IsKeyDown(DIK_ESCAPE) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 1))
	{
		PostQuitMessage(0);
	}
}

//Enterキーの処理
void TitleScene::EnterProcess()
{
	//Enterを押したら処理
	if (Input::IsKeyDown(DIK_RETURN) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 1))
	{
		//フラグがfalseの場合
		if (gameStartFlag_ == false)
		{
			//画像の位置を移動させる
			pPictArray_[LETS_BOXING]->SetPosition(D3DXVECTOR3(ORIGIN, ORIGIN, ORIGIN));
		}
		//フラグがtrueの場合
		else if (gameStartFlag_ == true)
		{
			//メニューシーンへ移行
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);
		}
	}

}

//秒数の処理でスプラッシュ画面に戻る
void TitleScene::SecondsSplashBack(double difference)
{
	//30秒たったらスプラッシュシーンに戻る
	if (difference >= 30.0f)
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_SPLASH);
	}
}

//経過時間測定
double TitleScene::ElapsedTime()
{
	//経過時間
	time_t elapsedTime;
	//今の時間を取得
	time(&elapsedTime);
	//経過時間 - シーン切り替え時の時間 で差を求める
	return difftime(elapsedTime, sceneStartTime_);
}

//ゲームスタート描画
void TitleScene::GameStartDraw(D3DXVECTOR3 &logoPos)
{
	//規定位置までたどり着いたなら
	if (logoPos.y <= ORIGIN)
	{
		//画面に「Game Start!」と表示
		pPictArray_[START_LOGO]->SetPosition(D3DXVECTOR3(ORIGIN, ORIGIN, ORIGIN));
		//ゲームスタートのフラグをtrue
		gameStartFlag_ = true;
	}
}

//ロゴの移動
void TitleScene::LogoMove(D3DXVECTOR3 &logoPos)
{
	//規定位置までたどり着いてない場合
	if (logoPos.y > ORIGIN)
	{
		//ロゴの表示位置移動
		logoPos.y -= 5;
		pPictArray_[LETS_BOXING]->SetPosition(D3DXVECTOR3(ORIGIN, logoPos.y, ORIGIN));
	}
}

//描画
void TitleScene::Draw()
{
}

//開放
void TitleScene::Release()
{
}