#include "DifficultyScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"


//コンストラクタ
DifficultyScene::DifficultyScene(IGameObject * parent)
	: IGameObject(parent, "DifficultyScene"), hPict_(-1),
	state_(STATE_EASY)
	
{
}

//初期化
void DifficultyScene::Initialize()
{
	//背景画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	//ファイル名
	const std::string fileName[MAX]{
		"data/EasyLogo.png",
		"data/NormalLogo.png",
		"data/HardLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}
}

//更新
void DifficultyScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//弱いロゴ選択時
	case STATE_EASY:
		//ロゴが黄色くなる
		pPictArray_[EASY_LOGO]->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		 
			//右キーを押したら普通ロゴにカーソルが移動
			if (Input::IsKeyDown(DIK_RIGHT))
			{
				state_ = STATE_NORMAL;
				pPictArray_[EASY_LOGO]->UnSelection();
			}
		    //左キーを押したら強いロゴにカーソルが移動
		    if (Input::IsKeyDown(DIK_LEFT))
		    {
			   state_ = STATE_HARD;
			   pPictArray_[EASY_LOGO]->UnSelection();
		    }
		break;

	//普通ロゴ選択時
	case STATE_NORMAL:
		//ロゴが黄色くなる
		pPictArray_[NORMAL_LOGO]->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//右キーを押したら強いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_HARD;
			pPictArray_[NORMAL_LOGO]->UnSelection();
		}
		//左キーを押したら弱いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_EASY;
			pPictArray_[NORMAL_LOGO]->UnSelection();
		}
		break;

	//強いロゴ選択時
	case STATE_HARD:
		//ロゴが黄色くなる
		pPictArray_[HARD_LOGO]->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//右キーを押したら弱いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_EASY;
			pPictArray_[HARD_LOGO]->UnSelection();
		}
		//左キーを押したら普通ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_NORMAL;
			pPictArray_[HARD_LOGO]->UnSelection();
		}
		break;
	}

	//Escキーで前の画面（メニュー画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}


//描画
void DifficultyScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);


}

//開放
void DifficultyScene::Release()
{
}

