#include "Character.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"
#include "PlayScene.h"
#include <time.h>
#include "NonPlayerCharacter.h"
#include "PlayerCharacter.h"
#include <math.h>
#include "Engine/Direct3D.h"
#include "Engine/Model.h"
#include "Engine/Audio.h"

#define PUNCH_COLLIDER_MOVE 0.5
#define PUNCH_COLLIDER_MOVE_MAX 1
#define PUNCH_COLLIDER_INIT_POS 0.5

#define PUNCH_DAMAGE 2
#define STRONG_PUNCH_DAMAGE 4 
#define COUNTER_DAMAGE 2

#define HIT_DISTANCE 2

#define WALL_POSITION 8

//コンストラクタ
//名前を付けないと名前がCharacterになった状態で下のコンストラクタが呼ばれる
Character::Character(IGameObject * parent)
	:Character(parent, "Character")
{
}

Character::Character(IGameObject * parent, std::string name)
	: IGameObject(parent, name), hModel_(-1), hp_(0), pp_(0), invincibleModelTime(0), timeFlag_(false), time_(0), invincibleTimeFlag_(false), invincibleTime_(0), isCommandFlag_(false),
	nextState_(STANDARD), nextStance_(DOWN), myState_(NORMAL), currentState_(STANDARD), currentStance_(DOWN), opponent_(""), moveFlag_(false), counterPunchFlag_(false),
	knockBackFlag_(false)
{
	hModel_ = Model::Load("data/Model2.fbx");
	assert(hModel_ >= 0);

	////モデル用マトリックス作成
	//D3DXMATRIX mX;
	//D3DXMatrixRotationX(&mX, D3DXToRadian(0));
	//D3DXMATRIX mY;
	//D3DXMatrixRotationY(&mY, D3DXToRadian(0));
	//D3DXMATRIX mZ;
	//D3DXMatrixRotationZ(&mZ, D3DXToRadian(0));

	//D3DXMATRIX m;
	//D3DXMatrixScaling(&m, 0.001f, 0.001f, 0.001f);

	//D3DXMATRIX mT;
	//D3DXMatrixTranslation(&mT, 0, -1.2f, 0);

	//D3DXMATRIX mW;

	//modelMatrix_ = m * mX * mY * mZ * mT;

	//worldMatrix_ *= m;

	Model::SetAnimFrame(hModel_, 70, 130, 1.5f);
}

//デストラクタ
Character::~Character()
{
}

//初期化
void Character::Initialize()
{
}

//更新
void Character::Update()
{
	//操作可能なら
	if (isCommandFlag_)
	{
		//2画面用カメラを使用
		SetCamera();
	}

	//かどわき作：モデルのアニメーション
	ModelChangeActivity();

	//画面分割するための処理



	//画面分割するための処理
	//if ((name_ == "PlayerCharacter") || (name_ == "NonPlayerCharacter"))
	//{
	//	CameraSet();
	//}

	//敵の方向を見る
	if (isCommandFlag_)
	{
		TurnForward((enemyCharacter_->GetPosition() - position_));
	}

	//下で動いていた場合moveFlag_をtrueにする処理をするため一旦ここでfalseにする
	moveFlag_ = false;

	//OnCollisionでstate_変更フラグが立っていた場合state_を変更する
	if (standardFlag_ == true)
	{
		standardFlag_ = false;
		nextState_ = STANDARD;
	}
	if (coolTimeFlag_ == true)
	{
		coolTimeFlag_ = false;
		nextState_ = COOLTIME;
	}
	if (counterPunchFlag_ == true)
	{
		counterPunchFlag_ = false;
		nextState_ = COUNTER_PUNCH;
	}
	if (knockBackFlag_ == true)
	{
		knockBackFlag_ = false;
		nextState_ = KNOCK_BACK;
	}

	//状態によって処理を分ける
	switch (nextState_)
	{
	case STANDARD:	//全ての行動ができる

		//試合中は下記の操作が可能
		if (isCommandFlag_)
		{
			PunchCommand();
			StrongPunchCommand();
			CounterCommand();
			StanceChangeCommand();
			MoveCommand();
		}
		break;

	case PUNCH:	//パンチ中はパンチが終わるまで他の行動ができない
		Punch();
		break;

	case CHARG:	//強攻撃のチャージ中はほかの行動ができない
		Charg();
		break;

	case STRONG_PUNCH:	//強攻撃中は他の行動ができない
		StrongPunch();
		break;

	case COOLTIME:	//攻撃のクールタイム中はカウンターと構えの上下と移動ができる
		CoolTime();

		//試合中は下記の操作が可能
		if (isCommandFlag_)
		{
			CounterCommand();
			StanceChangeCommand();
			MoveCommand();
		}
		break;

	case COUNTER:	//カウンター中は他の行動ができない
		Counter();
		break;

	case WEAK:	//カウンターが失敗すると他の行動ができない
		Weak();
		break;

	case COUNTER_PUNCH:	//カウンターパンチ中は他の行動ができない
		CoutnterPunch();
		break;

	case KNOCK_BACK:	//のけぞっている間は他の行動ができない
		KnockBack();
		break;
	}

	//無敵中
	if (myState_ == INVINCIBLE)
	{
		Invincible();
	}

	//ぶつかったときの処理
	returnPosition(enemyCharacter_->GetPosition(), enemyCharacter_->GetMoveFlag());

	if (hp_ <= 0)
	{
		KillMe();
	}
}


//描画
void Character::Draw()
{
	//無敵中
	if (myState_ == INVINCIBLE)
	{
		//点滅のためのタイムを増やす
		invincibleModelTime++;

		//割った時の余りが0以外の際に表示する
		//割る数字は、奇数じゃないと何故か表示がおかしくなる…。
		if (invincibleModelTime % 5 != 0)
		{
			Model::SetMatrix(hModel_, worldMatrix_);
			Model::Draw(hModel_);
		}
	}
	//それ以外は普通に表示
	else
	{
		Model::SetMatrix(hModel_, worldMatrix_);
		Model::Draw(hModel_);
	}

	//操作可能ならHPが見えるようになる
	if (isCommandFlag_)
	{
		//継承先でoverrideして使う
		ChildDraw();
	}
}

//開放
void Character::Release()
{
}

void Character::OnCollision(IGameObject * pTarget)
{
	//当たったコライダーが対戦相手の持っているコライダーだった場合
	if (pTarget->GetName() == opponent_)
	{
		for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
		{
			//当たっているコライダーの場合
			if ((*itr)->GetHitCollider() != NULL)
			{
				//自分の攻撃コライダーが相手の胴体コライダーに当たった時
				//ここで自分のstate_や相手のstate_変更やダメージの処理などを全て行う
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				{
					//無敵状態ではない
					if (((Character*)pTarget)->GetMyState() != INVINCIBLE)
					{
						if (nextState_ == PUNCH)
						{
							switch (((Character*)pTarget)->GetState())
							{
							case STANDARD:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case PUNCH:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								knockBackFlag_ = true;
								break;

							case CHARG:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);

								break;

							case STRONG_PUNCH:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case COOLTIME:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case COUNTER:
								hp_ -= COUNTER_DAMAGE;
								knockBackFlag_ = true;
								((Character*)pTarget)->SetCounterPunchFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case WEAK:
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;
							}
						}

						else if (nextState_ == STRONG_PUNCH)
						{
							//再生
							Audio::Play("Kill");
							switch (((Character*)pTarget)->GetState())
							{
							case STANDARD:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case PUNCH:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case CHARG:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case STRONG_PUNCH:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								break;

							case COOLTIME:
								if (((Character*)pTarget)->GetStance() == nextStance_)
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								else
								{
									((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
									((Character*)pTarget)->SetKnockBackFlag(true);
								}
								break;

							case COUNTER:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;

							case WEAK:
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
								((Character*)pTarget)->SetKnockBackFlag(true);
								((Character*)pTarget)->SetTimeFlag(false);
								break;
							}
							//pp0にする
							pp_ = 0;
						}
					}

					if (nextState_ != KNOCK_BACK)
					{
						coolTimeFlag_ = true;
					}
				}

				//本当は↓で攻撃を受けたときのstate_変更やダメージの処理をしようと思ったが上のやつをもう一回書かなきゃいけない
				////自分の胴体コライダーが相手の攻撃コライダーに当たった時
				//if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				//{
				//}

				//自分の攻撃コライダーが相手の攻撃コライダーに当たった時
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				{
					//攻撃コライダー同士が当たっている時はお互いのstate_がPUNCHかSTRONG_PUNCHの時だけ
					if (nextState_ == ((Character*)pTarget)->GetState())

					{
						coolTimeFlag_ = true;
					}
					else if(nextState_ == STRONG_PUNCH && ((Character*)pTarget)->GetState() == PUNCH)
					{
						((Character*)pTarget)->SetCoolTimeFlag(true);
					}
				}

				//通り抜けないようにする処理はUpDateでやることにしたのでここでの処理は必要ない
				////自分の胴体コライダーが相手の胴体コライダーに当たった時
				//if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				//{
				//	//通り抜けないようにする処理
				//}
			}
		}
	}
}

void Character::Punch()
{
	//攻撃用コライダーのオブジェクト作成　攻撃の初めに一回だけ処理される
	bool punchColliderCreateFlag_ = true;
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			punchColliderCreateFlag_ = false;
		}
	}
	if (punchColliderCreateFlag_)
	{
		//構えの位置によって攻撃の生成位置が変わる
		if (nextStance_ == UP)
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, 2, 0), 0.5f);
			AddCollider(punchCollider_);
		}
		else
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, -2, 0), 0.5f);
			AddCollider(punchCollider_);
		}

		//攻撃用コライダーが作られた時にポジション用の変数も攻撃発動初期位置にする
		punchColliderForwardPosition_ = PUNCH_COLLIDER_INIT_POS;
	}

	//攻撃開始
	if (punchColliderForwardPosition_ <= PUNCH_COLLIDER_MOVE_MAX)
	{
		D3DXMATRIX m;
		punchColliderForwardPosition_ += PUNCH_COLLIDER_MOVE;

		D3DXMatrixRotationY(&m, D3DXToRadian(rotate_.y));
		//構えの位置によって攻撃の生成位置が変わる
		if (nextStance_ == UP)
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, 2, punchColliderForwardPosition_), &m);
		}
		else
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, -2, punchColliderForwardPosition_), &m);
		}

		punchCollider_->SetCenter(punchColliderPosition_);
	}
	//攻撃終了
	else
	{
		//強攻撃だった場合ppを0にする
		if (nextState_ == STRONG_PUNCH)
		{
			pp_ = 0;
		}

		//クールタイム状態に変更
		nextState_ = COOLTIME;
	}
	
}

void Character::Charg()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1000)
	{
		nextState_ = STRONG_PUNCH;
		timeFlag_ = false;
	}
}

void Character::StrongPunch()
{
	Punch();
}

void Character::CoolTime()
{
	//攻撃用コライダーの削除
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end();)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			SAFE_DELETE((*itr));
			itr = ColliderList_.erase(itr);
			continue;
		}
		itr++;
	}

	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 250)
	{
		nextState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::Counter()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1500)
	{
		nextState_ = WEAK;
		timeFlag_ = false;
	}
}

void Character::Weak()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 500)
	{
		nextState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::StanceChange(STANCE stance)
{
	nextStance_ = stance;
}

void Character::CoutnterPunch()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 250)
	{
		nextState_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::KnockBack()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 300)
	{
		nextState_ = STANDARD;
		myState_ = INVINCIBLE;
		timeFlag_ = false;
	}
}

void Character::Invincible()
{
	if (invincibleTimeFlag_ == false)
	{
		invincibleTime_ = clock();
		invincibleTimeFlag_ = true;
	}

	if (clock() - invincibleTime_ >= 500)
	{
		myState_ = NORMAL;
		invincibleTimeFlag_ = false;
	}
}

void Character::returnPosition(D3DXVECTOR3 enemyPosition, bool enemyMoveFlag)
{
	//自分と相手の距離を求める
	double distance;
	distance = sqrt(pow(position_.x - enemyPosition.x, 2) + pow(position_.z - enemyPosition.z, 2));

	//距離がHIT_DISTANCEより短ければ当たっているとみなしポジションを戻す
	if (distance < HIT_DISTANCE)
	{
		//戻すときに使う変数
		D3DXVECTOR3 returnPosition = D3DXVECTOR3(0, 0, 0);

		//動いている側のポジションを戻す
		if(moveFlag_)
		{
			//ベクトルの計算
			returnPosition.x = position_.x - enemyPosition.x;
			returnPosition.z = position_.z - enemyPosition.z;
			D3DXVec3Normalize(&returnPosition, &returnPosition);
			returnPosition = returnPosition * HIT_DISTANCE;
			//戻す
			//一旦相手と同じ位置にポジションを移動してから…
			position_ = enemyPosition;
			//returnPositionベクトルの方向に離す
			position_ += returnPosition;
		}
	}
}

//進行方向を向ける
void Character::TurnForward(D3DXVECTOR3 &move)
{
	//縦方向の移動は考えない
	move.y = 0;

	//速度が0じゃない　＝　移動していたら
	//if (D3DXVec3Length(&move) > 0)
	{
		//移動ベクトルの長さを１にする
		D3DXVec3Normalize(&move, &move);

		//デフォルトの向いている方向
		D3DXVECTOR3 front = D3DXVECTOR3(0, 0, 1);

		//この後のacos関数に1未満
		if (D3DXVec3Dot(&move, &front) < 1 && D3DXVec3Dot(&move, &front) > -1)
		{
			//デフォルトの向きから見て、現在の進行方向への角度
			float angle = acos(D3DXVec3Dot(&move, &front));


			//デフォルトの向きから見て、現在の進行方向への角度

			if (D3DXVec3Dot(&move, &front) < 1 && D3DXVec3Dot(&move, &front) > -1)
			{
				float angle = acos(D3DXVec3Dot(&move, &front));
				//外積を使って、左右どちらに回転させればいいか求める
				D3DXVECTOR3 cross;
				D3DXVec3Cross(&cross, &move, &front);

				//逆向き
				if (cross.y > 0)
				{
					angle *= -1;
				}

				//回転させる（angleは単位がラジアンなので注意）
				rotate_.y = D3DXToDegree(angle);
			}
		}
	}
}

void Character::TargetMove(D3DXVECTOR3& moveX, D3DXVECTOR3& moveZ)
{
	//Y軸回転に合わせて移動出来るようにする
	D3DXMATRIX mRotZ, mRotX;
	D3DXMatrixRotationY(&mRotZ, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationY(&mRotX, D3DXToRadian(rotate_.y));

	//Z軸移動ベクトル
	D3DXVec3TransformCoord(&moveZ, &D3DXVECTOR3(0, 0, 0.1f), &mRotZ);

	//X軸移動ベクトル
	D3DXVec3TransformCoord(&moveX, &D3DXVECTOR3(0.1f, 0, 0), &mRotX);
}

//void Character::CameraSet()
//{

//}

void Character::MovementRestrictions()
{
	//奥移動制限
	if (position_.z >= WALL_POSITION)
	{
		position_.z = WALL_POSITION;
	}

	//前移動制限
	if (position_.z <= -WALL_POSITION)
	{
		position_.z = -WALL_POSITION;
	}

	//右移動制限
	if (position_.x >= WALL_POSITION)
	{
		position_.x = WALL_POSITION;
	}

	//左移動制限
	if (position_.x <= -WALL_POSITION)
	{
		position_.x = -WALL_POSITION;
	}
}

//かどわき作：モデルのアニメーション
void Character::ModelChangeActivity()
{
	//こちらはガードの位置を変えた時
	if (currentStance_ != nextStance_)
	{
		//上ガードに変更した時
		if (nextStance_ == UP)
		{
			//上ガード！
			Model::SetAnimFrame(hModel_, 0, 60, 1.5f);
		}
		//下ガードに変更した時
		else
		{
			//下ガード！
			Model::SetAnimFrame(hModel_, 70, 130, 1.5f);
		}

		//今の状態に次の状態を入れる
		currentStance_ = nextStance_;
	}


	//今の状態と次の状態が違うとき
	//要するに、もし今が「立ち」で
	//次が「パンチ」の行動の時に入る
	if (currentState_ != nextState_)
	{
		//操作時一連の行動が終わった
		if (nextState_ == STANDARD)
		{
			//かどわき作：移動時のアニメーション
			MoveMotion();
		}
		//クールタイム中
		else if (nextState_ == COOLTIME)
		{
			//ガードが上の時
			if (currentStance_ == UP)
			{
				//次の状態によって処理を分ける
				switch (currentState_)
				{
				case PUNCH:	//上パンチを戻す
					Model::SetAnimFrame(hModel_, 140, 205, 2.5f);
					break;

				case STRONG_PUNCH:	//上チャージパンチを戻す
					Model::SetAnimFrame(hModel_, 350, 415, 2.5f);
					break;
				}
			}
			//ガードが下の時
			else
			{
				//次の状態によって処理を分ける
				switch (currentState_)
				{
				case PUNCH:	//下パンチを戻す
					Model::SetAnimFrame(hModel_, 210, 275, 2.5f);
					break;

				case STRONG_PUNCH:	//下チャージパンチを戻す
					Model::SetAnimFrame(hModel_, 490, 555, 2.5f);
					break;
				}
			}
		}

		//今の状態が「立ち」か「クールタイム」か「無敵状態」の時
		if (currentState_ == STANDARD || currentState_ == COOLTIME)
		{
			//ガードが上の時
			if (currentStance_ == UP)
			{
				//次の状態によって処理を分ける
				switch (nextState_)
				{
				case PUNCH:	//上パンチ
					Model::SetAnimFrame(hModel_, 140, 140, 1.0f);
					break;

				case CHARG:	//上チャージ
					Model::SetAnimFrame(hModel_, 280, 345, 2.0f);
					break;

				case COUNTER: //カウンター
					Model::SetAnimFrame(hModel_, 560, 625, 1.5f);
					break;
				}
			}
			//ガードが下の時
			else
			{
				//次の状態によって処理を分ける
				switch (nextState_)
				{
				case PUNCH:	//下パンチ
					Model::SetAnimFrame(hModel_, 210, 210, 1.0f);
					break;

				case CHARG:	//下チャージ
					Model::SetAnimFrame(hModel_, 420, 485, 2.0f);
					break;

				case COUNTER: //カウンター
					Model::SetAnimFrame(hModel_, 560, 625, 1.5f);
					break;
				}
			}
		}
		//今の状態が「チャージ中」次の状態が「チャージパンチ」の時
		//要するにチャージ完了！
		if (currentState_ == CHARG && nextState_ == STRONG_PUNCH)
		{
			//ガードが上の時
			if (currentStance_ == UP)
			{
				//上チャージパンチ！
				Model::SetAnimFrame(hModel_, 350, 350, 1.0f);
			}
			//ガードが下の時
			else
			{
				//下チャージパンチ！
				Model::SetAnimFrame(hModel_, 490, 490, 1.0f);
			}
		}

		//今の状態が「カウンター」次の状態が「カウンターパンチ」の時
		//要するにカウンター成功時のパンチ！
		if (currentState_ == COUNTER && nextState_ == COUNTER_PUNCH)
		{
			//ガードが上の時
			if (currentStance_ == UP)
			{
				//上カウンターパンチー！
				Model::SetAnimFrame(hModel_, 350, 415, 2.5f);
			}
			//ガードが下の時
			else
			{
				//下カウンターパンチー！
				Model::SetAnimFrame(hModel_, 490, 555, 2.5f);
			}
		}

		//今の状態が「カウンター」次の状態が「弱点」の時
		//要するにカウンター失敗時の反動！
		if (currentState_ == COUNTER && nextState_ == WEAK)
		{
			//隙だらけだお☆
			Model::SetAnimFrame(hModel_, 630, 695, 1.0f);
		}

		//次の状態がのけぞりの時
		if (nextState_ == KNOCK_BACK)
		{
			//のけぞりモーション
			Model::SetAnimFrame(hModel_, 700, 765, 3.5f);
		}
		
		//今の状態に次の状態を入れる
		currentState_ = nextState_;
	}

	//こちらは移動時のアニメーションを変更
	//何もしてない状態にだけ入る
	if (currentMoveState_ != nextMoveState_ && nextState_ == STANDARD)
	{
		//かどわき作：移動時のアニメーション
		MoveMotion();

		//移動ステートを統一
		currentMoveState_ = nextMoveState_;
	}
	//かどわき作：アニメーションのストップ
	StopMotion();
}

void Character::MoveMotion()
{
	//移動時の状態によって処理を分ける
	switch (nextMoveState_)
	{
	//case MOVE_FRONT:	//前に移動
	//	//ガードが上
	//	if (currentStance_ == UP)
	//	{
	//		//上前移動
	//		Model::SetAnimFrame(hModel_, 140, 190, 1.5f);
	//	}
	//	//ガードが下
	//	else
	//	{
	//		//下前移動
	//		Model::SetAnimFrame(hModel_, 210, 260, 1.5f);
	//	}
	//	break;

	//case MOVE_BACK:	//後ろに移動
	//	//ガードが上
	//	if (currentStance_ == UP)
	//	{
	//		//上後移動
	//		Model::SetAnimFrame(hModel_, 350, 400, 1.5f);
	//	}
	//	//ガードが下
	//	else
	//	{
	//		//下後移動
	//		Model::SetAnimFrame(hModel_, 490, 540, 1.5f);
	//	}
	//	break;

	//case MOVE_LEFT:	//左に移動
	//	//ガードが上
	//	if (currentStance_ == UP)
	//	{
	//		//上左移動
	//		Model::SetAnimFrame(hModel_, 300, 320, 1.5f);
	//	}
	//	//ガードが下
	//	else
	//	{
	//		//下左移動
	//		Model::SetAnimFrame(hModel_, 450, 470, 1.5f);
	//	}
	//	break;

	//case MOVE_RIGHT:	//右に移動
	//	//ガードが上
	//	if (currentStance_ == UP)
	//	{
	//		//上右移動
	//		Model::SetAnimFrame(hModel_, 700, 730, 1.5f);
	//	}
	//	//ガードが上
	//	else
	//	{
	//		//下右移動
	//		Model::SetAnimFrame(hModel_, 560, 575, 1.5f);
	//	}
	//	break;

	default:	//立ち止まってる
		//ガードが上
		if (currentStance_ == UP)
		{
			//上立ち
			Model::SetAnimFrame(hModel_, 0, 60, 1.5f);
		}
		//ガードが下
		else
		{
			//下立ち
			Model::SetAnimFrame(hModel_, 70, 130, 1.5f);
		}
		break;
	}
}

//かどわき作：アニメーションのストップ
void Character::StopMotion()
{
	//上パンチ終了時…
	if (Model::GetAnimFrame(hModel_) >= 200 &&
		Model::GetAnimFrame(hModel_) <= 205)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 200, 200, 1.0f);
	}
	//下パンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 270 &&
		Model::GetAnimFrame(hModel_) <= 275)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 270, 270, 1.0f);
	}
	//上チャージ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 340 &&
		Model::GetAnimFrame(hModel_) <= 345)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 340, 340, 1.0f);
	}
	//上チャージパンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 410 &&
		Model::GetAnimFrame(hModel_) <= 415)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 410, 410, 1.0f);
	}
	//下チャージ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 480 &&
		Model::GetAnimFrame(hModel_) <= 485)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 480, 480, 1.0f);
	}
	//下チャージパンチ終了時…
	else if (Model::GetAnimFrame(hModel_) >= 550 &&
		Model::GetAnimFrame(hModel_) <= 555)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 550, 550, 1.0f);
	}
	//カウンター終了時…
	else if (Model::GetAnimFrame(hModel_) >= 620 &&
		Model::GetAnimFrame(hModel_) <= 625)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 620, 620, 1.0f);
	}
	//ウィーク終了時…
	else if (Model::GetAnimFrame(hModel_) >= 690 &&
		Model::GetAnimFrame(hModel_) <= 695)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 690, 690, 1.0f);
	}
	//のけぞり終了時…
	else if (Model::GetAnimFrame(hModel_) >= 760 &&
		Model::GetAnimFrame(hModel_) <= 765)
	{
		//そのフレームで固定
		Model::SetAnimFrame(hModel_, 760, 760, 1.0f);
	}
}
