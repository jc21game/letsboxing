#pragma once
#include "Engine/IGameObject.h"

class BoxCollider;
class SphereCollider;

//◆◆◆を管理するクラス
class Character : public IGameObject
{
protected:

	int hp_;		//体力
	int pp_;		//強攻撃のゲージ　とりあえずPowerPointでppにした

	bool timeFlag_;	//カウンターの時間やパンチのクールタイムを計っている時true
	double time_;	//カウンターの時間やパンチのクールタイムを計る時に使う

	enum STANCE		//構え　0で中段1で上段の構えになる
	{
		UP,
		DOWN
	}stance_;

	enum STATE
	{
		STANDARD = 0,
		PUNCH,
		CHARG,				//STRONG_PUNCHのチャージ状態
		STRONG_PUNCH,
		COOLTIME,			//パンチのクールタイム状態
		COUNTER,
		WEAK				//カウンターが失敗した時の隙が出来ている状態
	} state_;

	BoxCollider* bodyCollider_;			//体の当たり判定用コライダー
	SphereCollider* punchCollider_;		//パンチ用のコライダー
	D3DXVECTOR3 punchColliderPosition_;	//攻撃用コライダーを動かすときに使う
	double punchColliderForwardPosition_;	//攻撃用コライダーがどれくらい前方に出ているか

	std::string opponent_;	//対戦相手の名前　PlayerCharacterから見ればNonPlayerCharacter

	bool standardFlag_;		//攻撃が当たった時などに次のフレームでstate_をSTANDATDにするフラグ
	bool coolTimeFlag_;		//standardFlag_と同じ

	//胴体同士が接触していた時めり込まないようにする処理をする時に使う
	bool moveFlag_;			//動いている時true
	
	Character* enemyCharacter_;	//相手のポインタを入れる

public:
	//コンストラクタ
	Character(IGameObject* parent);
	Character(IGameObject* parent, std::string name);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	void Punch();			//パンチ
	void Charg();			//強パンチ
	void StrongPunch();		//強パンチ
	void CoolTime();		//攻撃のクールタイム
	void Counter();			//カウンター
	void Weak();			//カウンターが失敗したときの隙
	void StanceChange(STANCE stance);	//ガードする位置変更

	virtual void PunchCommand() = 0;		//パンチを発動する条件　プレイヤーならキーボードでの処理になるしNPCならAIが処理する
	virtual void StrongPunchCommand() = 0;	//強パンチを発動する条件　パンチ同様
	virtual void CounterCommand() = 0;		//カウンターを発動する条件　パンチ同様
	virtual void StanceChangeCommand() = 0;		//構え変更条件　パンチ同様
	virtual void MoveCommand() = 0;				//移動　パンチ同様

	STATE GetState()
	{
		return state_;
	}

	void SetState(STATE state)
	{
		state_ = state;
	}

	STANCE GetStance()
	{
		return stance_;
	}

	void SetStandardFlag(bool standardFlag)
	{
		standardFlag_ = standardFlag;
	}

	void SetCoolTimeFlag(bool coolTimeFlag)
	{
		coolTimeFlag_ = coolTimeFlag;
	}

	bool GetMoveFlag()
	{
		return moveFlag_;
	}

	void returnPosition(D3DXVECTOR3 enemyPosition, bool enemyMoveFlag);
};