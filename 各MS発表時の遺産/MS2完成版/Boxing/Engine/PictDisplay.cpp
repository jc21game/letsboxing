#include <string>
#include "PictDisplay.h"
#include "Image.h"


//コンストラクタ
//名前と画像の原点をセット
PictDisplay::PictDisplay(IGameObject * parent, std::string fileName, std::string selectFileName,
	float originX, float originY)
	:IGameObject(parent, "PictDisplay"),display_(NORMAL), fileName_(fileName), selectFileName_(selectFileName),
	originX_(originX), originY_(originY)
{
	//画像データのコンストラクタ
	for (int i = 0; i < MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
PictDisplay::~PictDisplay()
{
}

//初期化
void PictDisplay::Initialize()
{
	//画像データのロード
	hPict_[NORMAL] = Image::Load(fileName_);
	assert(hPict_[NORMAL] >= 0);

	//選択されてる時のファイル名があれば
	if (selectFileName_ != "")
	{
		//選択されてる時の画像データのロード
		hPict_[SELECT] = Image::Load(selectFileName_);
		assert(hPict_[SELECT] >= 0);
	}

	//画像の原点変更
	position_ = D3DXVECTOR3(originX_, originY_, 0);
}

//更新
void PictDisplay::Update()
{
}

//描画
void PictDisplay::Draw()
{
	//display_の状態により変化
	Image::SetMatrix(hPict_[display_], worldMatrix_);
	Image::Draw(hPict_[display_]);
}

//開放
void PictDisplay::Release()
{
}