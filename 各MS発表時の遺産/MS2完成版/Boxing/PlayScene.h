#pragma once
#include "Engine/global.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"

class Camera;
class PictDisplay;
class PlayerCharacter;
class NonPlayerCharacter;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	enum PICT {
		MY_HP,
		ENEMY_HP,
		MY_SKILL,
		ENEMY_SKILL,
		MY_NAME,
		ENEMY_NAME,
		ROUND,
		TIMER,
		MAX
	};
	Camera* pCamera_;			//仮カメラ
	bool nowPlayScene_;			//プレイシーンを操作できるかどうか
	PictDisplay* ui_[MAX];		//UIたち

	PlayerCharacter* pPlayerCharacter_;
	NonPlayerCharacter* pNonPlayerCharacter_;

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //プレイシーンを操作できるかのフラグをセット
  //引数：nowPlayScene プレイシーンの操作可か？
  void SetNowPlayScene(bool nowPlayScene)
  {
	  nowPlayScene_ = nowPlayScene;
  };

  //衝突
  void OnCollision(IGameObject *pTarget) override;

  PlayerCharacter* GetPlayerCharacter()
  {
	  return pPlayerCharacter_;
  }
  
  NonPlayerCharacter* GetNonPlayerCharacter()
  {
	  return pNonPlayerCharacter_;
  }
};