#include "PlayerCharacter.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"

//コンストラクタ
PlayerCharacter::PlayerCharacter(IGameObject * parent)
	:Character(parent, "PlayerCharacter")
{
}

//デストラクタ
PlayerCharacter::~PlayerCharacter()
{
}

//初期化
void PlayerCharacter::Initialize()
{
	//胴体コライダーを作成
	bodyCollider_ = new BoxCollider("bodyCollider", D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(2, 4, 2));
	AddCollider(bodyCollider_);

	//対戦相手の名前
	opponent_ = "NonPlayerCharacter";
	position_.y = 4;
	position_.z = -2;

	hp_ = 4;
}

////更新
//void PlayerCharacter::Update()
//{
//}
//
////描画
//void PlayerCharacter::Draw()
//{
//}
//
////開放
//void PlayerCharacter::Release()
//{
//}

//void PlayerCharacter::OnCollision(IGameObject * pTarget)
//{
//	SubOnCollision(pTarget);
//}

void PlayerCharacter::PunchCommand()
{
	if (Input::IsKeyDown(DIK_J))
	{
		state_ = PUNCH;
	}
}

void PlayerCharacter::StrongPunchCommand()
{
	if (Input::IsKeyDown(DIK_K))
	{
		state_ = CHARG;
	}
}

void PlayerCharacter::CounterCommand()
{
	if (Input::IsKeyDown(DIK_L))
	{
		state_ = COUNTER;
	}
}

void PlayerCharacter::StanceChangeCommand()
{
	if (Input::IsKeyDown(DIK_UP))
	{
		stance_ = UP;
	}
	if (Input::IsKeyDown(DIK_DOWN))
	{
		stance_ = DOWN;
	}
}

void PlayerCharacter::MoveCommand()
{
	//移動前
	if (Input::IsKey(DIK_W))
	{
		moveFlag_ = true;
		position_.z += 0.2;
	}
	//移動後ろ
	if (Input::IsKey(DIK_S))
	{
		moveFlag_ = true;
		position_.z -= 0.2;
	}
	//移動右
	if (Input::IsKey(DIK_D))
	{
		moveFlag_ = true;
		position_.x += 0.2;
	}
	//移動左
	if (Input::IsKey(DIK_A))
	{
		moveFlag_ = true;
		position_.x -= 0.2;
	}
}
