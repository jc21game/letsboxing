#include "OperationMemoScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"

//コンストラクタ
OperationMemoScene::OperationMemoScene(IGameObject * parent)
	: IGameObject(parent, "OperationMemoScene"), hPict_(-1)
{
}

//初期化
void OperationMemoScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	CreateGameObject<PictDisplay>(this, "data/OperationMemoLogo.png");
}

//更新
void OperationMemoScene::Update()
{
	//ESCキーでメニュー画面
	if (Input::IsKeyDown(DIK_ESCAPE) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 1))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void OperationMemoScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OperationMemoScene::Release()
{
}