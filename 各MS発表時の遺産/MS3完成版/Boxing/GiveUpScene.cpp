#include "GiveUpScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"

//コンストラクタ
GiveUpScene::GiveUpScene(IGameObject * parent)
	: IGameObject(parent, "GiveUpScene"), hPict_(-1),
	state_(STATE_YES), possible_(false)
{
}

//初期化
void GiveUpScene::Initialize()
{
	//背景画像データのロード
	hPict_ = Image::Load("data/GiveUpScene.png");
	assert(hPict_ >= 0);

	//ファイル名
	const std::string fileName[MAX]{
		"data/YesLogo.png",
		"data/NoLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}
}

//更新
void GiveUpScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//YESロゴ選択時
	case STATE_YES:
		//ロゴが黄色くなる
		pPictArray_[YES_LOGO]->Selection();

		//Enterでメニュー画面へ移動
		if (Input::IsKeyDown(DIK_RETURN)||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			//プレイヤーの勝ち数をリセット
			g.playerOneWinNum = 0;
			g.playerTwoWinNum = 0;

			//メニューに戻る
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);
		}
		//左キー右キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1)
			)
		{
			state_ = STATE_NO;
			pPictArray_[YES_LOGO]->UnSelection();
		}	
		
		break;

	//NOロゴ選択時
	case STATE_NO:
		//ロゴが黄色くなる
		pPictArray_[NO_LOGO]->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			KillMe();
		}
		//左キー右キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_YES;
			pPictArray_[NO_LOGO]->UnSelection();
		}

		break;
	}

	//Escキーで前の画面（試合画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE) && possible_ ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 0) && possible_ ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 1) && possible_)
	{
		KillMe();
	}
	//Escキーの誤操作防止
	possible_ = true;
}

//描画
void GiveUpScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GiveUpScene::Release()
{
}