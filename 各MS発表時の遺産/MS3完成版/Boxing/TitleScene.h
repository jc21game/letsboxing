#pragma once
#include "Engine/Global.h"

class PictDisplay;

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		LETS_BOXING,
		START_LOGO,
		MAX
	};
	PictDisplay* pPictArray_[MAX];

	time_t sceneStartTime_;		//このシーンになった時の時間
	bool gameStartFlag_;		//ゲームスタートのロゴが表示されているか

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//Escキーでゲーム終了
	void EscapeGameEnd();

	//Enterキーの処理
	void EnterProcess();

	//秒数の処理でスプラッシュ画面に戻る
	void SecondsSplashBack(double difference);

	//経過時間測定
	double ElapsedTime();

	//ゲームスタート描画
	void GameStartDraw(D3DXVECTOR3 &logoPos);

	//ロゴの移動
	void LogoMove(D3DXVECTOR3 &logoPos);

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
