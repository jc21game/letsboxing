#pragma once
#include "Engine/IGameObject.h"
#include "Character.h"

//◆◆◆を管理するクラス
class NonPlayerCharacter : public Character
{
	int hModel_;	//モデル番号
	int hPict_;    //画像番号
	D3DXMATRIX view_; //メンバビュー

public:
	//コンストラクタ
	NonPlayerCharacter(IGameObject* parent);

	//デストラクタ
	~NonPlayerCharacter();

	//初期化
	void Initialize() override;

	//更新
	//void Update() override;

	//描画
	//void Draw() override;
	void ChildDraw() override;

	////開放
	//void Release() override;

	//void OnCollision(IGameObject* pTarget) override;

	void PunchCommand() override;		//パンチを発動する条件　プレイヤーならキーボードでの処理になるしNPCならAIが処理する
	void StrongPunchCommand() override;	//強パンチを発動する条件　パンチ同様
	void CounterCommand() override;		//カウンターを発動する条件　パンチ同様
	void StanceChangeCommand() override;		//構え変更条件　パンチ同様
	void MoveCommand() override;				//移動　パンチ同様
	void SetCamera() override;

	D3DXMATRIX GetView()
	{
		return view_;
	}
	int GetHp()
	{
		return hp_;
	}

};