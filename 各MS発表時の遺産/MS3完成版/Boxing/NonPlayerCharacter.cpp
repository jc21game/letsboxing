#include "NonPlayerCharacter.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"
#include "Engine/Direct3D.h"
#include "Engine/Image.h"
#include "PlayScene.h"
#include "PlayerCharacter.h"

//コンストラクタ
NonPlayerCharacter::NonPlayerCharacter(IGameObject * parent)
	:Character(parent, "NonPlayerCharacter"), hPict_(-1)
{
}

//デストラクタ
NonPlayerCharacter::~NonPlayerCharacter()
{
}

//初期化
void NonPlayerCharacter::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/HP/HP11.png");
	assert(hPict_ >= 0);

	//胴体コライダーを作成
	bodyCollider_ = new BoxCollider("bodyCollider", D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(2, 4, 1));
	AddCollider(bodyCollider_);

	//対戦相手の名前
	opponent_ = "PlayerCharacter";

	rotate_.y = 180;
	position_.y = 4;
	position_.z = 2;

	hp_ = 10;
}

//更新
//void NonPlayerCharacter::Update()
//{
//	PunchCommand();
//	StrongPunchCommand();
//	CounterCommand();
//	StanceChangeCommand();
//	MoveCommand();
//	
//}

////描画
//void NonPlayerCharacter::Draw()
//{
//}
void NonPlayerCharacter::ChildDraw()
{

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 510, 0, 0);

	D3DXMATRIX s;
	D3DXMatrixScaling(&s, ((float)hp_ / 10), 1, 1);

	m = s * m;

	//Image::SetMatrix(hPict_, m);
	//Image::Draw(hPict_);

	if (hp_ > 6)
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(0, 0.6f, 0.9f, 1));
	}
	else if (hp_ > 3)
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(0.9f, 0.9f, 0, 1));
	}
	else
	{
		Image::SetMatrix(hPict_, m);
		Image::Draw(hPict_, D3DXCOLOR(1, 0, 0, 1));
	}
}

////開放
//void NonPlayerCharacter::Release()
//{
//}

//void NonPlayerCharacter::OnCollision(IGameObject * pTarget)
//{
//	SubOnCollision(pTarget);
//}

void NonPlayerCharacter::PunchCommand()
{
	if (Input::IsKeyDown(DIK_I))
	{
		nextState_ = PUNCH;
	}

	//パッド
	if (Input::IsPadButton(XINPUT_GAMEPAD_B, 1))
	{
		nextState_ = PUNCH;
	}
}

void NonPlayerCharacter::StrongPunchCommand()
{
	if (Input::IsKeyDown(DIK_O))
	{
		nextState_ = CHARG;
	}

	//パッド
	if (Input::IsPadButton(XINPUT_GAMEPAD_Y, 1))
	{
		nextState_ = CHARG;
	}
}

void NonPlayerCharacter::CounterCommand()
{
	if (Input::IsKeyDown(DIK_P))
	{
		nextState_ = COUNTER;
	}

	//パッド
	if (Input::IsPadButton(XINPUT_GAMEPAD_A, 1))
	{
		nextState_ = COUNTER;
	}
}

void NonPlayerCharacter::StanceChangeCommand()
{
	//パッド
	if (Input::IsPadButton(XINPUT_GAMEPAD_LEFT_SHOULDER, 1))
	{
		nextStance_ = DOWN;
	}
	if (Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, 1))
	{
		nextStance_ = UP;
	}
}

void NonPlayerCharacter::MoveCommand()
{
	D3DXVECTOR3 move;
	//ベクトル変形させるために行列を作る
	D3DXMATRIX ym;
	D3DXMatrixRotationY(&ym, D3DXToRadian(rotate_.y));
	D3DXMATRIX xm;
	D3DXMatrixRotationX(&xm, D3DXToRadian(rotate_.x));
	D3DXMATRIX zm;
	D3DXMatrixRotationZ(&zm, D3DXToRadian(rotate_.z));
	D3DXMATRIX wm;
	wm = ym * xm * zm;

	////前回の位置を記憶しておく
	//D3DXVECTOR3 prevPos = position_;

	//キーボード
	//移動前
	if (Input::IsKey(DIK_UP))
	{
		moveFlag_ = true;
		//TPS
		//position_.z += 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0 , 0, SPEED), &wm);
		position_ += move;
	}
	//移動後ろ
	if (Input::IsKey(DIK_DOWN))
	{
		moveFlag_ = true;
		//TPS
		//position_.z -= 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, -SPEED), &wm);
		position_ += move;
	}
	//移動右
	if (Input::IsKey(DIK_RIGHT))
	{		
		moveFlag_ = true;
		//TPS
		//position_.x += 0.2;
		
		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(SPEED, 0, 0), &wm);
		position_ += move;
	}
	//移動左
	if (Input::IsKey(DIK_LEFT))
	{
		moveFlag_ = true;
		//TPS
		//position_.x -= 0.2;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(-SPEED, 0, 0), &wm);
		position_ += move;
	}

	//パッド
	//コントローラー左スティック
	{
		moveFlag_ = true;
		//TPS
		//position_.x += Input::GetPadStickL(1).x * SPEED;
		//position_.z += Input::GetPadStickL(1).y * SPEED;

		//FPS
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(Input::GetPadStickL(1).x * SPEED, 0, 0), &wm);
		position_ += move;
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, Input::GetPadStickL(1).y * SPEED), &wm);
		position_ += move;
	}

	////前回の位置と現在の位置の差分から
	////移動ベクトルを求める
	//D3DXVECTOR3 vecMove = position_ - prevPos;

	////進行方向を向ける
	//TurnForward(vecMove);

	//敵の方向を見る
	//if (enemyCharacter_ != NULL)
	//{
	//	TurnForward((enemyCharacter_->GetPosition() - position_));
	//}

	//壁にめり込まないようにする処理
	MovementRestrictions();
}

void NonPlayerCharacter::SetCamera()
{
	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &D3DXVECTOR3(0, 2, 0), &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &D3DXVECTOR3(0, 1.5f, 1), &worldMatrix_);

	//ビュー行列
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);

	
}
	