#include <string>
#include "PictDisplay.h"
#include "Image.h"

const float alphaValueSpeed_ = 0.03f;

//コンストラクタ
PictDisplay::PictDisplay(IGameObject * parent, std::string fileName)
	:IGameObject(parent, "PictDisplay"), hPict_(-1) ,display_(NORMAL), fileName_(fileName),
	state_(LD_DARK),
	alphaValue_(1.0f),
	resetAlphaValueFlag_(false)
{
}

//デストラクタ
PictDisplay::~PictDisplay()
{
}

//初期化
void PictDisplay::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load(fileName_);
	assert(hPict_ >= 0);
}

//更新
void PictDisplay::Update()
{
	//現在の表示状態
	switch (display_)
	{
	//通常の画像の時（普通に画像を使う場合はこっちの処理だけ）
	case NORMAL:

		//リセットフラグがONなら
		//外部からアルファ値をいじるためのフラグ変数
		if (resetAlphaValueFlag_)
		{
			//アルファ値を戻す
			alphaValue_ = 1.0f;
			resetAlphaValueFlag_ = false;
		}

		break;

	//選択されている画像の時
	case SELECT:
		//アルファ値フラグをON！
		resetAlphaValueFlag_ = true;

		//明暗の状態
		switch (state_)
		{
		case LD_BRIGHT:
			//画像を明るくする処理
			alphaValue_ += alphaValueSpeed_;


			//もし最大まで明るくしたら
			if (alphaValue_ > 1.0f)
			{
				//状態変化
				state_ = LD_DARK;
			}
			break;

		case LD_DARK:
			//画像を暗くする処理
			alphaValue_ -= alphaValueSpeed_;

			//もし最大まで暗くしたら
			if (alphaValue_ < 0)
			{
				//状態変化
				state_ = LD_BRIGHT;
			}
			break;
		}

		break;
	}
}

//描画
void PictDisplay::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_, alphaValue_);
}

//開放
void PictDisplay::Release()
{
}