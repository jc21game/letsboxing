#pragma once
#include "Engine/Global.h"

class PictDisplay;

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//ロゴ画像
	enum PICTURE {
		YES_LOGO,
		NO_LOGO,
		MAX
	};
	PictDisplay* pPictArray_[MAX];

	enum GAME {
		WIN_ONRY,
		WIN_REMATCH,
		WIN_MAX
	};

	//状態の種類
	enum STATE {
		STATE_YES,	//YES
		STATE_NO	//NO
	} state_;

	int hPict_[WIN_MAX];	//画像番号
	int change;             //二つの画像を配列に入れて切り替えるために使ってる
	bool winFlag_;			//勝ったかフラグ

	int second;             //時を止める
	int LogoCreateFlag;     //時間差を利用するために使ってます
	bool nowPlayScene_;		//プレイシーンを操作できるかどうか

public:

	//コンストラクタ
	//引数１：parent  親オブジェクト（SceneManager）
	//引数２：winFlag 試合に買ったかどうか
	ResultScene(IGameObject* parent, bool  winFlag);

	

	//初期化
	void Initialize() override;

	//時止め
	void TimeStop();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイシーンを操作できるかのフラグをセット
 //引数：nowPlayScene プレイシーンの操作可か？
	void SetNowPlayScene(bool nowPlayScene)
	{
		nowPlayScene_ = nowPlayScene;
	};
};