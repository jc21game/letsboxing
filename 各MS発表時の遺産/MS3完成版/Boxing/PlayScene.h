#pragma once
#include <string>
#include "Engine/global.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Text.h"

class Camera;
class PictDisplay;
class PlayerCharacter;
class NonPlayerCharacter;
class ResultScene;

using namespace std;


//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//勝敗判定
	enum JUDGMENT {
		WIN_EMPTY,	//勝敗判定なし
		WIN_PLAYER_ONE,	//1PWIN
		WIN_PLAYER_TWO	//2PWIN
	} winJud_;

	//現在のシーンの状態
	enum SCENE_STATE {
		STATE_START,		//レディー…ゴー！
		STATE_PLAY,			//試合中
		STATE_GIVEUP_SCENE,	//ギブアップシーン中
		STATE_RESULT_SCENE,	//リザルトシーン中
		STATE_KO,			//KO！終了！
		STATE_JUDGMENT_WIN	//WIN表示
	} sceneState_;

	//スタートカウント時の処理
	enum START_COUNT_PROCESS {
		START_THREE,	//3秒前
		START_TWO,		//2秒前
		START_ONE		//1秒前
	} startCount_;

	string round_[3];
	string point_[3];
	Text* pText_;
	int hModel_;		//モデル番号
	int hPict_;			//画像番号

	//ronud用
	int roundsu_;
	int count_;

	Camera* pCamera_;			//カメラ
	float cameraPosX_, cameraPosY_, cameraPosZ_;	//カメラの位置
	float cameraTarX_, cameraTarY_, cameraTarZ_;	//カメラの向き
	
	PlayerCharacter* pPlayerCharacter_;
	NonPlayerCharacter* pNonPlayerCharacter_;

	//表示する数値を格納する
	string second_[11];
	string minute_[11];

	//1の位を数える変数
	int sec_;
	//10の位
	int tensec_;
	//分カウント
	int min_;
	//fremをカウントする
	int frame_;

	bool pointPlusFlag_;
	
	bool timeFlag_;		//タイムリセットフラグ
	double time_;		//クロック関数で時間を保存

	int ehp_;
	int php_;

	float angle_;			//角度
	bool cameraResetFlag_;	//カメラの位置と向きをリセットする用のフラグ

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //タイマーとラウンド数の配列中身初期化
  void TimerRoundInitialize();

  //更新
  void Update() override;

  //一度だけカメラの位置をリセット
  void CameraInitlaize();

  //Escキーでギブアップ画面
  void EscGiveUp();

  //勝ったときの処理
  void WinProcess(PlayerCharacter* pPlayerCharacter, NonPlayerCharacter* pNonPlayerCharacter);

  //クロック関数を使ってタイム変数を初期化する
  void TimeClockReset();

  //操作可能かを切り替える
  void IsInputCommand();

  //秒数によってカメラの位置が変わる
  void StartCameraMove();

  //描画
  void Draw() override;

  void DrawSub()override;
  //開放
  void Release() override;

  //カメラを原点を主軸に回転させる
  void CircleRotate();

  PlayerCharacter* GetPlayerCharacter()
  {
	  return pPlayerCharacter_;
  };
  
  NonPlayerCharacter* GetNonPlayerCharacter()
  {
	  return pNonPlayerCharacter_;
  };

  void timecnt(PlayerCharacter* pPlayerCharacter, NonPlayerCharacter* pNonPlayerCharacter);
};