#include "MenuScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene"), hPict_(-1),
	state_(STATE_MATCH)
{
}

//初期化
void MenuScene::Initialize()
{
	//背景画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	//ファイル名
	const std::string fileName[MAX]{
		"data/MatchLogo.png",
		"data/OperationMemoLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}
}

//更新
void MenuScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//試合ロゴ選択時
	case STATE_MATCH:
		//ロゴが黄色くなる
		pPictArray_[MATCH_LOGO]->Selection();

		//Enterで難易度選択画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//左キー右キーを押したら操作説明ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_OPERATION_MEMO;
			pPictArray_[MATCH_LOGO]->UnSelection();
		}
		
		break;

	//操作説明ロゴ選択時
	case STATE_OPERATION_MEMO:
		//ロゴが黄色くなる
		pPictArray_[OPERATION_MATCH_LOGO]->Selection();

		//Enterで操作説明画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_OPERATION_MEMO);
		}
		//左キー右キーを押したら試合ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_MATCH;
			pPictArray_[OPERATION_MATCH_LOGO]->UnSelection();
		}
		break;
	}

	//Escキーで前の画面（タイトル画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE) ||
		//パッド操作　BACKボタン
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 0) ||
		Input::IsPadButtonDown(XINPUT_GAMEPAD_BACK, 1))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void MenuScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void MenuScene::Release()
{
}