#include "ResultScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"
#include "Engine/Audio.h"

// unko = WIN_REMATCH 

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent, bool winFlag)
	: IGameObject(parent, "ResultScene"), change(WIN_ONRY),
	winFlag_(winFlag)
	, second(2000)//ここで時を止める秒数を変えれる
	, LogoCreateFlag(0)//特定の箇所を一回通ったかどうかを確認するフラグ
	, nowPlayScene_(true)
{
	for (int i = 0; i < WIN_MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//初期化
void ResultScene::Initialize()
{
	
	//画像データのロード
	//勝ったかフラグが真なら勝ち画像表示
	if (winFlag_ == true)
	{
		hPict_[WIN_ONRY] = Image::Load("data/1PWin.png");
		assert(hPict_ >= 0);

		hPict_[WIN_REMATCH] = Image::Load("data/1PWinRematch.png");
		assert(hPict_ >= 0);
	}
	//それ以外なら負け画像
	else 
	{
		hPict_[WIN_ONRY] = Image::Load("data/2PWin.png");
		assert(hPict_ >= 0);

		hPict_[WIN_REMATCH] = Image::Load("data/2PWinRematch.png");
		assert(hPict_ >= 0);
	}

	//ファイル名
	const std::string fileName[MAX]{
		"data/YesLogo.png",
		"data/NoLogo.png"
	};

	//インスタンス作成
	for (int i = 0; i < MAX; i++)
	{
		pPictArray_[i] = CreateGameObject<PictDisplay>(this, fileName[i]);
	}

	//ここで初期の時に移らないように画面外に出している
	pPictArray_[YES_LOGO]->SetPosition(D3DXVECTOR3(2000, 0, 0)); 
	pPictArray_[NO_LOGO]->SetPosition(D3DXVECTOR3(2000, 0, 0));
}


//更新
void ResultScene::Update()
{

	if (LogoCreateFlag == 1)
	{
		//止めたタイミングで出したいからここでまた位置をいじる
		pPictArray_[YES_LOGO]->SetPosition(D3DXVECTOR3(0, 0, 0));
		pPictArray_[NO_LOGO]->SetPosition(D3DXVECTOR3(0, 0, 0));

		//時を止める
		TimeStop();

		//ここで表示したい画像を切り替えている
		change = WIN_REMATCH;

		//ロゴが黄色くなる
		pPictArray_[YES_LOGO]->Selection();
	}
	


	//選択されている状態
	switch (state_)
	{
		
	//YESロゴ選択時
	case STATE_YES:
		//ロゴが黄色くなる
		pPictArray_[YES_LOGO]->Selection();

	    LogoCreateFlag++;//ここを通ると二秒止められる
		
		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) || Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			//リロード
			SceneManager* pSceneManager;
			pSceneManager->ReloadScene();

			g.playerOneWinNum = NULL;
			g.playerTwoWinNum = NULL;

			//停止
			Audio::Stop("BGM4");
		}
		//左キー右キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_NO;
			pPictArray_[YES_LOGO]->UnSelection();
		}
		
		break;

	//NOロゴ選択時
	case STATE_NO:
		//ロゴが黄色くなる
		pPictArray_[NO_LOGO]->Selection();

		//Enterでメニュー画面へ移動
		if (Input::IsKeyDown(DIK_RETURN) ||
			//パッド操作　Aボタン
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_A, 1))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);

			g.playerOneWinNum = NULL;
			g.playerTwoWinNum = NULL;

			//停止
			Audio::Stop("BGM4");
		}
		//左キー右キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT) || Input::IsKeyDown(DIK_LEFT) ||
			//パッド操作　左右キー
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_LEFT, 1) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 0) ||
			Input::IsPadButtonDown(XINPUT_GAMEPAD_DPAD_RIGHT, 1))
		{
			state_ = STATE_YES;
			pPictArray_[NO_LOGO]->UnSelection();
		}
		
		break;
	}
}


void ResultScene::TimeStop()
{
	//時間を止める関数
	Sleep(second);
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_[change], worldMatrix_);
	Image::Draw(hPict_[change]);
}

//開放
void ResultScene::Release()
{
}