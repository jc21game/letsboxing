#include "DifficultyScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"



//コンストラクタ
DifficultyScene::DifficultyScene(IGameObject * parent)
	: IGameObject(parent, "DifficultyScene"),
	hPict_(-1),
	state_(STATE_EASY)	
{
}

//初期化
void DifficultyScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	easyLogo_ = CreateGameObject<PictDisplay>(this, "data/EasyLogo.png", "data/SelectEasyLogo.png", 100.0f, 540.0f);
	normalLogo_ = CreateGameObject<PictDisplay>(this, "data/NormalLogo.png", "data/SelectNormalLogo.png", 700.0f, 540.0f);
	hardLogo_ = CreateGameObject<PictDisplay>(this, "data/HardLogo.png", "data/SelectHardLogo.png", 1300.0f, 540.0f);
}

//更新
void DifficultyScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//弱いロゴ選択時
	case STATE_EASY:
		//ロゴが黄色くなる
		easyLogo_->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//右キーを押したら普通ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_NORMAL;
			easyLogo_->UnSelection();
		}
		//左キーを押したら強いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_HARD;
			easyLogo_->UnSelection();
		}
		break;

	//普通ロゴ選択時
	case STATE_NORMAL:
		//ロゴが黄色くなる
		normalLogo_->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//右キーを押したら強いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_HARD;
			normalLogo_->UnSelection();
		}
		//左キーを押したら弱いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_EASY;
			normalLogo_->UnSelection();
		}
		break;

	//強いロゴ選択時
	case STATE_HARD:
		//ロゴが黄色くなる
		hardLogo_->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_PLAY);
		}
		//右キーを押したら弱いロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_EASY;
			hardLogo_->UnSelection();
		}
		//左キーを押したら普通ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_NORMAL;
			hardLogo_->UnSelection();
		}
		break;
	}

	

	//Escキーで前の画面（メニュー画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void DifficultyScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void DifficultyScene::Release()
{
}