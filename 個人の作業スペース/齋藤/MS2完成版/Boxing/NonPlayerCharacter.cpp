#include "NonPlayerCharacter.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"

//コンストラクタ
NonPlayerCharacter::NonPlayerCharacter(IGameObject * parent)
	:Character(parent, "NonPlayerCharacter")
{
}

//デストラクタ
NonPlayerCharacter::~NonPlayerCharacter()
{
}

//初期化
void NonPlayerCharacter::Initialize()
{
	//胴体コライダーを作成
	bodyCollider_ = new BoxCollider("bodyCollider", D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(2, 4, 2));
	AddCollider(bodyCollider_);

	//対戦相手の名前
	opponent_ = "PlayerCharacter";

	rotate_.y = 180;
	position_.y = 4;
	position_.z = 2;

	hp_ = 4;
}

//更新
void NonPlayerCharacter::Update()
{

	PunchCommand();
	StrongPunchCommand();
	CounterCommand();
	MoveCommand();

	//移動制限
    MovementRestrictions(); 
}

////描画
//void NonPlayerCharacter::Draw()
//{
//}
//
////開放
//void NonPlayerCharacter::Release()
//{
//}

//void NonPlayerCharacter::OnCollision(IGameObject * pTarget)
//{
//	SubOnCollision(pTarget);
//}

void NonPlayerCharacter::PunchCommand()
{
	if (Input::IsKeyDown(DIK_I))
	{
		state_ = PUNCH;
	}
}

void NonPlayerCharacter::StrongPunchCommand()
{
	if (Input::IsKeyDown(DIK_O))
	{
		state_ = CHARG;
	}
}

void NonPlayerCharacter::CounterCommand()
{
	if (Input::IsKeyDown(DIK_P))
	{
		state_ = COUNTER;
	}
}

void NonPlayerCharacter::StanceChangeCommand()
{
}

void NonPlayerCharacter::MoveCommand()
{
	//移動左
	if (Input::IsKey(DIK_T))
	{
		position_.x -= 0.2;
	}
}

void NonPlayerCharacter::MovementRestrictions()
{
	//奥壁衝突
	if (position_.z >= 6)
	{
		position_.z = 6;
	}

	//前壁衝突
	if (position_.z <= -6)
	{
		position_.z = -6;
	}

	//右壁衝突
	if (position_.x >= 8)
	{
		position_.x = 8;
	}

	//左壁衝突
	if (position_.x <= -8)
	{
		position_.x = -8;
	}
}
