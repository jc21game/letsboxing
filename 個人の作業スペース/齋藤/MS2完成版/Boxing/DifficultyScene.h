#pragma once
#include "Engine/Global.h"

class PictDisplay;

//難易度選択シーンを管理するクラス
class DifficultyScene : public IGameObject
{
	//状態の種類
	enum STATE {
		STATE_EASY,		//弱い
		STATE_NORMAL,	//普通
		STATE_HARD		//強い
	} state_;

	int hPict_;					//画像番号
	PictDisplay* easyLogo_;		//弱いロゴ
	PictDisplay* normalLogo_;	//普通ロゴ
	PictDisplay* hardLogo_;		//強いロゴ

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	DifficultyScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};