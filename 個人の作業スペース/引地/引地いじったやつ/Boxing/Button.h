#pragma once
#include "Engine/ObjectTree/IGameObject.h"

#define W_WIDTH 1920
#define W_HEIGHT 1080

//ボタンを管理するクラス
class Button : public IGameObject
{
	int hPict_;				//画像番号
	std::string fileName_;	//ファイルの名前
	float originX_;			//原点X
	float originY_;			//原点Y
	bool sceneChangeFlag_;	//シーンを切り替えるフラグ
	bool reactionFlag_;		//押したら反応するかフラグ（基本は使わないのでtrue）

	static LPRECT ClientRect_;

public:
	//コンストラクタ
	Button(IGameObject* parent, std::string fileName,
		float originX, float originY);

	//デストラクタ
	~Button();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーン切り替えのフラグを返す
		//戻値：bool フラグ
	bool GetSceneChangeFlag()
	{
		return sceneChangeFlag_;
	};

	//シーン切り替えのフラグをセットする
	//引数：sceneChangeFlag フラグを受け取る
	void SetSceneChangeFlag(bool sceneChangeFlag)
	{
		sceneChangeFlag_ = sceneChangeFlag;
	};

	//ボタンが反応したくない時に使うセット関数
	//引数：reactionFlag フラグを受け取る
	void SetReactionFlag(bool reactionFlag)
	{
		reactionFlag_ = reactionFlag;
	}

	static void SetClientRect(LPRECT ClientRect)
	{
		ClientRect_ = ClientRect;
	}
};