#include "PlayOptionScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

#include "PlayScene.h"
#include "PartialOperationMemoScene.h"

//コンストラクタ
//作成しきっていないので、最初はfalse
PlayOptionScene::PlayOptionScene(IGameObject* parent)
	: IGameObject(parent, "PlayOptionScene"), hPict_(-1), optionFlag_(false),
	optionFlagOn_(true)
{
}

//初期化
void PlayOptionScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/OptionScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	partialOperationMemoLogo_ = CreateButtonObject<Button>(this, "data/PartialOperationMemoLogo.png", 650.0f, 300.0f);
	retireLogo_ = CreateButtonObject<Button>(this, "data/RetireLogo.png", 650.0f, 650.0f);
}

//更新
void PlayOptionScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool partialOperationMemoLogoFlag = partialOperationMemoLogo_->GetSceneChangeFlag();
	bool retireLogoFlag = retireLogo_->GetSceneChangeFlag();

	//受け取ったフラグで処理を変える
	//trueは今このシーンを表示してる証拠
	if ((optionFlag_ == true) && (partialOperationMemoLogoFlag == true))
	{
		//操作の一部が書いてあるシーンへ変更
		CreateGameObject<PartialOperationMemoScene>(this);

		//表示しなくなるためfalse
		optionFlag_ = false;

		//操作確認画面から抜けられなくなるため、falseにする
		partialOperationMemoLogo_->SetSceneChangeFlag(false);

		//それぞれのボタンを反応させなくするためfalse
		partialOperationMemoLogo_->SetReactionFlag(false);
		retireLogo_->SetReactionFlag(false);
	}
	else if ((optionFlag_ == true) && (retireLogoFlag == true))
	{
		//メニューシーンに戻る
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}

	//ESCキーを押したら
	if ((optionFlag_ == true) && (Input::IsKeyDown(DIK_ESCAPE)))
	{
		//オプション画面を閉じる
		//プレイシーンを操作可能にするためフラグをtrueにする
		PlayScene* pPlayScene = (PlayScene*)GetParent();
		pPlayScene->SetPlayFlag(true);
		partialOperationMemoLogo_->KillMe();
		retireLogo_->KillMe();
		KillMe();
	}

	//作成しきったのでフラグをtrue
	if (optionFlagOn_ == true)
	{
		optionFlag_ = true;
		//二回目を通らなくするためfalse
		optionFlagOn_ = false;

		//ボタンを反応させるためtrue
		partialOperationMemoLogo_->SetReactionFlag(true);
		retireLogo_->SetReactionFlag(true);
	}
}

//描画
void PlayOptionScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void PlayOptionScene::Release()
{
}