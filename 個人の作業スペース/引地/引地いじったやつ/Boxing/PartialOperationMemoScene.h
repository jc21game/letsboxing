#pragma once
#include "Engine/Global.h"

//一部が書いてある操作説明シーンを管理するクラス
class PartialOperationMemoScene : public IGameObject
{
	int hPict_;    //画像番号
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PartialOperationMemoScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
