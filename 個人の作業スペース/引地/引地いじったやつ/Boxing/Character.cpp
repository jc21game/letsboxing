#include "Character.h"
#include "Engine/GameObject/Model.h"
#include "Engine/GameObject/Camera.h"

#include "Enemy.h"

//コンストラクタ
Character::Character(IGameObject * parent)
	:IGameObject(parent, "Character"), hModel_(-1)
{
}

//デストラクタ
Character::~Character()
{
}

//初期化
void Character::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Data/Character.fbx");
	assert(hModel_ >= 0);

	//敵作成（親はプレイシーン）
	pEnemy_ = CreateGameObject<Enemy>(GetParent());
	
	//カメラ設定（一人称）
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 4.5f, -0.3f));
	pCamera->SetTarget(D3DXVECTOR3(0, 4.5f, 0));
}

//更新
void Character::Update()
{
	//角度を求める
	float angle = ViewPoint(pEnemy_);
	//角度に合わせて回転する（ラジアンから度に変更）
	rotate_.y = D3DXToDegree(angle);
	
	//移動ベクトル作成
	D3DXVECTOR3 moveX, moveZ;
	TargetMove(moveX, moveZ);

	//移動ベクトルごとに移動
	if (Input::IsKey(DIK_A))
	{
		position_ -= moveX;
	}
	if (Input::IsKey(DIK_D))
	{
		position_ += moveX;
	}
	if (Input::IsKey(DIK_W))
	{
		position_ += moveZ;
	}
	if (Input::IsKey(DIK_S))
	{
		position_ -= moveZ;
	}

	//構えの上下
	//ホイールを奥に回す

	//ホイールを手前に回す
	
}

//描画
void Character::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Character::Release()
{
}

float Character::ViewPoint(IGameObject* target)
{
	//自分の位置から相手の位置へ向かうベクトル
	D3DXVECTOR3 targetVec = target->GetPosition() - position_;
	//Z軸方向のベクトル（角度を求めるため１）
	D3DXVECTOR3 vecZ = D3DXVECTOR3(0, 0, 1);
	//正規化（角度を求めるため）
	D3DXVec3Normalize(&targetVec, &targetVec);

	//内積で長さを出す
	float dot = D3DXVec3Dot(&targetVec, &vecZ);
	//acosで変換すると角度が出る
	float angle = acos(dot);

	//外積を求める
	D3DXVECTOR3 cross;
	D3DXVec3Cross(&cross, &targetVec, &vecZ);
	//角度が右へ行くなら
	if (cross.y > 0)
	{
		//角度をマイナスにする
		angle *= -1;
	}

	//角度を返す
	return angle;
}

void Character::TargetMove(D3DXVECTOR3 & moveX, D3DXVECTOR3 & moveZ)
{
	//Y軸回転に合わせて移動出来るようにする
	D3DXMATRIX mRotZ, mRotX;
	D3DXMatrixRotationY(&mRotZ, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationY(&mRotX, D3DXToRadian(rotate_.y));

	//Z軸移動ベクトル
	D3DXVec3TransformCoord(&moveZ, &D3DXVECTOR3(0, 0, 0.1f), &mRotZ);

	//X軸移動ベクトル
	D3DXVec3TransformCoord(&moveX, &D3DXVECTOR3(0.1f, 0, 0), &mRotX);
}