#include "PartialOperationMemoScene.h"
#include "Engine/GameObject/Image.h"

#include "PlayOptionScene.h"

//コンストラクタ
PartialOperationMemoScene::PartialOperationMemoScene(IGameObject* parent)
	: IGameObject(parent, "PartialOperationMemoScene"), hPict_(-1)
{
}

//初期化
void PartialOperationMemoScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/OperationMemoScene.png");
	assert(hPict_ >= 0);
}

//更新
void PartialOperationMemoScene::Update()
{
	//ESCキーでオプション画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		//オプション画面を操作可能にするため、trueにする
		PlayOptionScene* pPlayOptionScene = (PlayOptionScene*)GetParent();
		pPlayOptionScene->SetOptionFlagOn(true);

		KillMe();
	}
}

//描画
void PartialOperationMemoScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void PartialOperationMemoScene::Release()
{
}