#include "PlayScene.h"
#include "Engine/GameObject/Image.h"

#include "PlayOptionScene.h"
#include "ResultScene.h"
#include "Character.h"
#include "Enemy.h"

#define WIN 0
#define LOSE 1

//コンストラクタ
//プレイフラグがtrueの場合、操作可能
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), /*hPict_(-1),*/ playFlag_(true)
{
}

//初期化
void PlayScene::Initialize()
{
	//画像データのロード
	//hPict_ = Image::Load("data/PlayScene.png");
	//assert(hPict_ >= 0);

	CreateGameObject<Character>(this);
}

//更新
void PlayScene::Update()
{
	//ESCキーでオプション画面
	//trueは今このシーンを表示してる証拠
	if ((playFlag_ == true) && (Input::IsKeyDown(DIK_ESCAPE)))
	{
		CreateGameObject<PlayOptionScene>(this);

		//表示しなくなるためfalse
		playFlag_ = false;
	}
	//１ボタンで勝ちのリザルト画面
	if ((playFlag_ == true) && (Input::IsKeyDown(DIK_1)))
	{
		ResultScene* pResultScene = new ResultScene(this);
		this->PushBackChild(pResultScene);
		pResultScene->SetJudgment(WIN);
		pResultScene->Initialize();

		//表示しなくなるためfalse
		playFlag_ = false;
	}
	//２ボタンで負けのリザルト画面
	if ((playFlag_ == true) && (Input::IsKeyDown(DIK_2)))
	{
		ResultScene* pResultScene = new ResultScene(this);
		this->PushBackChild(pResultScene);
		pResultScene->SetJudgment(LOSE);
		pResultScene->Initialize();

		//表示しなくなるためfalse
		playFlag_ = false;
	}
}

//描画
void PlayScene::Draw()
{
	//Image::SetMatrix(hPict_, worldMatrix_);
	//Image::Draw(hPict_);
}

//開放
void PlayScene::Release()
{
}