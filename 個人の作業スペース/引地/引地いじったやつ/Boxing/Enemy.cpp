#include "Enemy.h"
#include "Engine/GameObject/Model.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルのロード
	hModel_ = Model::Load("Data/Character.fbx");
	assert(hModel_ >= 0);

	//初期位置
	position_ = D3DXVECTOR3(0, 0, 20);
}

//更新
void Enemy::Update()
{
}

//描画
void Enemy::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}


