#pragma once
#include "Engine/Global.h"

class Button;

//難易度選択シーンを管理するクラス
class DifficultyScene : public IGameObject
{
	int hPict_;				//画像番号
	Button* easyLogo_;		//簡単ロゴ
	Button* normalLogo_;	//普通ロゴ
	Button* hardLogo_;		//難しいロゴ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	DifficultyScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};