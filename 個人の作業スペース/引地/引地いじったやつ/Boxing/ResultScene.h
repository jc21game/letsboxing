#pragma once
#include "Engine/Global.h"

//試合の結果シーンを管理するクラス
class ResultScene : public IGameObject
{
	int hPict_;		//画像番号
	int judgment_;	//勝ち負けの判定
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイシーンから受け取った判定をセットする
	//引数：judgment 判定
	void SetJudgment(int judgment)
	{
		judgment_ = judgment;
	}
};
