#pragma once
#include "Engine/Global.h"

class ResultScene;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//int hPict_;					//画像番号
	ResultScene* pResultScene_; //リザルトシーン
	bool playFlag_;				//プレイシーンが操作できるかのフラグ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイフラグをセットする
	//引数：playFlag フラグを受け取る
	void SetPlayFlag(bool playFlag)
	{
		playFlag_ = playFlag;
	}
};
