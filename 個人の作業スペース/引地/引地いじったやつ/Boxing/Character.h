#pragma once
#include "Engine/ObjectTree/IGameObject.h"

class Enemy;

//キャラクターを管理するクラス
class Character : public IGameObject
{
	int hModel_;	//モデル番号
	Enemy* pEnemy_; //敵のポインタ
public:
	//コンストラクタ
	Character(IGameObject* parent);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//敵の方向に向かせる
	//引数：target 敵
	//戻値：float y軸の向き
	float ViewPoint(IGameObject* target);

	//回転後に敵の方へ向かうようにする処理
	//引数：moveX,moveZ 前後左右の動き（参照渡し）
	void TargetMove(D3DXVECTOR3& moveX, D3DXVECTOR3& moveZ);
};