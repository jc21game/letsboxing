#pragma once
#include "IGameObject.h"

//シーンのIDをenumで管理
enum SCENE_ID
{
	SCENE_ID_SPLASH,
	SCENE_ID_TITLE,
	SCENE_ID_MENU,
	SCENE_ID_DIFFICULTY,
	SCENE_ID_TRAINING,
	SCENE_ID_TRAINING_OPTION,
	SCENE_ID_ALL_OPERATION_MEMO,
	SCENE_ID_REMATCH,
	SCENE_ID_PLAY,
};

//シーンの切り替えを管理するクラス
class SceneManager : public IGameObject
{
	//staticはコンパイルした時点で作られるため、初期値を入れなければならない
	static SCENE_ID currentSceneID_;	//今のシーンID
	static SCENE_ID nextSceneID_;		//次のシーンID
	static IGameObject* pCurrentScene_; //現在のシーン

public:
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーンを切り替える関数
	//引数 next シーンのID
	//戻値 なし
	static void ChangeScene(SCENE_ID next);

	//カレントシーンのゲッター
	//戻値 pCurrentScene 値を返す
	static IGameObject* GetCurrentScene()
	{
		return pCurrentScene_;
	}
};