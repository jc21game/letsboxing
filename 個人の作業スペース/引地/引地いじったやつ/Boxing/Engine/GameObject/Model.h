#pragma once
#include <string>
#include "../DirectX/Fbx.h"

namespace Model
{
	//構造体モデルデータ
	struct ModelData
	{
		std::string fileName;	//STLの文字列を管理する変数
		Fbx*	pFbx;			//FBXファイルを入れる変数
		D3DXMATRIX matrix;		//行列変数

		//コンストラクタ
		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//ファイル名を受け取る関数
	//引数：fileName	ファイルの名前
	//戻値：int			ファイル番号を返す
	int Load(std::string fileName);

	//ファイルを読み込む関数
	//引数：handle		ファイル番号
	//戻値：なし
	void Draw(int handle);

	//行列をセットする関数
	//引数：handle　ファイル番号、matrix　行列
	//戻値：なし
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//解放処理
	//引数：handle　ファイル番号
	//戻値：なし
	void Release(int handle);

	//ゲーム終了時に呼び出される解放処理
	//引数：なし
	//戻値：なし
	void AllRelease();

	//レイキャスト（レイを飛ばして当たり判定）
	//引数：handle モデルリストの番号　	data レイキャストのデータ
	void RayCast(int handle, RayCastData& data);
};


