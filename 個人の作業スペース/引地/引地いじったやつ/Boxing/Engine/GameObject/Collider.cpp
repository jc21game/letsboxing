#include "Collider.h"



Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	//セット！
	owner_ = owner;
	center_ = center;
	radius_ = radius;
	
}


Collider::~Collider()
{
}

bool Collider::IsHit(Collider& target)
{
	//中心点と自分の位置を足した値から、相手の中心点と相手の位置を足した値から引く
	D3DXVECTOR3 v = (center_ + owner_->GetPosition()) - 
		(target.center_ + target.owner_->GetPosition());

	//ベクトルの長さ
	float length = D3DXVec3Length(&v);

	//ベクトルの長さが、自分の半径と相手の半径を足した値より少ない場合
	if (length <= radius_  + target.radius_	)
	{
		return true;
	}
	return false;
}
