#pragma once
#include "Engine/Global.h"

class Button;

//プレイシーンのオプションシーンを管理するクラス
class PlayOptionScene : public IGameObject
{
	int hPict_;								//画像番号
	Button* partialOperationMemoLogo_;		//一部の操作説明ロゴ
	Button* retireLogo_;					//リタイアロゴ
	bool optionFlag_;						//オプションシーンが存在してるかフラグ
	bool optionFlagOn_;						//オプションシーンフラグをtrueにするフラグ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayOptionScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//オプションシーンフラグをtrueにするためのフラグをセットする
	//引数：optionFlagOn フラグを受け取る
	void SetOptionFlagOn(bool optionFlagOn)
	{
		optionFlagOn_ = optionFlagOn;
	}
};