#include "AllOperationMemoScene.h"
#include "Engine/GameObject/Image.h"

//コンストラクタ
AllOperationMemoScene::AllOperationMemoScene(IGameObject * parent)
	: IGameObject(parent, "AllOperationMemoScene"), hPict_(-1)
{
}

//初期化
void AllOperationMemoScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/OperationMemoScene.png");
	assert(hPict_ >= 0);
}

//更新
void AllOperationMemoScene::Update()
{
	//ESCキーでオプション画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TRAINING_OPTION);
	}
}

//描画
void AllOperationMemoScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void AllOperationMemoScene::Release()
{
}