#pragma once
#include "Engine/ObjectTree/IGameObject.h"

//キャラクターを管理するクラス
class Enemy : public IGameObject
{
	int hModel_;	//モデル番号
public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


};