#include "RematchScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

//コンストラクタ
RematchScene::RematchScene(IGameObject * parent)
	: IGameObject(parent, "RematchScene"), hPict_(-1)
{
}

//初期化
void RematchScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/RematchScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	yesLogo_ = CreateButtonObject<Button>(this, "data/YesLogo.png", 80.0f, 440.0f);
	noLogo_ = CreateButtonObject<Button>(this, "data/NoLogo.png", 1200.0f, 440.0f);
}

//更新
void RematchScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool yesLogoFlag = yesLogo_->GetSceneChangeFlag();
	bool noLogoFlag = noLogo_->GetSceneChangeFlag();

	//受け取ったフラグで処理を変える
	if (yesLogoFlag == true)
	{
		//プレイシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
	else if (noLogoFlag == true)
	{
		//メニューシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void RematchScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void RematchScene::Release()
{
}