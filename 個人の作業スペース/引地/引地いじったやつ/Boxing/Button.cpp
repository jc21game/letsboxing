#include "Button.h"
#include "Engine/GameObject/Image.h"
#include <string>
#include <math.h>

LPRECT Button::ClientRect_ = { 0 };


//コンストラクタ
//名前と画像の原点をセット
//reactionFlagは基本true
Button::Button(IGameObject * parent, std::string fileName,
	float originX, float originY)
	:IGameObject(parent, "Button"), fileName_(fileName), originX_(originX),
	originY_(originY), hPict_(-1), sceneChangeFlag_(false),
	reactionFlag_(true)
{
}

//デストラクタ
Button::~Button()
{
}

//初期化
void Button::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load(fileName_);
	assert(hPict_ >= 0);

	//画像の原点変更
	position_ = D3DXVECTOR3(originX_, originY_, 0);
}

//更新
void Button::Update()
{
	//マウスのポジション取得
	D3DXVECTOR3 mousePos = Input::GetMousePosition();

	//左クリックしたか
	if (Input::IsMouseButtonDown(0))
	{
		//画像内にクリックしてるか
		
		if ((originX_ * (float)(fabsf(ClientRect_->right - ClientRect_->left)/ W_WIDTH)) < (mousePos.x * (float)(fabsf(ClientRect_->right - ClientRect_->left) / W_WIDTH))
			&& ((originX_ + Image::GetImageSizeWidth(hPict_)) * (float)(fabsf(ClientRect_->right - ClientRect_->left) / W_WIDTH)) > (mousePos.x * (float)(fabsf(ClientRect_->right - ClientRect_->left) / W_WIDTH))
			&& (originY_ * (fabsf(ClientRect_->top - ClientRect_->bottom) / W_HEIGHT)) < (mousePos.y * (fabsf(ClientRect_->top - ClientRect_->bottom) / W_HEIGHT))
			&& ((originY_ + Image::GetImageSizeHeight(hPict_)) * (float)(fabsf(ClientRect_->top - ClientRect_->bottom) / W_HEIGHT)) > (mousePos.y * (float)(fabsf(ClientRect_->top - ClientRect_->bottom) / W_HEIGHT)) &&
			(reactionFlag_ == true))
		{
			float a = ((float)(fabsf(ClientRect_->right - ClientRect_->left) / W_WIDTH));
			//シーン切り替えのフラグをオンにする
			sceneChangeFlag_ = true;
		}
	}
}

//描画
void Button::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Button::Release()
{
}