#include "NonPlayerCharacter.h"
#include "Engine/Input.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"

//コンストラクタ
NonPlayerCharacter::NonPlayerCharacter(IGameObject * parent)
	:Character(parent, "NonPlayerCharacter")
{
}

//デストラクタ
NonPlayerCharacter::~NonPlayerCharacter()
{
}

//初期化
void NonPlayerCharacter::Initialize()
{
	//胴体コライダーを作成
	bodyCollider_ = new BoxCollider("bodyCollider", D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(2, 4, 2));
	AddCollider(bodyCollider_);

	//対戦相手の名前
	opponent_ = "PlayerCharacter";

	rotate_.y = 180;
	position_.y = 4;
	position_.z = 2;

	hp_ = 4;
}

////更新
//void NonPlayerCharacter::Update()
//{
//}
//
////描画
//void NonPlayerCharacter::Draw()
//{
//}
//
////開放
//void NonPlayerCharacter::Release()
//{
//}

//void NonPlayerCharacter::OnCollision(IGameObject * pTarget)
//{
//	SubOnCollision(pTarget);
//}

void NonPlayerCharacter::PunchCommand()
{
	if (Input::IsKeyDown(DIK_I))
	{
		state_ = PUNCH;
	}
}

void NonPlayerCharacter::StrongPunchCommand()
{
	if (Input::IsKeyDown(DIK_O))
	{
		state_ = CHARG;
	}
}

void NonPlayerCharacter::CounterCommand()
{
	if (Input::IsKeyDown(DIK_P))
	{
		state_ = COUNTER;
	}
}

void NonPlayerCharacter::StanceChangeCommand()
{
}

void NonPlayerCharacter::MoveCommand()
{
}