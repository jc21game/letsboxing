#include "MenuScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene"), hPict_(-1),
	state_(STATE_MATCH)
{
}

//初期化
void MenuScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Menu.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	matchLogo_ = CreateGameObject<PictDisplay>(this, "data/MatchLogo.png", "data/SelectMatchLogo.png", 80.0f, 440.0f);
	operationMemoLogo_ = CreateGameObject<PictDisplay>(this, "data/OperationMemoLogo.png", "data/SelectOperationMemoLogo.png", 1200.0f, 440.0f);
}

//更新
void MenuScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//試合ロゴ選択時
	case STATE_MATCH:
		//ロゴが黄色くなる
		matchLogo_->Selection();

		//Enterで難易度選択画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_DIFFICULTY);
		}
		//右キーを押したら操作説明ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_OPERATION_MEMO;
			matchLogo_->UnSelection();
		}
		//左キーを押したら操作説明ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_OPERATION_MEMO;
			matchLogo_->UnSelection();
		}
		break;

	//操作説明ロゴ選択時
	case STATE_OPERATION_MEMO:
		//ロゴが黄色くなる
		operationMemoLogo_->Selection();

		//Enterで操作説明画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_OPERATION_MEMO);
		}
		//右キーを押したら試合ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_MATCH;
			operationMemoLogo_->UnSelection();
		}
		//左キーを押したら試合ロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_MATCH;
			operationMemoLogo_->UnSelection();
		}
		break;
	}

	//Escキーで前の画面（タイトル画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void MenuScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void MenuScene::Release()
{
}