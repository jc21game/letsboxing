#include "Stage.h"
#include "PlayScene.h"
#include "Engine/Model.h"
#include "ResultScene.h"


//コンストラクタ
//名前と画像の原点をセット
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), hModel_(-1), pText_(nullptr), sec_(0), tensec_(0), min_(7), frame_(0)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	pText_ = new Text("ゴシック", 80);
	//画像データのロード
	hModel_ = Model::Load("data/Stage.fbx");
	assert(hModel_ >= 0);

	second_[0] = "0";
	second_[1] = "9";
	second_[2] = "8";
	second_[3] = "7";
	second_[4] = "6";
	second_[5] = "5";
	second_[6] = "4";
	second_[7] = "3";
	second_[8] = "2";
	second_[9] = "1";
	second_[10] = "0";

	minute_[0] = "0";
	minute_[1] = "9";
	minute_[2] = "8";
	minute_[3] = "7";
	minute_[4] = "6";
	minute_[5] = "5";
	minute_[6] = "4";
	minute_[7] = "3";
	minute_[8] = "2";
	minute_[9] = "1";
	minute_[10] = "0";

	
}

//更新
void Stage::Update()
{
	timecnt();
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);

	//第一、第二引数は位置情報
	pText_->Draw(600, 40, minute_[min_] + ":" + second_[tensec_] + second_[sec_]);

	

}

//開放
void Stage::Release()
{
}

void Stage::timecnt()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (min_ <= 10)
		{

			//最初3:00から2:59にする
			if (tensec_ == 0 && sec_ == 0)
			{
				tensec_ = 5;
				sec_ = 0;
				min_++;
			}



			//tensec_は10の位のカウント
			if (tensec_ <= 10)
			{
				if (tensec_ == 10 && sec_ == 10)
				{
					tensec_ = 5;
					sec_ = 1;
					min_++;
				}

				//1の位のカウント
				else if (sec_ < 10)
				{
					sec_++;
				}

				else if (sec_ == 10)
				{
					sec_ = 1;
					tensec_++;
				}

			}
			if (min_ == 10 && tensec_ == 10 && sec_ == 10)
			{
				sec_ = 0;
				tensec_ = 0;
				min_ = 7;
			}
		}
	}
}
void Stage::Round()
{
	/*if (be == 1)
	{
		roundsu_++;
	}*/
}
