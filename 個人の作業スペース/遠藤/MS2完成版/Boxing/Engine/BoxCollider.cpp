#include "BoxCollider.h"
#include "Direct3D.h"

//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
BoxCollider::BoxCollider(const std::string& name, D3DXVECTOR3 basePos, D3DXVECTOR3 size) : Collider(name)
{
	center_ = basePos;
	size_ = size;
	type_ = COLLIDER_BOX;

	m_NormaDirect[0] = D3DXVECTOR3(1, 0, 0);
	m_NormaDirect[1] = D3DXVECTOR3(0, 1, 0);
	m_NormaDirect[2] = D3DXVECTOR3(0, 0, 1);

	m_fLength[0] = size.x / 2;
	m_fLength[1] = size.y / 2;
	m_fLength[2] = size.z / 2;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

BoxCollider::BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size) : BoxCollider("", basePos, size)
{
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool BoxCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);
}