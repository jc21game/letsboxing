#include <assert.h>
#include <d3dx9.h>
#include "Input.h"

namespace Input
{
	//namespaceでコンストラクタがないので、ここで初期化する
	//キーボード
	LPDIRECTINPUT8   pDInput = nullptr;
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;	//デバイスオブジェクト
	BYTE keyState[256] = { 0 };	//現在の各キーの状態
	BYTE prevKeyState[256] = { 0 };    //前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice = nullptr; //デバイスオブジェクト
	DIMOUSESTATE mouseState = { 0 };    //マウスの状態
	DIMOUSESTATE prevMouseState = { 0 };   //前フレームのマウスの状態

	//ウィンドウハンドル
	HWND hWnd_;

	void Initialize(HWND hWnd)
	{
		//pDinputにDirectInput本体が入る
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
			IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		//キーボード
		{
			//インスタンスを作成
			pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
			assert(pKeyDevice != nullptr);

			//デバイスの種類を指定（今回はキーボード）
			pKeyDevice->SetDataFormat(&c_dfDIKeyboard);

			//強調レベルの設定（他の実行中のアプリに対する優先度）
			//ビット演算の順番は、宣言の上から書く
			pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		//マウス
		{
			pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
			assert(pKeyDevice);
			pMouseDevice->SetDataFormat(&c_dfDIMouse);
			pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		}

		hWnd_ = hWnd;
	}

	void Update()
	{
		//キーボード
		//前に押したキーの情報をコピー
		memcpy(prevKeyState, keyState, sizeof(keyState));

		//キーボードを見失ったときにもう一回探す
		pKeyDevice->Acquire();

		//実行した瞬間すべてのキーが押されてるか押されてないか
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->Acquire();
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);
	}

	void Release()
	{
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}

	///////////////////////////// キーボード情報取得 //////////////////////////////////

	bool IsKey(int keyCode)
	{
		//ビット演算で押されてるか確認
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if ((IsKey(keyCode)) &&
			(prevKeyState[keyCode] & 0x80) == 0)
		{
			return true;
		}
		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//放したときに反応（今は押してないけど、前回は押してる)
		if ((IsKey(keyCode) == 0) &&
			(prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	///////////////////////////// マウス情報取得 //////////////////////////////////

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;
		GetCursorPos(&mousePos);
		ScreenToClient(hWnd_, &mousePos);

		D3DXVECTOR3 result = D3DXVECTOR3((float)mousePos.x, (float)mousePos.y, (float)0);
		return result;
	}


	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result((float)mouseState.lX, (float)mouseState.lY, (float)mouseState.lZ);
		return result;
	}
}