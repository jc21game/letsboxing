#pragma once
#include "Engine/global.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"

class Camera;
class PictDisplay;
class Round;


//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	string round_[3];
	string point_[3];
	Text* pText_;
	int hModel_;

	//ronud用
	 int roundsu_;
	 Round * pRound_;
	int count_;
	
	enum PICT {
		MY_HP,
		ENEMY_HP,
		MY_SKILL,
		ENEMY_SKILL,
		MY_NAME,
		ENEMY_NAME,
		ROUND,
		TIMER,
		MAX
	};
	Camera* pCamera_;			//仮カメラ
	bool nowPlayScene_;			//プレイシーンを操作できるかどうか
	PictDisplay* ui_[MAX];		//UIたち
	
public:
	
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //プレイシーンを操作できるかのフラグをセット
  //引数：nowPlayScene プレイシーンの操作可か？
  void SetNowPlayScene(bool nowPlayScene)
  {
	  nowPlayScene_ = nowPlayScene;
  };

  //衝突
  void OnCollision(IGameObject *pTarget) override;

 
};