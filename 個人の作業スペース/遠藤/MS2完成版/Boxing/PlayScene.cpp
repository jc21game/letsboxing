#include <string>
#include "PlayScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "Stage.h"
#include "Character.h"
#include "PlayerCharacter.h"
#include "NonPlayerCharacter.h"
#include "GiveUpScene.h"
#include "ResultScene.h"
#include "Engine/Camera.h"
#include "Engine/Model.h"
#include "Stage.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")	//親のコンストラクタを引数ありで呼ぶ
	, pCamera_(nullptr), nowPlayScene_(true), roundsu_(0) 
{
}

//初期化
void PlayScene::Initialize()
{
	pText_ = new Text("ゴシック", 80);

	round_[0] = "1";
	round_[1] = "2";
	round_[2] = "3";

	point_[0] = "0";
	point_[1] = "1";
	point_[2] = "2";
	


	


	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 20, -5));

	//プレイヤーたち
	CreateGameObject<PlayerCharacter>(this);
	CreateGameObject<NonPlayerCharacter>(this);

	//ステージ
	CreateGameObject<Stage>(this);

	//ファイルの名前
	//const std::string fileName[MAX]{
	//	"data/HP.png",
	//	"data/HP.png",
	//	"data/Skill.png",
	//	"data/Skill.png",
	//	"data/name.png",
	//	"data/name2.png",
	//	//"data/Round.png",
	//	//"data/time.png"
	//};

	//UI作成
	for (int i = 0; i < MAX; i++)
	{
	//	ui_[i] = CreateGameObject<PictDisplay>(this, fileName[i], "", NULL, NULL);
	}

	//画像のposition設定
	/*ui_[0]->SetPosition(D3DXVECTOR3(NULL, 30.0f, NULL));
	ui_[1]->SetPosition(D3DXVECTOR3(1400.0f, 30.0f, NULL));
	ui_[2]->SetPosition(D3DXVECTOR3(NULL, 90.0f, NULL));
	ui_[3]->SetPosition(D3DXVECTOR3(1400.0f, 90.0f, NULL));
	ui_[4]->SetPosition(D3DXVECTOR3(NULL, 120.0f, NULL));
	ui_[5]->SetPosition(D3DXVECTOR3(1700.0f, 120.0f, NULL));*/
	//ui_[6]->SetPosition(D3DXVECTOR3(850.0f, NULL, NULL));
	//ui_[7]->SetPosition(D3DXVECTOR3(600.0f, NULL, NULL));

	
}

//更新
void PlayScene::Update()
{
	//真だったら操作可能
	if (nowPlayScene_)
	{
		//画像のposition設定
		/*ui_[0]->SetPosition(D3DXVECTOR3(NULL, 30.0f, NULL));
		ui_[1]->SetPosition(D3DXVECTOR3(1400.0f, 30.0f, NULL));
		ui_[2]->SetPosition(D3DXVECTOR3(NULL, 90.0f, NULL));
		ui_[3]->SetPosition(D3DXVECTOR3(1400.0f, 90.0f, NULL));
		ui_[4]->SetPosition(D3DXVECTOR3(NULL, 120.0f, NULL));
		ui_[5]->SetPosition(D3DXVECTOR3(1700.0f, 120.0f, NULL));*/
		//ui_[6]->SetPosition(D3DXVECTOR3(850.0f, NULL, NULL));
		//ui_[7]->SetPosition(D3DXVECTOR3(600.0f, NULL, NULL));

		//１キーで勝ち！
		if (Input::IsKeyDown(DIK_1))
		{
			//count_ = pRound_->GetCount();
			g.win++;

			if (  g.win+g.lose < 3 && g.win<=1)
			{
				
				SceneManager* pSceneManager;
				pSceneManager->ReloadScene();
			}
			else if(g.win= 2)//2ポイントでResultへ
			{
				CreateGameObject<ResultScene>(pParent_, true);
				nowPlayScene_ = false;
			}

		}
		//２キーで負け！
		else if(Input::IsKeyDown(DIK_2))
		{
			//count_ = pRound_->GetCount();
			g.lose++;
			if (g.win+g.lose < 3 && g.lose <= 1)
			{
				
				SceneManager* pSceneManager;
				pSceneManager->ReloadScene();
			}
			else if(g.lose = 2 )//2ポイントでResultへ
			{

				CreateGameObject<ResultScene>(pParent_, false);
				nowPlayScene_ = false;
			}
		}
		//ESCキーでギブアップ画面
		else if (Input::IsKeyDown(DIK_ESCAPE))
		{
			CreateGameObject<GiveUpScene>(pParent_);
			nowPlayScene_ = false;
		}
		
	}
	//else
	//{
	//	//画像を画面外へ
	//	for (int i = 0; i < MAX; i++)
	//	{
	//		ui_[i]->SetPosition(D3DXVECTOR3(-2000.0f, -2000.0f, 0));
	//	}
	//}
}

//描画
void PlayScene::Draw()
{
	//第一、第二引数は位置情報,ラウンドの表示
	pText_->Draw(800, 40, round_[g.win+g.lose] + "/" + "3");
	//勝利ポイントの表示
	pText_->Draw(20, 200, point_[g.win ] );
	//敗北ポイントの表示
	pText_->Draw(1800, 200, point_[g.lose] );

	
}

//開放
void PlayScene::Release()
{
}

void PlayScene::OnCollision(IGameObject * pTarget)
{
}
