#include <time.h>
#include "SplashScene.h"
#include "Engine/PictDisplay.h"
#include "Engine/Image.h"


//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene")	//親のコンストラクタを引数ありで呼ぶ
	,hPict_(-1), alphaValue_(0)
	, alphaValueflag_(true)
	, startClock_(clock())	//clock()関数はプログラムが始まってから何ミリ秒経ったか返す
	, sceneSwitchFlag_(false), state_(STATE_FRONT), startTime_(NULL), endTime_(NULL)
{
}

//初期化
void SplashScene::Initialize()
{
	hPict_ = Image::Load("Data/TeamLogo.png");

	assert(hPict_ >= 0);

	schoolLogoFront_ = CreateGameObject<PictDisplay>(this, "data/SchoolLogoFront.png", "", -512.0f, 320.0f);
	schoolLogoBack_ = CreateGameObject<PictDisplay>(this, "data/SchoolLogoBack.png", "", 1920.0f, 480.0f);
}

//更新
void SplashScene::Update()
{

	if (alphaValueflag_ == true)
	{
		alphaValue_ += 0.02;
	}
	if (alphaValue_ >= 1)
	{
		alphaValueflag_ = false;
	}
	if (alphaValueflag_ == false)
	{
		if ((clock() - startClock_) >= 2000)
		{
			alphaValue_ -= 0.02;
		}
	}
	if (alphaValue_ < 0)
	{
		sceneSwitchFlag_ = true;
	}
	//チームロゴの処理完了後に実行
	if (sceneSwitchFlag_)
	{
		//二つのロゴの現在位置を取得
		D3DXVECTOR3 frontPos = schoolLogoFront_->GetPosition();
		D3DXVECTOR3 backPos = schoolLogoBack_->GetPosition();

		//状態の種類
		switch (state_)
		{
		//最初に「東北電子」のロゴを動かす
		case STATE_FRONT:
			frontPos.x += 15.0f;
			schoolLogoFront_->SetPosition(D3DXVECTOR3(frontPos.x, frontPos.y, frontPos.z));
			
			//一定位置まで動かしたら「専門学校」のロゴを動かす状態にする
			if (frontPos.x >= 600.0f)
			{
				state_ = STATE_BACK;
			}
			break;

		//次に「専門学校」のロゴを動かす
		case STATE_BACK:
			backPos.x -= 15.0f;
			schoolLogoBack_->SetPosition(D3DXVECTOR3(backPos.x, backPos.y, backPos.z));

			//一定位置まで行ったらタイムの計測開始
			if (backPos.x <= 720.0f)
			{
				state_ = STATE_UNION;
			}
			break;
		
		//合体後のタイム計測
		case STATE_UNION:
			//スタートタイムだけ更新は一回だけ
			if (startTime_ == NULL)
			{
				time(&startTime_);
			}
			time(&endTime_);
		
			//二秒たったらタイトル画面に移行
			if (endTime_ - startTime_ >= 2.0f)
			{
				SceneManager* pSceneManager;
				pSceneManager->ChangeScene(SCENE_ID_TITLE);
			}
			break;
		}
	}

	//エンターキーでタイトル画面
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void SplashScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_, alphaValue_);
}

//開放
void SplashScene::Release()
{
}