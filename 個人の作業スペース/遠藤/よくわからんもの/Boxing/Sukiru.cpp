#include "Sukiru.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Sukiru::Sukiru(IGameObject * parent)
	: IGameObject(parent, "Sukiru"), Sukiruict_(-1)
{
}

//デストラクタ
Sukiru::~Sukiru()
{
}


//初期化
void Sukiru::Initialize()
{
	//画像データのロード
	Sukiruict_ = Image::Load("data/Sukiru.png");
	assert(Sukiruict_ >= 0);
	position_.y = 90;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z = 0.5f;
}

//更新
void Sukiru::Update()
{

}

//描画
void Sukiru::Draw()
{
	Image::SetMatrix(Sukiruict_, worldMatrix_);
	Image::Draw(Sukiruict_);
}

//開放
void Sukiru::Release()
{
}