#include "Right_Arm.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/GameObject/Model.h"

//コンストラクタ
Right_Arm::Right_Arm(IGameObject * parent)
	:IGameObject(parent, "Right_Arm"), _hModel(-1)
{
}

//デストラクタ
Right_Arm::~Right_Arm()
{
}

//初期化
void Right_Arm::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Right_Arm.fbx");
	assert(_hModel >= 0);
	position_ = D3DXVECTOR3(-2.5, 7.5, 4.5);
	rotate_.x = -95.0f;
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);


}

//更新
void Right_Arm::Update()
{
	if (Input::IsKeyDown(DIK_E))
	{
		position_.z += 10;
	}
	if (Input::IsKeyUp(DIK_E))
	{
		position_.z -= 10;
	}
}

//描画
void Right_Arm::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Right_Arm::Release()
{
}