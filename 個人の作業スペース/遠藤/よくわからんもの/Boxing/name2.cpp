#include "name2.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
name2::name2(IGameObject * parent)
	: IGameObject(parent, "name2"), name2ict_(-1)
{
}

//デストラクタ
name2::~name2()
{
}


//初期化
void name2::Initialize()
{
	//画像データのロード
	name2ict_ = Image::Load("data/name2.png");
	assert(name2ict_ >= 0);
	position_.x = 1700;
	position_.y = 120;
}

//更新
void name2::Update()
{

}

//描画
void name2::Draw()
{
	Image::SetMatrix(name2ict_, worldMatrix_);
	Image::Draw(name2ict_);
}

//開放
void name2::Release()
{
}