#include "SplashScene.h"
#include "Engine/GameObject/Image.h"

//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene")	//親のコンストラクタを引数ありで呼ぶ
	,hPict_(-1), alphaValue_(0)
	, alphaValueflag_(true)
	, startClock_(clock())	//clock()関数はプログラムが始まってから何ミリ秒経ったか返す
	, sceneSwitchFlag_(false)
{
}

//初期化
void SplashScene::Initialize()
{
	hPict_ = Image::Load("Data/チーム名ロゴ.jpg");

	assert(hPict_ >= 0);
}

//更新
void SplashScene::Update()
{

	if (alphaValueflag_ == true)
	{
		alphaValue_ += 0.02;
	}
	if (alphaValue_ >= 1)
	{
		alphaValueflag_ = false;
	}
	if (alphaValueflag_ == false)
	{
		if ((clock() - startClock_) >= 2000)
		{
			alphaValue_ -= 0.02;
		}
	}
	if (alphaValue_ < 0)
	{
		sceneSwitchFlag_ = true;
	}
	if (sceneSwitchFlag_)
	{
		//シーン切り替え
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

}

//描画
void SplashScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_, alphaValue_);
}

//開放
void SplashScene::Release()
{
}