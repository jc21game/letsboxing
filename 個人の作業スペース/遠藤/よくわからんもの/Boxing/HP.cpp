#include "HP.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
HP::HP(IGameObject * parent)
	: IGameObject(parent, "HP"), hPict_(-1)
{
}

//デストラクタ
HP::~HP()
{
}


//初期化
void HP::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/HP.png");
	assert(hPict_ >= 0);
	position_.y = 30;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z= 0.5f;
}

//更新
void HP::Update()
{

}

//描画
void HP::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void HP::Release()
{
}