#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//HP2を管理するクラス
class HP2 : public IGameObject
{
	int HP2ict_;    //モデル番号
public:
	//コンストラクタ
	HP2(IGameObject* parent);

	//デストラクタ
	~HP2();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};