#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//nameを管理するクラス
class name : public IGameObject
{
	int nameict_;    //モデル番号
public:
	//コンストラクタ
	name(IGameObject* parent);

	//デストラクタ
	~name();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};