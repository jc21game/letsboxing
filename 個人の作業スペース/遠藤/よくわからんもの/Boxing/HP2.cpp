#include "HP2.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
HP2::HP2(IGameObject * parent)
	: IGameObject(parent, "HP2"), HP2ict_(-1)
{
}

//デストラクタ
HP2::~HP2()
{
}


//初期化
void HP2::Initialize()
{
	//画像データのロード
	HP2ict_ = Image::Load("data/HP.png");
	assert(HP2ict_ >= 0);
	position_.y = 30;
	position_.x = 1400;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z = 0.5f;
}

//更新
void HP2::Update()
{

}

//描画
void HP2::Draw()
{
	Image::SetMatrix(HP2ict_, worldMatrix_);
	Image::Draw(HP2ict_);
}

//開放
void HP2::Release()
{
}