#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//name2を管理するクラス
class name2 : public IGameObject
{
	int name2ict_;    //モデル番号
public:
	//コンストラクタ
	name2(IGameObject* parent);

	//デストラクタ
	~name2();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};