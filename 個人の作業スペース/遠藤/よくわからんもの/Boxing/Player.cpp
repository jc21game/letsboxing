#include "Player.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/GameObject/Model.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), _hModel(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Body.fbx");
	assert(_hModel >= 0);
	position_ = D3DXVECTOR3(0, -3, 0);
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);

	
}

//更新
void Player::Update()
{
	if (Input::IsKeyDown(DIK_W))
	{
		position_.z += 1.0f;
	}
	if (Input::IsKeyDown(DIK_S))
	{
		position_.z -= 1.0f;
	}
}

//描画
void Player::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Player::Release()
{
}