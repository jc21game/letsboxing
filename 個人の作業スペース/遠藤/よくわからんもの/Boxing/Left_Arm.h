#pragma once
#include "Engine/ObjectTree/IGameObject.h"


//Left_Armを管理するクラス
class Left_Arm : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	Left_Arm(IGameObject* parent);

	//デストラクタ
	~Left_Arm();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};