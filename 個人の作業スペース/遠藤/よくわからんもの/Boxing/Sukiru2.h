#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//Sukiru2を管理するクラス
class Sukiru2 : public IGameObject
{
	int Sukiru2ict_;    //モデル番号
public:
	//コンストラクタ
	Sukiru2(IGameObject* parent);

	//デストラクタ
	~Sukiru2();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};