#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//HPを管理するクラス
class HP : public IGameObject
{
	int hPict_;    //モデル番号
public:
	//コンストラクタ
	HP(IGameObject* parent);

	//デストラクタ
	~HP();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};