#include "Raund.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Raund::Raund(IGameObject * parent)
	: IGameObject(parent, "Raund"), Raundict_(-1)
{
}

//デストラクタ
Raund::~Raund()
{
}


//初期化
void Raund::Initialize()
{
	//画像データのロード
	Raundict_ = Image::Load("data/Raund.png");
	assert(Raundict_ >= 0);
	position_.x = 850;
	position_.y = 0;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z = 0.5f;
}

//更新
void Raund::Update()
{

}

//描画
void Raund::Draw()
{
	Image::SetMatrix(Raundict_, worldMatrix_);
	Image::Draw(Raundict_);
}

//開放
void Raund::Release()
{
}