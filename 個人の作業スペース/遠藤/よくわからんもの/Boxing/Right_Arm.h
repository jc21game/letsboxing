#pragma once
#include "Engine/ObjectTree/IGameObject.h"


//Right_Armを管理するクラス
class Right_Arm : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	Right_Arm(IGameObject* parent);

	//デストラクタ
	~Right_Arm();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};