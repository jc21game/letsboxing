#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//Raundを管理するクラス
class Raund : public IGameObject
{
	int Raundict_;    //モデル番号
public:
	//コンストラクタ
	Raund(IGameObject* parent);

	//デストラクタ
	~Raund();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};