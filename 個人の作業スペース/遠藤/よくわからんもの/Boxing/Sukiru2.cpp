#include "Sukiru2.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Sukiru2::Sukiru2(IGameObject * parent)
	: IGameObject(parent, "Sukiru2"), Sukiru2ict_(-1)
{
}

//デストラクタ
Sukiru2::~Sukiru2()
{
}


//初期化
void Sukiru2::Initialize()
{
	//画像データのロード
	Sukiru2ict_ = Image::Load("data/Sukiru.png");
	assert(Sukiru2ict_ >= 0);
	position_.x = 1400;
	position_.y = 90;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z = 0.5f;
}

//更新
void Sukiru2::Update()
{

}

//描画
void Sukiru2::Draw()
{
	Image::SetMatrix(Sukiru2ict_, worldMatrix_);
	Image::Draw(Sukiru2ict_);
}

//開放
void Sukiru2::Release()
{
}