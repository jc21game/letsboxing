#include "Enemy.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/GameObject/Model.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), _hModel(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Enemy.fbx");
	assert(_hModel >= 0);
	//scale_.y = 10;
	rotate_.y = 90.0f;
	position_ = D3DXVECTOR3(2, 3, 15);
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);


}

//更新
void Enemy::Update()
{
	if (Input::IsKeyDown(DIK_W))
	{
		position_.z += 1.0f;
	}
	if (Input::IsKeyDown(DIK_S))
	{
		position_.z -= 1.0f;
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{
}