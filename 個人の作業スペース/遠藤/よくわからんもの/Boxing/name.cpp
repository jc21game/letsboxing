#include "name.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
name::name(IGameObject * parent)
	: IGameObject(parent, "name"), nameict_(-1)
{
}

//デストラクタ
name::~name()
{
}


//初期化
void name::Initialize()
{
	//画像データのロード
	nameict_ = Image::Load("data/name.png");
	assert(nameict_ >= 0);
	position_.y = 120;

}

//更新
void name::Update()
{

}

//描画
void name::Draw()
{
	Image::SetMatrix(nameict_, worldMatrix_);
	Image::Draw(nameict_);
}

//開放
void name::Release()
{
}