#include "time.h"
#include "Engine/GameObject/Image.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
time::time(IGameObject * parent)
	: IGameObject(parent, "time"), timeict_(-1)
{
}

//デストラクタ
time::~time()
{
}


//初期化
void time::Initialize()
{
	//画像データのロード
	timeict_ = Image::Load("data/time.png");
	assert(timeict_ >= 0);
	position_.x = 600;
	position_.y = 0;
	scale_.x = 0.5f;
	scale_.y = 0.5f;
	scale_.z = 0.5f;
}

//更新
void time::Update()
{

}

//描画
void time::Draw()
{
	Image::SetMatrix(timeict_, worldMatrix_);
	Image::Draw(timeict_);
}

//開放
void time::Release()
{
}