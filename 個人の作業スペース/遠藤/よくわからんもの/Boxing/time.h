#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//timeを管理するクラス
class time : public IGameObject
{
	int timeict_;    //モデル番号
public:
	//コンストラクタ
	time(IGameObject* parent);

	//デストラクタ
	~time();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};