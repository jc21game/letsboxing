#include "MenuScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene"), hPict_(-1)
{
}

//初期化
void MenuScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/MenuScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	matchLogo_ = CreateButtonObject<Button>(this, "data/MatchLogo.png", 80.0f, 440.0f);
	trainingLogo_ = CreateButtonObject<Button>(this, "data/TrainingLogo.png", 1200.0f, 440.0f);
}

//更新
void MenuScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool matchLogoFlag = matchLogo_->GetSceneChangeflag();
	bool trainingLogoFlag = trainingLogo_->GetSceneChangeflag();

	//受け取ったフラグで処理を変える
	if (matchLogoFlag == true)
	{
		//難易度選択シーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_DIFFICULTY);
	}
	else if (trainingLogoFlag == true)
	{
		//トレーニングシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TRAINING);
	}

	//ESCキーで前の画面に戻る
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void MenuScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void MenuScene::Release()
{
}