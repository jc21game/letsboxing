#include "Camera.h"
#include "../DirectX/Direct3D.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"),target_(0, 0, 0)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	//プロジェクション行列
	D3DXMATRIX proj;

	//プロジェクション行列を作る関数
	//パースペクティブは遠近という意味。略称パース
	//第一引数：プロジェクションに変換する行列
	//第二引数：視野角や画角。狭めると遠近感がなくなり、広くするとどぎつくなる
	//第三引数：アスペクト比。ウィンドウの縦横の比率を指定する
	//第四引数：カメラから何メートル先に描画するか。近クリッピング面までの距離
	//第五引数：第四引数の値から何メートル先に描画するか。遠クリッピング面までの距離
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 1.0f, 1000.0f);
	
	
	//プロジェクション行列として使ってくれというSetTransform
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	//変形を最初に入れることでカクツキを無くせる
	Transform();

	//世界から見た位置と見る位置
	D3DXVECTOR3 worldPosition, worldTarget;

	//ベクトルを変形させる
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;

	//ビュー行列を作る関数
	//第一引数：ビューに変換する行列
	//第二引数：カメラの位置（視点）
	//第三引数：どこを見るか（注視点、焦点）
	//第四引数：カメラの上方向ベクトル、角度を指定できる。カメラ時点が傾く(x方向限定)
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	
	//ビュー行列として使ってくれというSetTransform
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);

	
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}