#pragma once
#define DIRECTINPUT_VERSION 0x0800

#include <dInput.h>

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	//初期化処理
	//引数：hWnd	ウィンドウハンドル
	//戻値：なし
	void Initialize(HWND hWnd);
	
	//更新処理
	//引数：なし
	//戻値：なし
	void Update();
	
	//解放処理
	//引数：なし
	//戻値：なし
	void Release();


	/////////////////////////// キーボード //////////////////////////////////
	//任意のキーが押されているか
	//引数：keyCode	キーのコード
	//戻値：なし
	bool IsKey(int keyCode);

	//任意のキーを押した
	//引数：keyCode	キーのコード
	//戻値：なし
	bool IsKeyDown(int keyCode);

	//任意のキーを放した
	//引数：keyCode	キーのコード
	//戻値：なし
	bool IsKeyUp(int keyCode);


	/////////////////////////// マウス //////////////////////////////////
	//マウスのボタンが押されているか調べる
	//引数：buttonCode 調べたいボタンの番号
	//戻値：押されていればtrue
	bool IsMouseButton(int buttonCode);

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：buttonCode 調べたいボタンの番号
	//戻値：押した瞬間だったらtrue
	bool IsMouseButtonDown(int buttonCode);

	//マウスのボタンを今放したか調べる
	//引数：buttonCode 調べたいボタンの番号
	//戻値：放した瞬間だったらtrue
	bool IsMouseButtonUp(int buttonCode);

	//マウスカーソルの位置を取得
	//戻値：マウスカーソルの位置
	D3DXVECTOR3 GetMousePosition();

	//そのフレームでのマウスの移動量を取得
	//戻値：X,Y マウスの移動量 ／ Z,ホイールの回転量
	D3DXVECTOR3 GetMouseMove();
};