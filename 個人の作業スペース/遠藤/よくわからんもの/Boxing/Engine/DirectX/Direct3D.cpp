#include "Direct3D.h"

LPDIRECT3D9			Direct3D::pD3d = nullptr;		//Direct3Dオブジェクト
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;	//Direct3Dデバイスオブジェクト

void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	//作成というお決まりの記述！！！！！！！！！！！！！！
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);
	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	//これもお決まり
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	//TRUEは普通の画面、FALSEは全画面になる
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);

	//アルファブレンド...半透明
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);	//TRUEでライティングをON

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));	//構造体を初期化
	lightState.Type = D3DLIGHT_DIRECTIONAL;	//平行光源
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//ライトの向き
	lightState.Diffuse.r = 1.0f;	//R
	lightState.Diffuse.g = 1.0f;	//G	色を指定
	lightState.Diffuse.b = 1.0f;	//B
	pDevice->SetLight(0, &lightState);	//ライトを作成
	pDevice->LightEnable(0, TRUE);	//ライトのスイッチをON

	//とりあえずのカメラ
	//ビュー行列とプロジェクション行列を作成
	D3DXMATRIX view, proj;
	
	//ビュー行列を作る関数
	//第一引数：ビューに変換する行列
	//第二引数：カメラの位置（視点）
	//第三引数：どこを見るか（注視点、焦点）
	//第四引数：カメラの上方向ベクトル、角度を指定できる。カメラ時点が傾く(x方向限定)
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 7, -1), &D3DXVECTOR3(0, 7, 0), &D3DXVECTOR3(0, 1, 0));
	//ビュー行列として使ってくれというSetTransform
	pDevice->SetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列を作る関数
	//パースペクティブは遠近という意味。略称パース
	//第一引数：プロジェクションに変換する行列
	//第二引数：視野角や画角。狭めると遠近感がなくなり、広くするとどぎつくなる
	//第三引数：アスペクト比。ウィンドウの縦横の比率を指定する
	//第四引数：カメラから何メートル先に描画するか。近クリッピング面までの距離
	//第五引数：第四引数の値から何メートル先に描画するか。遠クリッピング面までの距離
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);
	//プロジェクション行列として使ってくれというSetTransform
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

void Direct3D::BeginDraw()
{
	//画面をクリア
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(255, 255, 255), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	//開放処理
	//開放の順番は、作った順番の逆から開放していく（ダンボールに入ってる紙と同じ）
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pD3d);
}
