#include "OperationMemoScene.h"
#include "Engine/GameObject/Image.h"

//コンストラクタ
OperationMemoScene::OperationMemoScene(IGameObject * parent)
	: IGameObject(parent, "OperationMemoScene"), hPict_(-1)
{
}

//初期化
void OperationMemoScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/OperationMemoScene.png");
	assert(hPict_ >= 0);
}

//更新
void OperationMemoScene::Update()
{
	//ESCキーでオプション画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_OPTION);
	}
}

//描画
void OperationMemoScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OperationMemoScene::Release()
{
}