#pragma once
#include "Engine/ObjectTree/IGameObject.h"
//Sukiruを管理するクラス
class Sukiru : public IGameObject
{
	int Sukiruict_;    //モデル番号
public:
	//コンストラクタ
	Sukiru(IGameObject* parent);

	//デストラクタ
	~Sukiru();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};