#include "Left_Arm.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/GameObject/Model.h"

//コンストラクタ
Left_Arm::Left_Arm(IGameObject * parent)
	:IGameObject(parent, "Left_Arm"), _hModel(-1)
{
}

//デストラクタ
Left_Arm::~Left_Arm()
{
}

//初期化
void Left_Arm::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Left_Arm.fbx");
	assert(_hModel >= 0);
	position_ = D3DXVECTOR3(1, 6.5, 15);
	rotate_.x = 120.0f;
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);


}

//更新
void Left_Arm::Update()
{
	if (Input::IsKeyDown(DIK_Q))
	{
		position_.z += 20;
	}
	if (Input::IsKeyUp(DIK_Q))
	{
		position_.z -= 20;
	}
}

//描画
void Left_Arm::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Left_Arm::Release()
{
}