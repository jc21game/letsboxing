#include "TrainingScene.h"
#include "Engine/GameObject/Image.h"

//コンストラクタ
TrainingScene::TrainingScene(IGameObject * parent)
	: IGameObject(parent, "TrainingScene"), hPict_(-1)
{
}

//初期化
void TrainingScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/TrainingScene.png");
	assert(hPict_ >= 0);
}

//更新
void TrainingScene::Update()
{
	//ESCキーでオプション画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_OPTION);
	}
}

//描画
void TrainingScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void TrainingScene::Release()
{
}