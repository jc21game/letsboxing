#pragma once
#include "Engine/Global.h"

class Button;

//リタイアシーンを管理するクラス
class RetireScene : public IGameObject
{
	int hPict_;			//画像番号
	Button* yesLogo_;	//YESロゴ
	Button* noLogo_;	//NOロゴ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	RetireScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};