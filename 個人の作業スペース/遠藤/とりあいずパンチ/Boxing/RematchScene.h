#pragma once
#include "Engine/Global.h"

class Button;

//再戦シーンを管理するクラス
class RematchScene : public IGameObject
{
	int hPict_;			//画像番号
	Button* yesLogo_;	//YESロゴ
	Button* noLogo_;	//NOロゴ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	RematchScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};