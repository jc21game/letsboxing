#include "RetireScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

//コンストラクタ
RetireScene::RetireScene(IGameObject * parent)
	: IGameObject(parent, "RetireScene"), hPict_(-1)
{
}

//初期化
void RetireScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/RetireScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	yesLogo_ = CreateButtonObject<Button>(this, "data/YesLogo.png", 80.0f, 440.0f);
	noLogo_ = CreateButtonObject<Button>(this, "data/NoLogo.png", 1200.0f, 440.0f);
}

//更新
void RetireScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool yesLogoFlag = yesLogo_->GetSceneChangeflag();
	bool noLogoFlag = noLogo_->GetSceneChangeflag();

	//受け取ったフラグで処理を変える
	if (yesLogoFlag == true)
	{
		//メニューシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
	else if (noLogoFlag == true)
	{
		//リタイア画面を閉じる
		yesLogo_->KillMe();
		noLogo_->KillMe();
		KillMe();
	}
}

//描画
void RetireScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void RetireScene::Release()
{
}