#include "PlayScene.h"
#include "Stage.h"
#include "Player.h"
#include "Right_Arm.h"
#include "Left_Arm.h"
#include "HP.h"
#include "Engine/GameObject/Image.h"
#include "Engine/gameObject/Camera.h"
#include "RetireScene.h"
#include "ResultScene.h"

#define WIN 0
#define LOSE 1

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), hPict_(-1)
{
}

//初期化
void PlayScene::Initialize()
{
	//画像データのロード
	/*hPict_ = Image::Load("data/PlayScene.png");
	assert(hPict_ >= 0);*/
	CreateGameObject<Stage>(this);
	CreateGameObject<Player>(this);
	CreateGameObject<Right_Arm>(this);
	CreateGameObject<Left_Arm>(this);
	CreateGameObject<HP>(this);
	//Camera* pCamera = CreateGameObject<Camera>(this);

}

//更新
void PlayScene::Update()
{
	//ESCキーでリタイア画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		CreateGameObject<RetireScene>(this);
	}
	////左クリックで勝ちのリザルト画面
	//if (Input::IsKeyDown(DIK_1))
	//{
	//	ResultScene* pResultScene = new ResultScene(this);
	//	this->PushBackChild(pResultScene);
	//	pResultScene->Setjudgment(WIN);
	//	pResultScene->Initialize();
	//}
	////右クリックで負けのリザルト画面
	//if (Input::IsKeyDown(DIK_2))
	//{
	//	ResultScene* pResultScene = new ResultScene(this);
	//	this->PushBackChild(pResultScene);
	//	pResultScene->Setjudgment(LOSE);
	//	pResultScene->Initialize();
	//}
}

//描画
void PlayScene::Draw()
{
	/*Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);*/
}

//開放
void PlayScene::Release()
{
}