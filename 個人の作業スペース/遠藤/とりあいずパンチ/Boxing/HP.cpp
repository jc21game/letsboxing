#include "HP.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/GameObject/Model.h"

//コンストラクタ
HP::HP(IGameObject * parent)
	:IGameObject(parent, "HP"), _hModel(-1)
{
}

//デストラクタ
HP::~HP()
{
}

//初期化
void HP::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/HP.fbx");
	assert(_hModel >= 0);
	position_ = D3DXVECTOR3(-16, 8, 20);
	rotate_.y = -10.0f;
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);


}

//更新
void HP::Update()
{
}

//描画
void HP::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void HP::Release()
{
}