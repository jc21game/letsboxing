#include "Stage.h"
#include "Engine/GameObject/Model.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), _hModel(-1)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Stage.fbx");
	assert(_hModel >= 0);
	position_ = D3DXVECTOR3(0, -5, 20);
	SetCollider(D3DXVECTOR3(0, 0, 0), 2.0f);
	
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);
	Model::Draw(_hModel);
}

//開放
void Stage::Release()
{
}