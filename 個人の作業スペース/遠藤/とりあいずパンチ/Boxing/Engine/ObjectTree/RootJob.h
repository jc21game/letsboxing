#pragma once
#include "IGameObject.h"
class RootJob : public IGameObject
{
public:
	//コンストラクタ
	RootJob();

	//デストラクタ
	~RootJob();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

