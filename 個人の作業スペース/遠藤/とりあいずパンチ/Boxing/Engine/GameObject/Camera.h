#pragma once
#include "../ObjectTree/IGameObject.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	D3DXVECTOR3 target_; //カメラが見る変数


public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//見る位置のセッター
	//引数：target、見る位置の値を受け取る
	//戻値：なし
	void SetTarget(D3DXVECTOR3 target)
	{
		target_ = target;
	}
};