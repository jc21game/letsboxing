#include <vector>
#include "Model.h"

namespace Model
{
	//モデルデータリストの配列
	std::vector<ModelData*> dataList;

	int Load(std::string fileName)
	{
		//モデルデータ
		ModelData* pData = new ModelData;

		//受け取ったファイルネームをモデルデータに突っ込む
		pData->fileName = fileName;

		//isExist...存在するかどうか
		bool isExist = false;
		//同じ作成を何回もやらないようにするためのループ
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//既に同じファイル名があった場合
			if (dataList[i]->fileName == fileName)
			{
				//pFbxのアドレス「だけ」をいれる
				pData->pFbx = dataList[i]->pFbx;
				
				//同じのがあった！
				isExist = true;

				//その後の処理は不要のためブレイク
				break;
			}
		}

		//存在しなかったときの処理
		if (isExist == false)
		{
			//Fbx型のモデルデータを作成
			pData->pFbx = new Fbx;

			//c_str()を使うと、キャラ型のデータに変えてくれる
			pData->pFbx->Load(fileName.c_str());
		}

		//データを入れる
		dataList.push_back(pData);

		//ファイルの番号。-1をすることで正確な値を返せる
		return dataList.size() - 1;
	}

	void Draw(int handle)
	{
		//データリストのハンドル番目にあるマトリックスを引数にして、描画する
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX &matrix)
	{
		//行列をそのモデルにセット
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		{
			//同じFbxが他にも使ってないか
			bool isExist = false;
			for (unsigned int i = 0; i < dataList.size(); i++)
			{
				//そのFbxが同じで、i番目のデータリストがNULLじゃなくて、自分同士じゃなければ
				if (i != handle && dataList[i] != nullptr &&
					dataList[i]->pFbx == dataList[handle]->pFbx)
				{
					//処理をスキップ
					isExist = true;
					break;
				}
			}

			//なければ解放
			if (isExist == false)
			{
				SAFE_DELETE(dataList[handle]->pFbx);
			}

			//これは問答無用で解放
			SAFE_DELETE(dataList[handle]);
		}
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//まだ削除していなければ
			if (dataList[i] != nullptr)
			{
				//解放
				Release(i);
			}
		}
		//全要素消去
		dataList.clear();
	}

	void RayCast(int handle, RayCastData & data)
	{
		dataList[handle]->pFbx->
			RayCast(data, dataList[handle]->matrix);
	}

}