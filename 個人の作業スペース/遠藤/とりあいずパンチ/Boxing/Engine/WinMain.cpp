//インクルード
#include <Windows.h>
#include "Global.h"
#include "DirectX/Direct3D.h"
#include "DirectX/Sprite.h"
#include "DirectX/Fbx.h"
#include "DirectX/Input.h"
#include "ObjectTree/RootJob.h"
#include "GameObject/Model.h"
#include "GameObject/Image.h"
#include "GameObject/Audio.h"


//デバッグモードだったら
#ifdef _DEBUG
//これを実行
#include <crtdbg.h>
//endifはセット
#endif

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//定数
const char* WIN_CLASS_NAME = "SampleGame";
const int	WINDOW_WIDTH = 1920;	 //ウィンドウの幅
const int	WINDOW_HEIGHT = 1080;	//ウィンドウの高さ

//プロトタイプ宣言
//関数より上にmainを書くときは、プロトタイプ宣言が必要
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//グローバル変数
Global g;
RootJob *pRootJob;

//エントリーポイント
//hInstance...インスタンスごとに割り当てられた番号(ハンドル)
//hPrevInst...昔の名残
//lpCmdLine...ツール開発で意識。起動したプログラムの追加情報
//nCmdShow...使ってない
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	//○○sizeときたら、sizeofを使う
	wc.cbSize = sizeof(WNDCLASSEX);	            //この構造体のサイズ
	//インスタンスハンドルをhInstanceに入れる
	wc.hInstance = hInstance;	                  //インスタンスハンドル
	//"SampleGame"はウィンドウの名前
	wc.lpszClassName = WIN_CLASS_NAME;	        	  //ウィンドウクラス名
	//ウィンドウプロシージャの名前を書いて、呼び出す
	wc.lpfnWndProc = WndProc;	                  //ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;	        //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	  //小さいアイコン
	//IDC_ARROWでカーソルを指定できる。カーソルは白黒でしかできないが
	//非表示にして絵を載せれば、色付きでも可能
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	  //マウスカーソル
	wc.lpszMenuName = NULL;	                    //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	//RegisterClassExに設計図を入れる！
	RegisterClassEx(&wc);	//クラスを登録

	//ウィンドウサイズの計算
	//スタイルと同じにしないとおかしくなる
	//winRectを使うことで、白い画面のサイズに合わせることができる
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);

	//ウィンドウを作成
	//hWndはウィンドウハンドルを扱う。hが来たら○○ハンドル！
	//ウィンドウを出すごとに番号を割り振る
	HWND hWnd = CreateWindow(
		//ウィンドウクラス名は、設計図に書いた名前と同じにする
		WIN_CLASS_NAME,	       //ウィンドウクラス名
		"サンプルゲーム",    	//タイトルバーに表示する内容
		//WS_POPUPは閉じるボタンとかも全部消す
		WS_VISIBLE | WS_POPUP,				//スタイル（子ウィンドウ）
		0,						//表示位置左（親ウィンドウの0,0の位置になる）
		0,						//表示位置上（上記と同じ）
		winRect.right - winRect.left,	       //ウィンドウ幅
		winRect.bottom - winRect.top,	       //ウィンドウ高さ
		NULL,	               //親ウインドウ（なし）
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス
		NULL	                //パラメータ（なし）
	);

	//create系で作ったのはassertでエラー処理をする
	assert(hWnd != NULL);

	//ウィンドウを表示
	//hWndで作成し、mCmdShowで番号を振る
	ShowWindow(hWnd, nCmdShow);

	//画面サイズにウィンドウサイズを入れる
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの初期化（準備）
	Direct3D::Initialize(hWnd);

	//DirectInputの初期化
	Input::Initialize(hWnd);

	//Audioの初期化（準備）

	//RootJobの初期化
	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	//ZeroMemoryで構造体の中身を全部0にする
	ZeroMemory(&msg, sizeof(msg));
	//WM_QUITは閉じるということ
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		//ウィンドウ側に何かメッセージがあったとき(マウスの右クリック？)
		//PeekMessageは二種類ある
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			//メッセージ駆動型...何かが起きたら処理をする
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

			//情報の更新
			Input::Update();
			pRootJob->Update();

			//描画開始
			Direct3D::BeginDraw();

			//RootJobの更新（IGaneObjectを継承）
			pRootJob->UpdateSub();

			//RootJobの描画（IGaneObjectを継承）
			pRootJob->DrawSub();

			//描画終了
			Direct3D::EndDraw();
		}
	}

	//解放処理
	Model::AllRelease();
	Image::AllRelease();
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();

	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
//ユーザが何かをしたときに反応する
//msg...メッセージの略。何が起きたか入る
//wParam, lParam...情報次第で中身が変わる
//何かあったときに呼ばれるのがCALLBACK
//RESULTは結果を返す。LRESULTとHRESULTの二種類がある
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//WM_CREATE...ウィンドウが作られた時
	//受け取った関数msgから、switchのcaseで処理が分けられる
	switch (msg)
	{
	case WM_DESTROY:		//ウィンドウが閉じた後に動く
		PostQuitMessage(0);	//プログラム終了!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		return 0;
	}
	//全部書くのはめんどいため
	//デフォルトでいい場合はDefWindowProcでOK、設定したときは0でOK
	return DefWindowProc(hWnd, msg, wParam, lParam);
}