#include "ResultScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent, bool winFlag)
	: IGameObject(parent, "ResultScene"), hPict_(-1),
	winFlag_(winFlag)
{
}

//初期化
void ResultScene::Initialize()
{
	//画像データのロード
	//勝ったかフラグが真なら勝ち画像表示
	if (winFlag_ == true)
	{
		hPict_ = Image::Load("data/Win.png");
		assert(hPict_ >= 0);
	}
	//それ以外なら負け画像
	else
	{
		hPict_ = Image::Load("data/Lose.png");
		assert(hPict_ >= 0);
	}

	//ロゴ生成
	yesLogo_ = CreateGameObject<PictDisplay>(this, "data/YesLogo.png", "data/SelectYesLogo.png", 80.0f, 440.0f);
	noLogo_ = CreateGameObject<PictDisplay>(this, "data/NoLogo.png", "data/SelectNoLogo.png", 1200.0f, 440.0f);
}

//更新
void ResultScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//YESロゴ選択時
	case STATE_YES:
		//ロゴが黄色くなる
		yesLogo_->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//リロード
			SceneManager* pSceneManager;
			pSceneManager->ReloadScene();
		}
		//右キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_NO;
			yesLogo_->UnSelection();
		}
		//左キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_NO;
			yesLogo_->UnSelection();
		}
		break;

	//NOロゴ選択時
	case STATE_NO:
		//ロゴが黄色くなる
		noLogo_->Selection();

		//Enterでメニュー画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);
		}
		//右キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_YES;
			noLogo_->UnSelection();
		}
		//左キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_YES;
			noLogo_->UnSelection();
		}
		break;
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}