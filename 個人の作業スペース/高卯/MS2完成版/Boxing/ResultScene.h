#pragma once
#include "Engine/Global.h"

class PictDisplay;

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//状態の種類
	enum STATE {
		STATE_YES,	//YES
		STATE_NO	//NO
	} state_;

	int hPict_;				//画像番号
	PictDisplay* yesLogo_;	//YESロゴ
	PictDisplay* noLogo_;	//NOロゴ
	bool winFlag_;			//勝ったかフラグ
public:
	//コンストラクタ
	//引数１：parent  親オブジェクト（SceneManager）
	//引数２：winFlag 試合に買ったかどうか
	ResultScene(IGameObject* parent, bool winFlag);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};