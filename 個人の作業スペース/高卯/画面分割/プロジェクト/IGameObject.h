﻿#pragma once
#include <d3dx9.h>
#include <vector>
#include <list>


class Collider;

class IGameObject
{
protected:
	IGameObject* pParent_;
	std::list<IGameObject*>	childList_;
	std::string name_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	D3DXMATRIX localMatrix_;
	D3DXMATRIX worldMatrix_;

	bool dead_;

	Collider	*pCollider_;

	void Transform();

public:
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	virtual ~IGameObject();
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;

	void UpdateSub();
	virtual void DrawSub();
	void ReleaseSub();

	void KillMe();

	void SetCollider(D3DXVECTOR3& center, float radius);

	void Collision(IGameObject* targetObject);

	virtual void OnCollision(IGameObject* target);


	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		T* p = new T(parent);
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}


	void PushBackChild(IGameObject* pObj);

	//位置を設定
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
	void SetRotation(D3DXVECTOR3 rotate) { rotate_ = rotate; }
	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }

	D3DXVECTOR3 GetPosition() { return position_; }
	D3DXVECTOR3 GetRotate() { return rotate_; }
	D3DXVECTOR3 GetScale() { return scale_; }
};

