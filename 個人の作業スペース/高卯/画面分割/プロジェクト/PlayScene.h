﻿#pragma once

#include "global.h"

class Camera;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void DrawSub()	override;
};