﻿#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"

#ifdef _DEBUG
#include <crtdbg.h>
#endif


//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//定数
const char* WIN_CLASS_NAME = "SampleGame";	//ウィンドウクラス名
const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//グローバル変数
Global g;
RootJob *pRootJob;

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);	            //この構造体のサイズ
	wc.hInstance = hInstance;	                  //インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;	        	  //ウィンドウクラス名
	wc.lpfnWndProc = WndProc;	                  //ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;	        //スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	  //小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	  //マウスカーソル
	wc.lpszMenuName = NULL;	                    //メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	RegisterClassEx(&wc);	//クラスを登録

	RECT winRect = { 0, 0, WINDOW_WIDTH , WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);


	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,	       //ウィンドウクラス名
		"おでん360",    	//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,	//スタイル（普通のウィンドウ）
		CW_USEDEFAULT,	      //表示位置左（おまかせ）
		CW_USEDEFAULT,	      //表示位置上（おまかせ）
		winRect.right - winRect.left,	       //ウィンドウ幅
		winRect.bottom - winRect.top,	       //ウィンドウ高さ
		NULL,	               //親ウインドウ（なし）
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス
		NULL	                //パラメータ（なし）
	);
	assert(hWnd != NULL);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの初期化（準備）
	Direct3D::Initialize(hWnd);

	//DirectInputの初期化
	Input::Initialize(hWnd);

	pRootJob = new RootJob;
	pRootJob->Initialize();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//ゲームの処理

		   //入力情報の更新
			Input::Update();
			pRootJob->UpdateSub();

			Direct3D::BeginDraw();

			pRootJob->DrawSub();

			Direct3D::EndDraw();
		}

	}

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	Direct3D::Release();
	Model::AllRelease();

	return 0;

}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);	//プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}