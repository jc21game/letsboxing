﻿#include "Model.h"
#include <vector>

//CPP
namespace Model
{
	std::vector<ModelData*>	dataList;


	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		bool isExist = false;
		for (int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName.c_str());
		}

		dataList.push_back(pData);

		return dataList.size() - 1;
	}

	void Draw(int handle)
	{
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		bool isExist = false;
		for (int i = 0; i < dataList.size(); i++)
		{
			if (i != handle && dataList[i] != nullptr &&
				dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (int i = 0; i < dataList.size(); i++)
		{
			if(dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}

};