﻿#pragma once
#include "Global.h"

class Quad
{
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;

	LPDIRECT3DTEXTURE9	pTexture_; //テクスチャ
	D3DMATERIAL9         material_;

public:
	Quad();
	~Quad();

	void Load(const char* fileName);
	void Draw(const D3DXMATRIX &matrix);
};