#include "MiniOden.h"
#include "Model.h"

//コンストラクタ
MiniOden::MiniOden(IGameObject * parent)
	:IGameObject(parent, "MiniOden"), hModel_(-1)
{
}

//デストラクタ
MiniOden::~MiniOden()
{
}

//初期化
void MiniOden::Initialize()
{
	hModel_ = Model::Load("Data/Oden.fbx");
}

//更新
void MiniOden::Update()
{
	scale_ = D3DXVECTOR3(0.5f, 0.5f, 0.5f);

	rotate_.y += 10;

	position_ = D3DXVECTOR3(5, 0, 0);

	//�@行列の変数作る
	D3DXMATRIX my;

	//�A　�@で作った変数を回転行列にする
	//　　　（Ｙ軸でrotate_y度回転させる）
	D3DXMatrixRotationY(&my, D3DXToRadian(rotate_.y));

	//�Bposition_を�Aの行列で変形させる
	D3DXVec3TransformCoord(&position_, &position_, &my);

}

//描画
void MiniOden::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void MiniOden::Release()
{
}