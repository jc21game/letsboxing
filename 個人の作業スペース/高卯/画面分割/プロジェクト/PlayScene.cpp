﻿#include "PlayScene.h"
#include "Player.h"
#include "Camera.h"
#include "Direct3D.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	Player *pOden = CreateGameObject<Player>(this);


	//プロジェクション行列
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth/ 2 / g.screenHeight, 1.0f, 100.0f);
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void PlayScene::Update()
{


	if (Input::IsKeyDown(DIK_SPACE))
	{
		CreateGameObject<Player>(this);
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

