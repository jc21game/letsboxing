#pragma once
#include "Engine/Global.h"

class PictDisplay;

//プレイシーンを管理するクラス
class SplashScene : public IGameObject
{
	enum STATE {
		STATE_FRONT,
		STATE_BACK,
		STATE_UNION
	} state_;

	int hPict_;			//画像番号
	float alphaValue_;	//画像の透明度
	bool alphaValueflag_;
	double startClock_;	//タイマーの計測開始時間
	time_t startTime_;	//ロゴ合体後から始まった時間
	time_t endTime_;	//ロゴ合体後、終わった時間

	bool sceneSwitchFlag_;	//シーン切り替えフラグ

	PictDisplay* schoolLogoFront_;
	PictDisplay* schoolLogoBack_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};