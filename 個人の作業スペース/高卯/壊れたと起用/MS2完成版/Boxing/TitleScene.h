#pragma once
#include "Engine/Global.h"

class PictDisplay;

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	int hPict_;					//画像番号
	float moveY_;				//画像を上に動かすY軸方向への移動
	D3DXMATRIX move_;			//画像を動かす行列
	PictDisplay* StartLogo_;	//ゲームスタートのロゴ
	time_t sceneStartTime_;		//このシーンになった時の時間
	bool gameStartFlag_;		//ゲームスタートのロゴが表示されているか

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
