#include "OperationMemoScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "Engine/Input.h"

//コンストラクタ
OperationMemoScene::OperationMemoScene(IGameObject * parent)
	: IGameObject(parent, "OperationMemoScene"), hPict_(-1), posY_(0),
	posLogoX_(80.0f), posLogoY_(50.0f),state_(STATE_PUNCH)
{
}

//初期化
void OperationMemoScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/Menu.png"); 
	assert(hPict_ >= 0);

	//CreateGameObject<PictDisplay>(this, "data/OperationMemoLogo.png", "", 50.0f, 50.0f);
	punchLogo_ = CreateGameObject<PictDisplay>(this, "data/pu.png", "data/punch.png", posLogoX_, posLogoY_);
}

//更新
void OperationMemoScene::Update()
{
	//ESCキーでメニュー画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
	//選択されている状態
	switch (state_)
	{
		//試合ロゴ選択時
	case STATE_PUNCH:
		punchLogo_->Selection();
		
		if(Input::IsKeyDown(DIK_RETURN))
		{
			posY_ = -1024;
				D3DXMatrixTranslation(&picMove_, 0, posY_, 0);
		}
	}
	WeelOperation();
}

//描画
void OperationMemoScene::Draw()
{
	Image::SetMatrix(hPict_, picMove_);
	Image::Draw(hPict_);
}

//開放
void OperationMemoScene::Release()
{
}

void OperationMemoScene::WeelOperation()
{
	float wheelSave;

	//inputの GetMouseMove()を用いてホイールの回転量を取得
	//zが回転量
	if (wheelSave = Input::GetMouseMove().z)
	{

		//回転量と現在の値を足した値
		posY_ += wheelSave;

		//画像のないところまでスクロール出来てしまうので画像の域を出ないようにする
		if (posY_ > 0)
		{
			posY_ = 0;
		}
		D3DXMatrixTranslation(&picMove_, 0, posY_, 0);
	}
	//UPキーが押されたら
	if (Input::IsKey(DIK_UP))
	{
		//画像の位置情報を上げる
		posY_ += 5.0f;
		if (posY_ > 0)
		{
			posY_ = 0;
		}
		D3DXMatrixTranslation(&picMove_, 0, posY_, 0);
	}
	//DOWNキーが押されたら	
	else if (Input::IsKey(DIK_DOWN))
	{
		posY_ -= 5.0f;
		D3DXMatrixTranslation(&picMove_, 0, posY_, 0);
	}

}
