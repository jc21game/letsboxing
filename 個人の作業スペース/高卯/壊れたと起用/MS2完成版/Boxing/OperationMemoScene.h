#pragma once
#include "Engine/Global.h"

class PictDisplay;

//操作説明シーンを管理するクラス
class OperationMemoScene : public IGameObject
{
	//状態の種類
	enum STATE {
		STATE_PUNCH,			//パンチ
		//STATE_OPERATION_MEMO	//操作説明
	} state_;

	int hPict_;    //画像番号
	float posY_; //画像のポジション
	PictDisplay* punchLogo_;
	D3DXMATRIX picMove_; //mousuの移動行列
	float posLogoX_;
	float posLogoY_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	OperationMemoScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void WeelOperation();
};
