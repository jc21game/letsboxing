#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Direct3D.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
SphereCollider::SphereCollider(const std::string& name, D3DXVECTOR3 center, float radius) : Collider(name)
{
	center_ = center;
	radius_ = radius;
	type_ = COLLIDER_CIRCLE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}

SphereCollider::SphereCollider(D3DXVECTOR3 center, float radius) : SphereCollider("", center, radius)
{
}



//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool SphereCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, this);
	else
		return IsHitCircleVsCircle((SphereCollider*)target, this);
}