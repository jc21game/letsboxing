#pragma once
#include "Engine/Global.h"

class Button;

//メニューシーンを管理するクラス
class MenuScene : public IGameObject
{
	int hPict_;				//画像番号
	Button* matchLogo_;		//マッチロゴ
	Button* trainingLogo_;	//トレーニングロゴ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	MenuScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};