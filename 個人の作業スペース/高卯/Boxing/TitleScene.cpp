#include "TitleScene.h"
#include "Engine/GameObject/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), hPict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/レッツボクシング  タイトル　パンチシルエットなし.jpg");
	assert(hPict_ >= 0);
}

//更新
void TitleScene::Update()
{
	//左クリックしたらメニューシーンへ移行
	if (Input::IsMouseButtonDown(0))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void TitleScene::Release()
{
}