#pragma once
#include "Engine/Global.h"
#include <string>
#include "Text.h"

class ResultScene;
using namespace std;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	Text* pText_;
	int hPict_;					//画像番号
	ResultScene* pResultScene_; //リザルトシーン
	//表示する数値を格納する
	string second_[11];
	string minute_[11];
	//1の位を数える変数
	int sec_;
	//10の位
	int tensec_;
	//分カウント
	int min_;
	//fremをカウントする
	int frame_;
	bool ReleaseFlag_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void timecnt();

	
};
