#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "DirectX/Direct3D.h"
#include "DirectX/Input.h"
#include "ObjectTree/IGameObject.h"
#include "ObjectTree/SceneManager.h"

//マクロ
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

#define VERTICAL 15
#define SIDE 15

//グローバルなウィンドウサイズ
struct Global
{
	int screenWidth;
	int screenHeight;
};
//エクスターンで全ファイルで操作可能
//変数を他ファイルでも使用可能
extern Global g;