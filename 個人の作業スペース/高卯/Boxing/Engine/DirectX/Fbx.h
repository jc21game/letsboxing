#pragma once
#include <fbxsdk.h>
#include "../Global.h"

#pragma comment(lib,"libfbxsdk-mt.lib")

struct RayCastData
{
	D3DXVECTOR3 start;		//レイの発射位置
	D3DXVECTOR3 direction;	//レイの発射方向
	float distance;			//レイが当たった点までの距離
	bool hit;				//レイが当たったかどうか
};

class Fbx
{
	//頂点情報と色情報の構造体
	struct Vertex
	{
		//宣言の上から書かないといけない。
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線
		D3DXVECTOR2 uv;		//UVの座標
	};

	FbxManager*  pManager_;	//機能を管理するマネージャー
	FbxImporter* pImporter_;	//インポーター。ファイルを開く部下みたいなやつ
	FbxScene*    pScene_;	//ファイル管理のシーン

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;	//頂点バッファ
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;	//インデックスバッファ
	LPDIRECT3DTEXTURE9*	ppTexture_;	//テクスチャ(複数あるため配列)
	D3DMATERIAL9*       pMaterial_;	//マテリアル(複数あるため配列)

	int vertexCount_;	//頂点の数が入るメンバ変数
	int polygonCount_;	//ポリゴン数が入るメンバ変数
	int indexCount_;	//インデックス数が入るメンバ変数
	int materialCount_; //マテリアル数が入るメンバ変数
	int* polygonCountOfMaterial_;	//マテリアルごとのポリゴン数が入る配列の変数

	//ノードの内容をチェック
	//引数：pNode	自分のノード
	//戻値：なし
	void CheckNode(FbxNode* pNode);

	//情報を取り出してバーテックスバッファとインデックスバッファくを作る
	//引数：pMesh	メッシュ情報が入ってるノード
	//戻値：なし
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx();
	~Fbx();

	//頂点と色を作成する
	//引数：FileName	ファイルの名前
	//戻値：なし
	void Load(const char* FileName);

	//描画処理をする
	//引数：matrix	行列変形
	//戻値：なし
	void Draw(const D3DXMATRIX &matrix);

	//モデルとレイとの当たり判定
	//引数：data　レイキャストを使うためのデータ　matrix ワールド行列
	void RayCast(RayCastData& data, const D3DXMATRIX &matrix);
};