#include "Fbx.h"
#include "Direct3D.h"



Fbx::Fbx() :
	pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr),
	pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), ppTexture_(nullptr), pMaterial_(nullptr),
	vertexCount_(0), polygonCount_(0), indexCount_(0), materialCount_(0),
	polygonCountOfMaterial_(nullptr)
{
	//初期化処理
}


Fbx::~Fbx()
{
	//解放処理
	SAFE_DELETE_ARRAY(pMaterial_);
	for (int i = 0; i < materialCount_; i++)
	{
		//棚の中にある棚を一個ずつ開放していく
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(ppTexture_[i]);
	}
	//最後にでっかい棚を開放する（イメージです）
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(ppTexture_);
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char* FileName)
{
	//マネージャーを作成し、それをもとにインポーターとシーンを作成
	pManager_ = FbxManager::Create();
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	//インポーターでFBXファイルを開く
	pImporter_->Initialize(FileName);

	//ファイルをシーンに渡す
	pImporter_->Import(pScene_);

	//インポーターを開放
	pImporter_->Destroy();

	//変える前に今のカレントディレクトリを調べて、終わったら元に戻す
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをDate/にすることで、フォルダ内のみを見るようにしている
	char dir[MAX_PATH];
	_splitpath_s(FileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	//FBXファイルを読み込んだシーンのルートノードを取り出す
	FbxNode* rootNode = pScene_->GetRootNode();

	//ルートノードの子供を調べる
	int childCount = rootNode->GetChildCount();

	//子供の数分だけループ
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	//元に戻している
	//Linuxのcdと同じ
	SetCurrentDirectory(defaultCurrentDir);
}



void Fbx::CheckNode(FbxNode* pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();	//マテリアルの数を調べる
		pMaterial_ = new D3DMATERIAL9[materialCount_];	//マテリアルの動的配列を作る
		ppTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];	//テクスチャの動的配列を作る

		//////////////////////////////////////////////////////
		//for (int i = 0; i < materialCount_; i++)
		//{
		//	pTexture_[i] = nullptr;
		//}
		////////////////////////////////////////////////////////
		
		//ダブルポインタの中身をnullptrするためには、ZeroMemoryを使うとよい
		ZeroMemory(ppTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);
		
		////////////////////////////////////////////////////////

		for (int i = 0; i < materialCount_; i++)
		{
			//マテリアルの情報を読み込む
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			
			//ディフューズ(ポリゴンの色)取得
			FbxDouble3 diffuse = lambert->Diffuse;

			//アンビエント(環境光)取得
			FbxDouble3 ambient = lambert->Ambient;

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			//配列に入れる。Quadのマテリアルを思い出すんだ！
			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;

			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			//テクスチャの情報を取得
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			
			//使われたファイルの情報を取得
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);
			
			//nullptrじゃなかったらファイル名を入れる
			if (textureFile == nullptr)
			{
				ppTexture_[i] = nullptr;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				//ファイル名と拡張子を入れる
				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				
				//第一引数：第三と第四の文字列を連結して入れられる
				//第二引数：文字を表示
				//第三・第四引数：文字列が入ってる変数
				wsprintf(name, "%s%s", name, ext);

				//いつも通りテクスチャを作成
				//ポインタがcdcdcd...だと失敗している
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
					D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &ppTexture_[i]);
				assert(ppTexture_[i] != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			//再帰呼び出しで子供をどんどん調べていく
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//専用の型のポインタを用意して、頂点情報を読み込む
	//このvertexは二次元配列として扱うことができ、
	//pVertexPos[0][0]には最初の頂点のX座標、
	//pVertexPos[0][1]には最初の頂点のY座標、
	//pVertexPos[0][2]には最初の頂点のZ座標、
	//そしてpVertexPos[1][0]には2番目の頂点のX座標が入る。
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//頂点数を入れる
	vertexCount_ = pMesh->GetControlPointsCount();

	//頂点の数と同じサイズのVertex構造体型の動的配列を作成
	Vertex* vertexList = new Vertex[vertexCount_];

	//メッシュの中にあるポリゴンの数を調べる
	polygonCount_ = pMesh->GetPolygonCount();

	//メッシュの中にあるインデックスの数を調べる
	indexCount_ = pMesh->GetPolygonVertexCount();

	//二次元配列になっているvertexPosから、今作ったvertexListのメンバ変数posにデータをコピー
	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	for (int i = 0; i < polygonCount_; i++)
	{
		//最初の頂点番号を調べると、ポリゴンの3頂点がわかる
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		
		//３頂点内でループ
		for (int j = 0; j < 3; j++)
		{
			//調べたい頂点番号がわかる
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;

			//i番目のポリゴンの、Jの頂点向きに法線が伸びる
			pMesh->GetPolygonVertexNormal(i, j, Normal);

			//法線情報を頂点情報に入れる
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);
			
			//UVの情報をゲットだ
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			//index番目の頂点のUV情報をuvという変数に入れる
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	//頂点バッファにぶちこめええええええええええええ！！！！
	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	SAFE_DELETE_ARRAY(vertexList);


	//インデックス情報を読み込む前に配列にする
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];

	//マテリアルごとのポリゴン数を動的配列にする
	polygonCountOfMaterial_ = new int[materialCount_];

	//配列をループして、マテリアルごとにインデックス情報を与える
	for (int i = 0; i < materialCount_; i++)
	{
		//インデックス情報を入れる動的配列作成
		int* indexList = new int[indexCount_];

		//インデックス情報導入
		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンが何番目のマテリアルかを確認
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			
			//それがi番目なら
			if (materialID == i)
			{
				//3頂点をインデックス情報に追加
				for (int vertex = 0; vertex < 3; vertex++)
				{
					//第一引数：ポリゴンの番号
					//第二引数：頂点の番号
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		//頂点数を3で割ればポリゴン数になるから、マテリアルごとにポリゴン数を入れていく
		polygonCountOfMaterial_[i] = count / 3;

		//バーテックスバッファにぶちこ(ry
		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);

		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();

		SAFE_DELETE_ARRAY(indexList);
	}
}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	//オブジェクトを変形
	//第一引数：ワールド行列
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//テクスチャを指定
	//Direct3D::pDevice->SetTexture(0, pTexture_);

	//マテリアルを指定
	//Direct3D::pDevice->SetMaterial(&material_);

	//表示したい頂点バッファとインデックスバッファ
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	//Direct3D::pDevice->SetIndices(pIndexBuffer_);

	//頂点データを指定してセット	  論理和（ビット演算子）
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//描画処理をマテリアルごとにループして処理
	for (int i = 0; i < materialCount_; i++)
	{
		//インデックス情報を表示
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);

		//マテリアルを指定
		Direct3D::pDevice->SetMaterial(&pMaterial_[i]);

		//テクスチャを指定
		Direct3D::pDevice->SetTexture(0, ppTexture_[i]);

		//描画処理
		//第4引数：頂点データの数
		//最後の引数：ポリゴンの数（三角形の数）
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);
	}
	
}

void Fbx::RayCast(RayCastData& data, const D3DXMATRIX &matrix)
{
	//最小の数だと更新
	//ここと、最短距離を求める変数には同じ値を入れておこう
	float min = 9999.0f;

	//頂点バッファをロック
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < (unsigned)materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < (unsigned)polygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			//maya通りのためローカル座標
			D3DXVECTOR3 v0, v1, v2;

			v0 = vCopy[iCopy[j * 3 + 0]].pos;
			//ここでワールド座標へと変換
			D3DXVec3TransformCoord(&v0, &v0, &matrix);
			
			v1 = vCopy[iCopy[j * 3 + 1]].pos;
			D3DXVec3TransformCoord(&v1, &v1, &matrix);

			v2 = vCopy[iCopy[j * 3 + 2]].pos;
			D3DXVec3TransformCoord(&v2, &v2, &matrix);

			//ここで、D3DXIntersectTri関数を使う
			//大文字INT等は、ウィンドウズでしか使えない。でも機能は変わらない！
			BOOL hit = D3DXIntersectTri(&v0, &v1, &v2, &data.start, 
				&data.direction, nullptr, nullptr, &data.distance);

			//レイが当たっていて、distanceが最小ならば更新
			if (hit)
			{
				data.hit = true;
				if (min > data.distance)
				{
					min = data.distance;
				}
			}
		}

		//インデックスバッファ使用終了
		ppIndexBuffer_[i]->Unlock();
	}
	//最小の数を再びdistance入れる
	data.distance = min;
}
