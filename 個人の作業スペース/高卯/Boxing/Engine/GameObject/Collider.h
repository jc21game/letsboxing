#pragma once
#include "../Global.h"

class Collider
{
	D3DXVECTOR3 center_;	//自分の中心点
	float		radius_;	//半径
	IGameObject* owner_;	//自分の当たり判定
public:

	//引数付きコンストラクタ
	//引数：owner、自分　center、自分の中心点　radius、自分の半径
	Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	~Collider();

	//当たってたらトゥルーを返す
	//引数：target、相手の位置
	//戻値：bool、真偽
	bool IsHit(Collider& target);
};

