#pragma once
#include <d3dx9.h>
#include <list>
#include <string>

class Collider;

//クラスのプロトタイプ宣言
//戻り値にしたりポインタにしたりならできる
//class Player;

//純粋仮想関数しかないクラスはポインタでしか扱えない
//全部純粋仮想関数のことを、インターフェースという(interface)
//I○○の場合は、純粋仮想関数のこと
class IGameObject
{
private:
	std::string name_;		//文字列を変数として扱えるstring
	bool dead_;				//死んだかどうかを判定する変数
	Collider *pCollider_;	//当たり判定
	IGameObject* pParent_;	//私が親です

protected:
	D3DXVECTOR3 position_;	//位置
	D3DXVECTOR3 rotate_;	//角度
	D3DXVECTOR3 scale_;		//大きさ
	
	std::list<IGameObject*> childList_;	//vectorの可変長配列

	//親と子の動きを独立させるための変数
	D3DXMATRIX localMatrix_;

	//世界から見た行列の変数
	D3DXMATRIX worldMatrix_;
	
	//行列を作成する関数
	//引数：なし
	//戻値：なし
	void Transform();

public:
	//コンストラクタ
	IGameObject();

	//引数ありのコンストラクタ
	//第一引数：parent	親のゲームオブジェクト
	IGameObject(IGameObject* parent);
	
	//引数ありのコンストラクタ
	//第一引数：parent	親のゲームオブジェクト
	//第二引数：name	子供の名前
	IGameObject(IGameObject* parent, const std::string& name);

	//仮想デストラクタ
	virtual ~IGameObject();

	//初期化
	//純粋仮想関数
	virtual void Initialize() = 0;

	//更新
	//純粋仮想関数
	virtual void Update() = 0;

	//描画
	//純粋仮想関数
	virtual void Draw() = 0;

	//解放
	//純粋仮想関数
	virtual void Release() = 0;

	//衝突したときに呼ばれる関数
	//引数：targetObject 衝突相手
	virtual void OnCollision(IGameObject* targetObject) {};

	//自身のUpdate処理を呼んでから、子供のUpdateを呼び出していく
	//引数：なし
	//戻値：なし
	void UpdateSub();

	//自身のDraw処理を呼んでから、子供のDrawを呼び出していく
	//引数：なし
	//戻値：なし
	void DrawSub();

	//自身のRelease処理を呼んでから、子供のReleaseを呼び出していく
	//引数：なし
	//戻値：なし
	void ReleaseSub();
	
	//テンプレート作成
	//テンプレートクラスを使えば、<>内を指定すればなんにでもなれるよ
	template <class T>

	//オブジェクトを作成する
	//引数：parent	親のゲームオブジェクトを指定
	//戻値：T*	オブジェクト本体を返す 
	T* CreateGameObject(IGameObject* parent)
	{
		//テンプレートクラスはインライン関数
		//オブジェクトを動的作成
		T* p = new T(parent);

		//親に子供（自分）を追加する
		parent->PushBackChild(p);

		//オブジェクト本体を初期化する
		p->Initialize();

		return p;
	}

	
	template <class T>
	//ボタンを作成する
	//第一引数：parent	親のゲームオブジェクトを指定
	//第二引数：fileName ボタンのロゴの名前
	//第三引数：originX 原点Xの位置
	//第四引数：originY 原点Yの位置
	//戻値：T*	オブジェクト本体を返す
	T* CreateButtonObject(IGameObject* parent, std::string fileName,
		float originX, float originY)
	{
		//テンプレートクラスはインライン関数
		//オブジェクトを動的作成
		T* p = new T(parent, fileName, originX, originY);

		//親に子供（自分）を追加する
		parent->PushBackChild(p);

		//オブジェクト本体を初期化する
		p->Initialize();

		return p;
	}

	//ベクターに子供を入れる
	//引数：pObj	オブジェクト本体
	//戻値：なし
	void PushBackChild(IGameObject* pObj);

	//自分を〇す
	//引数：なし
	//戻値：なし
	void KillMe();

	//名前でオブジェクトを検索
	//引数：name 検索する名前
	//戻値：IGameObject 見つけたオブジェクトのアドレス
	IGameObject* FindObject(const std::string& name);

	//オブジェクトの名前を取得
	//戻値：名前
	const std::string& GetName() const;

	//親オブジェクトを取得
	//戻値：親オブジェクトのアドレス
	IGameObject* GetParent()
	{
		return pParent_;
	}

	IGameObject* GetChild(int i)
	{
		auto it = childList_.begin();
		return (*it) + i;
	}

	//コライダーを付ける
	//引数：centar、自分の中心点　radius、半径
	//戻値：なし
	void SetCollider(D3DXVECTOR3& centar, float radius);

	//相手との衝突判定
	//引数：targetObject、相手のオブジェクト
	//戻値：なし
	void Collision(IGameObject* targetObject);

	//位置のセッター
	//引数：position、位置の値を受け取る
	//戻値：なし
	void SetPosition(D3DXVECTOR3 position)
	{
		position_ = position;
	}

	//回転のセッター
	//引数：rotate、回転の値を受け取る
	//戻値：なし
	void SetRotate(D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	}

	//拡大縮小のセッター
	//引数：scale、拡大縮小の値を受け取る
	//戻値：なし
	void SetScale(D3DXVECTOR3 scale)
	{
		scale_ = scale;
	}

	//位置のゲッター
	//引数：なし
	//戻値：position_、位置の値を返す
	D3DXVECTOR3 GetPosition()
	{
		return position_;
	}

	//回転のゲッター
	//引数：なし
	//戻値：rotate_、回転の値を返す
	D3DXVECTOR3 GetRotate()
	{
		return rotate_;
	}

	//拡大縮小のゲッター
	//引数：なし
	//戻値：scale_、拡大縮小の値を返す
	D3DXVECTOR3 GetScale()
	{
		return scale_;
	}
};

