#include "SceneManager.h"
#include "../../SplashScene.h"
#include "../../TitleScene.h"
#include "../../MenuScene.h"
#include "../../DifficultyScene.h"
#include "../../PlayScene.h"
#include "../../TrainingScene.h"
#include "../../OptionScene.h"
#include "../../OperationMemoScene.h"
#include "../../RematchScene.h"

//マクロ
#define SET_CURRENT

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_SPLASH;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_SPLASH;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<SplashScene>(this);
}

//更新
void SceneManager::Update()
{
	//今のシーンと次のシーンのIDが違ったら変更
	if (currentSceneID_ != nextSceneID_)
	{
		//子供と孫を全員〇す
 		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		SAFE_DELETE(*scene);

		//リスト自体の箱も残るため、クリアを使う
		childList_.erase(scene);

		//〇したらシーンを切り替える
		switch (nextSceneID_)
		{
		case SCENE_ID_SPLASH: pCurrentScene_ = CreateGameObject<SplashScene>(this); break;
		case SCENE_ID_TITLE: pCurrentScene_ = CreateGameObject<TitleScene>(this); break;
		case SCENE_ID_MENU: pCurrentScene_ = CreateGameObject<MenuScene>(this); break;
		case SCENE_ID_DIFFICULTY: pCurrentScene_ = CreateGameObject<DifficultyScene>(this); break;
		case SCENE_ID_TRAINING: pCurrentScene_ = CreateGameObject<TrainingScene>(this); break;
		case SCENE_ID_OPTION: pCurrentScene_ = CreateGameObject<OptionScene>(this); break;
		case SCENE_ID_OPERATION_MEMO: pCurrentScene_ = CreateGameObject<OperationMemoScene>(this); break;
		case SCENE_ID_REMATCH: pCurrentScene_ = CreateGameObject<RematchScene>(this); break;
		case SCENE_ID_PLAY: pCurrentScene_ = CreateGameObject<PlayScene>(this); break;
		}

		//今のシーンと次のシーンを同じにする
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
