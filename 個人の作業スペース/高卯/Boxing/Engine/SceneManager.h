#pragma once
#include "IGameObject.h"

enum SCENE_ID
{
	SCENE_ID_SPLASH,
	SCENE_ID_TITLE,
	SCENE_ID_MENU,
	SCENE_ID_DIFFICULTY,
	SCENE_ID_OPERATION_MEMO,
	SCENE_ID_PLAY,
};

//Sceneを管理するクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;
	static SCENE_ID nextSceneID_;
	static IGameObject* pCurrentScene_;
	static bool sceneReload_;			//シーンをリロードする際に使う

public:
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	static void ChangeScene(SCENE_ID next);

	//シーンをリロードしたい時に使う関数（trueにする）
	static void ReloadScene();

	static IGameObject* GetCurrentSecne()
	{
		return pCurrentScene_;
	}

	//名前でオブジェクトを検索
	//引数：name 検索する名前
	//戻値：IGameObject 見つけたオブジェクトのアドレス
	IGameObject* FindChildObject(const std::string& name)
	{
		//ここから下にあるやつを探し出す
		return FindGameObject(name);
	}
};