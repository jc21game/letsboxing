#pragma once
#include "Engine/Global.h"
#include <time.h>

//プレイシーンを管理するクラス
class SplashScene : public IGameObject
{
	int hPict_;    //画像番号
	float alphaValue_;	//画像の透明度
	bool alphaValueflag_;

	double startClock_;	//タイマーの計測開始時間

	bool sceneSwitchFlag_;	//シーン切り替えフラグ

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};