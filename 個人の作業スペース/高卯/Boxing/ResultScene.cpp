#include "ResultScene.h"
#include "Engine/GameObject/Image.h"

#define WIN 0
#define LOSE 1

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), hPict_(-1), judgment_(-1)
{
}

//初期化
void ResultScene::Initialize()
{
	//勝ちなら勝ちの画像表示
	if (judgment_ == WIN)
	{
		//画像データのロード
		hPict_ = Image::Load("data/Win.png");
		assert(hPict_ >= 0);
	}
	//負けなら負けの画像表示
	else if (judgment_ == LOSE)
	{
		//画像データのロード
		hPict_ = Image::Load("data/Lose.png");
		assert(hPict_ >= 0);
	}
}

//更新
void ResultScene::Update()
{
	//左クリックで再戦シーンへ変更
	if (Input::IsMouseButtonDown(0))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_REMATCH);
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}