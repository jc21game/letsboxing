#include "Button.h"
#include "Engine/GameObject/Image.h"
#include <string>

//コンストラクタ
Button::Button(IGameObject * parent, std::string fileName,
	float originX, float originY)
	:IGameObject(parent, "Button"), hPict_(-1), sceneChangeflag_(false)
{
	//名前と画像の原点をセット
	fileName_ = fileName;
	originX_ = originX;
	originY_ = originY;
}

//デストラクタ
Button::~Button()
{
}

//初期化
void Button::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load(fileName_);
	assert(hPict_ >= 0);

	//画像の原点変更
	position_ = D3DXVECTOR3(originX_, originY_, 0);
}

//更新
void Button::Update()
{
	//マウスのポジション取得
	D3DXVECTOR3 mousePos = Input::GetMousePosition();

	//左クリックしたか
	if (Input::IsMouseButtonDown(0))
	{
		//ここを変更
		//画像内にクリックしてるか
		if (originX_ < mousePos.x && originX_ + Image::GetImageSizeWidth(hPict_) > mousePos.x &&
			originY_ < mousePos.y && originY_ + Image::GetImageSizeHeight(hPict_) > mousePos.y)
		{
			//シーン切り替えのフラグをオンにする
			sceneChangeflag_ = true;
		}
	}
	//フラグをoffにしないと操作説明シーンで
	//ずっと同じ動作が呼ばれるためマウスホイールを
	//検知してフラグをオフに
	if(Input::GetMouseMove().z)
	{
		sceneChangeflag_ = false;
	}
	//キー操作でも同様にフラグをオフに
	if (Input::IsKey(DIK_UP)|| Input::IsKey(DIK_DOWN))
	{
		sceneChangeflag_ = false;
	}
}

//描画
void Button::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void Button::Release()
{
}