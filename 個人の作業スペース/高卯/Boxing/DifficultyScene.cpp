#include "DifficultyScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

//コンストラクタ
DifficultyScene::DifficultyScene(IGameObject * parent)
	: IGameObject(parent, "DifficultyScene"), hPict_(-1)
{
}

//初期化
void DifficultyScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/DifficultyScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	easyLogo_ = CreateButtonObject<Button>(this, "data/EasyLogo.png", 100.0f, 540.0f);
	normalLogo_ = CreateButtonObject<Button>(this, "data/NormalLogo.png", 700.0f, 540.0f);
	hardLogo_ = CreateButtonObject<Button>(this, "data/hardLogo.png", 1300.0f, 540.0f);
}

//更新
void DifficultyScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool easyLogoFlag = easyLogo_->GetSceneChangeflag();
	bool normalLogoFlag = normalLogo_->GetSceneChangeflag();
	bool hardLogoFlag = hardLogo_->GetSceneChangeflag();

	//受け取ったフラグで処理を変える
	if (easyLogoFlag == true)
	{
		//簡単な敵のプレイシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
	else if (normalLogoFlag == true)
	{
		//普通の敵のプレイシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
	else if (hardLogoFlag == true)
	{
		//難しい敵のプレイシーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	//ESCキーで前の画面に戻る
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void DifficultyScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void DifficultyScene::Release()
{
}