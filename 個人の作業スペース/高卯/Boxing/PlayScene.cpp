#include "PlayScene.h"
#include "Engine/GameObject/Image.h"

#include "RetireScene.h"
#include "ResultScene.h"

#define WIN 0
#define LOSE 1

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), hPict_(-1), pText_(nullptr), sec_(0), tensec_(0), min_(8), frame_(0),ReleaseFlag_(false)
{
}

//初期化
void PlayScene::Initialize()
{
	//数字の字体 サイズ
	pText_ = new Text("ゴシック", 80);

	//画像データのロード
	hPict_ = Image::Load("data/PlayScene.png");
	assert(hPict_ >= 0);

	second_[0] = "0";
	second_[1] = "9";
	second_[2] = "8";
	second_[3] = "7";
	second_[4] = "6";
	second_[5] = "5";
	second_[6] = "4";
	second_[7] = "3";
	second_[8] = "2";
	second_[9] = "1";
	second_[10] = "0";

	minute_[0] = "0";
	minute_[1] = "9";
	minute_[2] = "8";
	minute_[3] = "7";
	minute_[4] = "6";
	minute_[5] = "5";
	minute_[6] = "4";
	minute_[7] = "3";
	minute_[8] = "2";
	minute_[9] = "1";
	minute_[10] = "0";
}

//更新
void PlayScene::Update()
{
	timecnt();

	//ESCキーでリタイア画面
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		CreateGameObject<RetireScene>(this);
	}
	//左クリックで勝ちのリザルト画面
	if (Input::IsKeyDown(DIK_1))
	{
		ResultScene* pResultScene = new ResultScene(this);
		this->PushBackChild(pResultScene);
		pResultScene->Setjudgment(WIN);
		pResultScene->Initialize();
	}
	//右クリックで負けのリザルト画面
	if (Input::IsKeyDown(DIK_2))
	{
		ResultScene* pResultScene = new ResultScene(this);
		this->PushBackChild(pResultScene);
		pResultScene->Setjudgment(LOSE);
		pResultScene->Initialize();
	}
}

//描画
void PlayScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);

	//第一、第二引数は位置情報
	if (!ReleaseFlag_) 
	{
		pText_->Draw(800, 50, minute_[min_] + ":" + second_[tensec_] + second_[sec_]);
	}
}

//開放
void PlayScene::Release()
{
}

void PlayScene::timecnt()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (min_ <= 10)
		{

			//最初3:00から2:59にする
			if (tensec_ == 0 && sec_ == 0)
			{
				tensec_ = 5;
				sec_ = 0;
				min_++;
			}



			//tensec_は10の位のカウント
			if (tensec_ <= 10)
			{
				if (tensec_ == 10 && sec_ == 10)
				{
					tensec_ = 5;
					sec_ = 1;
					min_++;
				}

				//1の位のカウント
				else if (sec_ < 10)
				{
					sec_++;
				}

				else if (sec_ == 10)
				{
					sec_ = 1;
					tensec_++;
				}

			}
			//時間0の処理
			if (min_ == 10 && tensec_ == 10 && sec_ == 10)
			{
				/*sec_ = 0;
				tensec_ = 0;
				min_ = 7;*/
				
				ReleaseFlag_ = true;

				ResultScene* pResultScene = new ResultScene(this);
				this->PushBackChild(pResultScene);
				pResultScene->Setjudgment(LOSE);
				pResultScene->Initialize();
			}

		}
	}
}
