#pragma once
#include "Engine/ObjectTree/IGameObject.h"

//ボタンを管理するクラス
class Button : public IGameObject
{
	int hPict_;				//画像番号
	std::string fileName_;	//ファイルの名前
	float originX_;			//原点X
	float originY_;			//原点Y
	bool sceneChangeflag_;	//シーンを切り替えるフラグ
public:
	//コンストラクタ
	Button(IGameObject* parent, std::string fileName,
		float originX, float originY);

	//デストラクタ
	~Button();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーン切り替えのフラグを返す
	//戻値：bool フラグ
	bool GetSceneChangeflag()
	{
		return sceneChangeflag_;
	};
};