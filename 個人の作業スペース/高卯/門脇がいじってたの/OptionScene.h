#pragma once
#include "Engine/Global.h"

class Button;

//オプションシーンを管理するクラス
class OptionScene : public IGameObject
{
	int hPict_;						//画像番号
	Button* operationMemoLogo_;		//操作説明ロゴ
	Button* menuBackLogo_;			//メニューへ戻るロゴ
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	OptionScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};