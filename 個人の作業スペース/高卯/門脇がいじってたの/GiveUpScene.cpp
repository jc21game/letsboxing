#include "GiveUpScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"
#include "PlayScene.h"

//コンストラクタ
GiveUpScene::GiveUpScene(IGameObject * parent)
	: IGameObject(parent, "GiveUpScene"), hPict_(-1),
	state_(STATE_YES), possible_(false)
{
}

//初期化
void GiveUpScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/GiveUpScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	yesLogo_ = CreateGameObject<PictDisplay>(this, "data/YesLogo.png", "data/SelectYesLogo.png", 80.0f, 440.0f);
	noLogo_ = CreateGameObject<PictDisplay>(this, "data/NoLogo.png", "data/SelectNoLogo.png", 1200.0f, 440.0f);
}

//更新
void GiveUpScene::Update()
{
	//選択されている状態
	switch (state_)
	{
	//YESロゴ選択時
	case STATE_YES:
		//ロゴが黄色くなる
		yesLogo_->Selection();

		//Enterでメニュー画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);
		}
		//右キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_NO;
			yesLogo_->UnSelection();
		}
		//左キーを押したらNOロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_NO;
			yesLogo_->UnSelection();
		}
		break;

	//NOロゴ選択時
	case STATE_NO:
		//ロゴが黄色くなる
		noLogo_->Selection();

		//Enterで試合画面へ移動
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//試合画面を操作可能にしておく
			PlayScene* pPlayScene = (PlayScene*)FindObject("PlayScene");
			pPlayScene->SetNowPlayScene(true);
			yesLogo_->KillMe();
			noLogo_->KillMe();
			KillMe();
		}
		//右キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_RIGHT))
		{
			state_ = STATE_YES;
			noLogo_->UnSelection();
		}
		//左キーを押したらYESロゴにカーソルが移動
		if (Input::IsKeyDown(DIK_LEFT))
		{
			state_ = STATE_YES;
			noLogo_->UnSelection();
		}
		break;
	}

	//Escキーで前の画面（試合画面）へ移動
	if (Input::IsKeyDown(DIK_ESCAPE) && possible_)
	{
		//試合画面を操作可能にしておく
		PlayScene* pPlayScene = (PlayScene*)FindObject("PlayScene");
		pPlayScene->SetNowPlayScene(true);
		yesLogo_->KillMe();
		noLogo_->KillMe();
		KillMe();
	}
	//Escキーの誤操作防止
	possible_ = true;
}

//描画
void GiveUpScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GiveUpScene::Release()
{
}