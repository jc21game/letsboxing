#include "Stage.h"
#include "Engine/Model.h"


//コンストラクタ
//名前と画像の原点をセット
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), hModel_(-1)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//画像データのロード
	hModel_ = Model::Load("data/Stage.fbx");
	assert(hModel_ >= 0);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Stage::Release()
{
}