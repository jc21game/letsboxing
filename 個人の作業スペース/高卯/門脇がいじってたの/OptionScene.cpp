#include "OptionScene.h"
#include "Engine/GameObject/Image.h"
#include "Button.h"

//コンストラクタ
OptionScene::OptionScene(IGameObject * parent)
	: IGameObject(parent, "OptionScene"), hPict_(-1)
{
}

//初期化
void OptionScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/OptionScene.png");
	assert(hPict_ >= 0);

	//ロゴ生成
	operationMemoLogo_ = CreateButtonObject<Button>(this, "data/OperationMemoLogo.png", 650.0f, 300.0f);
	menuBackLogo_ = CreateButtonObject<Button>(this, "data/MenuBackLogo.png", 650.0f, 650.0f);
}

//更新
void OptionScene::Update()
{
	//各フラグの結果をそれぞれに代入
	bool operationMemoLogoFlag = operationMemoLogo_->GetSceneChangeflag();
	bool menuBackLogoFlag = menuBackLogo_->GetSceneChangeflag();

	//受け取ったフラグで処理を変える
	if (operationMemoLogoFlag == true)
	{
		//操作説明シーンへ変更
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_OPERATION_MEMO);
	}
	else if (menuBackLogoFlag == true)
	{
		//メニューシーンに戻る
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}

	//ESCキーで前の画面に戻る
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_TRAINING);
	}
}

//描画
void OptionScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void OptionScene::Release()
{
}