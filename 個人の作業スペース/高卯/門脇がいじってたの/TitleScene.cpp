#include <ctime>
#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/PictDisplay.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), hPict_(-1), sceneStartTime_(NULL), moveY_(1024),
	gameStartFlag_(false)
{
	D3DXMatrixIdentity(&move_);
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/LetsBoxingg.png");
	assert(hPict_ >= 0);

	//初期位置
	D3DXMatrixTranslation(&move_, 0, moveY_, 0);

	//ロゴ作成
	StartLogo_ = CreateGameObject<PictDisplay>(this, "data/StartLogo.png", "", -2000.0f, -2000.0f);

	//シーン切り替え時の時間を取得
	time(&sceneStartTime_);
}

//更新
void TitleScene::Update()
{
	//経過時間
	time_t elapsedTime;
	//今の時間を取得
	time(&elapsedTime);
	//経過時間 - シーン切り替え時の時間 で差を求める
	double difference = difftime(elapsedTime, sceneStartTime_);

	//ロゴの表示位置移動
	if (moveY_ >= 0)
	{
		moveY_ -= 5;
		D3DXMatrixTranslation(&move_, 0, moveY_, 0);
	}

	//画面に「Game Start!」と表示
	if (moveY_ <= 0)
	{
		StartLogo_->SetPosition(D3DXVECTOR3(440.0f, 820.0f, 0));
		//ゲームスタートのフラグをtrue
		gameStartFlag_ = true;
	}

	//30秒たったらスプラッシュシーンに戻る
	if (difference >= 30.0f)
	{
		SceneManager* pSceneManager;
		pSceneManager->ChangeScene(SCENE_ID_SPLASH);
	}

	//Enterを押したら処理
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//フラグがfalseの場合
		if (gameStartFlag_ == false)
		{
			//画像の位置を移動させる
			moveY_ = 0;
		}
		//フラグがtrueの場合
		else if (gameStartFlag_ == true)
		{
			//メニューシーンへ移行
			SceneManager* pSceneManager;
			pSceneManager->ChangeScene(SCENE_ID_MENU);
		}
	}

	//ゲーム終了
	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		PostQuitMessage(0);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_, move_);
	Image::Draw(hPict_);
}

//開放
void TitleScene::Release()
{
}