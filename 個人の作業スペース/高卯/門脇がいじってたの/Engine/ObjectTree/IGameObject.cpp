#include "IGameObject.h"
#include "../Global.h"
#include "../GameObject/Collider.h"


//親なし名前なしのコンストラクタ
IGameObject::IGameObject() : IGameObject(nullptr, "")
{
}

//名前なしのコンストラクタ
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

//他のコンストラクタを、全部こっちのコンストラクタにつなげている
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false), pCollider_(nullptr)
{
}


IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	//子供を追加する関数
	childList_.push_back(pObj);
}


void IGameObject::UpdateSub()
{
	Update();

	//行列変形の関数
	Transform();

	//自分に当たり判定が付いてるときに処理
	if (pCollider_ != nullptr)
	{
		//引数に今のシーンの直下の当たり判定の捜索
		Collision(SceneManager::GetCurrentScene());
	}

 	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		//しぬ時の関数
		if ((*it)->dead_ == true)
		{
			//リリースサブで末代まで消さないとダメ
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);

			//リストの要素を削除して、詰める
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
		
	}
}

void IGameObject::DrawSub()
{
	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::Transform()
{
	//行列
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	//位置
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	
	//回転
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	
	//拡大縮小
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//ローカル行列
	localMatrix_ = matS * matRZ * matRX * matRY * matT;
	
	//ワールド行列
	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

void IGameObject::KillMe()
{
	//死ぬ時の処理をON！
	dead_ = true;
}

IGameObject * IGameObject::FindObject(const std::string & name)
{
	//子供がいないなら終わり
	if (childList_.empty())
	{
		return nullptr;
	}

	//イテレータ
	for(auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetName() == name)
			return (*it);

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindObject(name);
		if (obj != nullptr)
		{
			return obj;
		}
	}

	//見つからなかった
	return nullptr;
}

const std::string& IGameObject::GetName() const
{
	return name_;
}


void IGameObject::SetCollider(D3DXVECTOR3 & centar, float radius)
{
	//コンストラクタでセットする
	pCollider_ = new Collider(this, centar, radius);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	//自分のコライダーと相手のコライダーは当たった時の処理
	if (targetObject != this &&
		targetObject->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetObject->pCollider_))
	{
		//当たった
		this->OnCollision(targetObject);
	}

	//相手のオブジェクトを再帰で呼び出して、総当たりする
	for (auto itr = targetObject->childList_.begin();
		itr != targetObject->childList_.end(); itr++)
	{
		Collision(*itr);
	}
}
