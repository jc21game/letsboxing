#include "Audio.h"

IXACT3Engine* pXactEngine;	//XACTエンジン（本体）
IXACT3WaveBank* pWaveBank;	//ウェーブバンクを入れる
IXACT3SoundBank* pSoundBank;	//サウンドバンクを入れる

//XACTエンジン作成
void Audio::Initialize()
{
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	XACT3CreateEngine(0, &pXactEngine);
	XACT_RUNTIME_PARAMETERS xactParam = { 0 };
	xactParam.lookAheadTime = XACT_ENGINE_LOOKAHEAD_DEFAULT;
	pXactEngine->Initialize(&xactParam);
}

//ウェーブバンクをロード
void Audio::WaveLoad(const char * waveBankName)
{
	//ファイルを開く
	HANDLE hFile = CreateFile(waveBankName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

	//開いたファイルのサイズ
	DWORD fileSize = GetFileSize(hFile, NULL);


	//ファイルをマッピングする
	HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, fileSize, NULL);
	void* mapWaveBank;
	mapWaveBank = MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 0);

	//WAVEバンク作成
	pXactEngine->CreateInMemoryWaveBank(mapWaveBank, fileSize, 0, 0, &pWaveBank);

	//ファイルを閉じる
	CloseHandle(hMapFile);
	CloseHandle(hFile);
}

//サウンドバンクをロード
void Audio::SoundLoad(const char * soundBankName)
{
	//ファイルを開く
	HANDLE hFile = CreateFile(soundBankName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

	//ファイルのサイズを調べる
	DWORD fileSize = GetFileSize(hFile, NULL);

	//ファイルの中身をいったん配列に入れる
	void* soundBankData;
	soundBankData = new BYTE[fileSize];
	DWORD byteRead;
	ReadFile(hFile, soundBankData, fileSize, &byteRead, NULL);

	//サウンドバンク作成
	pXactEngine->CreateSoundBank(soundBankData, fileSize, 0, 0, &pSoundBank);

	//ファイルを閉じる
	CloseHandle(hFile);
}

//再生
void Audio::Play(const char * cueName)
{
	XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName);
	pSoundBank->Play(cueIndex, 0, 0, NULL);
}

//停止
void Audio::Stop(const char * cueName)
{
	XACTINDEX cueIndex = pSoundBank->GetCueIndex(cueName);
	pSoundBank->Stop(cueIndex, XACT_FLAG_CUE_STOP_IMMEDIATE);
}

//開放
void Audio::Release()
{
	pSoundBank->Destroy();
	pWaveBank->Destroy();
	pXactEngine->ShutDown();
	CoUninitialize();
}
