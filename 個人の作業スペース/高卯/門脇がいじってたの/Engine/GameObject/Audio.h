#pragma once
#include <xact3.h>
#include <xact3d3.h>
#include "../Global.h"

namespace Audio
{
	//XACTエンジン作成
	void Initialize();

	//ウェーブバンクをロード
	//引数：waveBankName ウェーブバンクの名前
	void WaveLoad(const char* waveBankName);

	//サウンドバンクをロード
	//引数：soundBankName サウンドバンクの名前
	void SoundLoad(const char* soundBankName);

	//再生
	//引数：cueName BGMを作成したときの名前
	void Play(const char* cueName);

	//停止
	//引数：cueName BGMを作成したときの名前
	void Stop(const char* cueName);

	//開放処理
	void Release();
};