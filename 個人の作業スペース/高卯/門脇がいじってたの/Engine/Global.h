#pragma once
#include <d3dx9.h>
#include <assert.h>
#include "IGameObject.h"
#include "SceneManager.h"
#include "Input.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete [] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}

//違うcppで実装しても値を保持するための構造体
struct Global
{
	//ウィンドウの背景サイズ
	int screenWidth;
	int screenHeight;
};
extern Global g;
