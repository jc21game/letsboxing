#pragma once
#include "../Global.h"

namespace Direct3D
{
	//LP(ロングポインタ)
	//ポインタの型
	//LPSTR...文字列が入るポインタ
	extern LPDIRECT3D9			pD3d;		//Direct3Dオブジェクト
	//このpDeviceはめっちゃ使うから仲良くなろう
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト

	//Direct3Dの初期化
	//引数：hWnd	ウィンドウハンドル
	//戻値：なし
	void Initialize(HWND hWnd);

	//描画を開始する
	//引数：なし
	//戻値：なし
	void BeginDraw();

	//描画を終了する
	//引数：なし
	//戻値：なし
	void EndDraw();

	//開放処理をする
	//引数：なし
	//戻値：なし
	void Release();
};