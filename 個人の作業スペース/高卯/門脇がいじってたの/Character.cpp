#include "Character.h"
#include "Engine/BoxCollider.h"
#include "Engine/SphereCollider.h"
#include "Engine/Collider.h"
#include <time.h>

#define PUNCH_COLLIDER_MOVE 0.45
#define PUNCH_COLLIDER_MOVE_MAX 3
#define PUNCH_DAMAGE 2
#define STRONG_PUNCH_DAMAGE 4 
#define COUNTER_DAMAGE 2

//コンストラクタ
//名前を付けないと名前がCharacterになった状態で下のコンストラクタが呼ばれる
Character::Character(IGameObject * parent)
	:Character(parent, "Character")
{
}

Character::Character(IGameObject * parent, std::string name)
	:IGameObject(parent, name), hp_(0), pp_(0), timeFlag_(false), time_(0), state_(STANDARD), stance_(DOWN), opponent_("")
{
}

//デストラクタ
Character::~Character()
{
}

//初期化
void Character::Initialize()
{

}

//更新
void Character::Update()
{
	if (standardFlag_ == true)
	{
		standardFlag_ = false;
		state_ = STANDARD;
	}
	if (coolTimeFlag_ == true)
	{
		coolTimeFlag_ = false;
		state_ = COOLTIME;
	}

	switch (state_)
	{
	case STANDARD:	//全ての行動ができる

		PunchCommand();
		StrongPunchCommand();
		CounterCommand();
		StanceChangeCommand();
		MoveCommand();
		break;

	case PUNCH:	//パンチ中はパンチが終わるまで他の行動ができない
		Punch();
		break;

	case CHARG:	//強攻撃のチャージ中はほかの行動ができない
		Charg();
		break;
	
	case STRONG_PUNCH:	//強攻撃中は他の行動ができない
		StrongPunch();
		break;

	case COOLTIME:	//攻撃のクールタイム中はカウンターと構えの上下と移動ができる
		CoolTime();
		CounterCommand();
		StanceChangeCommand();
		MoveCommand();
		break;

	case COUNTER:	//カウンター中は他の行動ができない
		Counter();
		break;

	case WEAK:	//カウンターが失敗すると他の行動ができない
		Weak();
		break;
	}

	if (hp_ <= 0)
	{
		KillMe();
	}
}

//描画
void Character::Draw()
{
}

//開放
void Character::Release()
{
}

void Character::OnCollision(IGameObject * pTarget)
{
	//当たったコライダーが対戦相手の持っているコライダーだった場合
	if (pTarget->GetName() == opponent_)
	{
		for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
		{
			//当たっているコライダーの場合
			if ((*itr)->GetHitCollider() != NULL)
			{
				//自分の攻撃コライダーが相手の胴体コライダーに当たった時
				//ここで自分のstate_や相手のstate_変更やダメージの処理などを全て行う
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				{
					if (state_ == PUNCH)
					{
						switch (((Character*)pTarget)->GetState())
						{
						case STANDARD:
							if (((Character*)pTarget)->GetStance() == stance_)
							{
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
							}
							else
							{
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
							}
							break;

						case PUNCH:
							((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
							break;

						case CHARG:
							((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
							((Character*)pTarget)->SetStandardFlag(true);
							break;

						case STRONG_PUNCH:
							((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
							break;

						case COOLTIME:
							if (((Character*)pTarget)->GetStance() == stance_)
							{
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE / 2;
							}
							else
							{
								((Character*)pTarget)->hp_ -= PUNCH_DAMAGE;
							}
							break;

						case COUNTER:
							hp_ -= COUNTER_DAMAGE;
							((Character*)pTarget)->SetStandardFlag(true);
							break;

						case WEAK:
							((Character*)pTarget)->hp_ -= PUNCH_DAMAGE * 2;
							((Character*)pTarget)->SetStandardFlag(true);
							break;
						}
					}

					else if (state_ == STRONG_PUNCH)
					{
						switch (((Character*)pTarget)->GetState())
						{
						case STANDARD:
							if (((Character*)pTarget)->GetStance() == stance_)
							{
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
							}
							else
							{
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							}
							break;

						case PUNCH:
							((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							break;

						case CHARG:
							((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							((Character*)pTarget)->SetStandardFlag(true);
							break;

						case STRONG_PUNCH:
							((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							break;

						case COOLTIME:
							if (((Character*)pTarget)->GetStance() == stance_)
							{
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							}
							else
							{
								((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE / 2;
							}
							break;

						case COUNTER:
							((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE;
							((Character*)pTarget)->SetStandardFlag(true);
							break;

						case WEAK:
							((Character*)pTarget)->hp_ -= STRONG_PUNCH_DAMAGE * 2;
							((Character*)pTarget)->SetStandardFlag(true);
							break;
						}
						//pp0にする
						pp_ = 0;
					}

					coolTimeFlag_ = true;
				}

				//本当は↓で攻撃を受けたときのstate_変更やダメージの処理をしようと思ったが攻撃した側のstate_の処理の問題でできなそう
				////自分の胴体コライダーが相手の攻撃コライダーに当たった時
				//if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				//{
				//}

				//自分の攻撃コライダーが相手の攻撃コライダーに当たった時
				if ((*itr)->GetName() == "punchCollider" && (*itr)->GetHitCollider()->GetName() == "punchCollider")
				{
					//攻撃コライダー同士が当たっている時はお互いのstate_がPUNCHかSTRONG_PUNCHの時だけ
					if (state_ == ((Character*)pTarget)->GetState())
					{
						coolTimeFlag_ = true;
					}
					else if(state_ == STRONG_PUNCH && ((Character*)pTarget)->GetState() == PUNCH)
					{
						((Character*)pTarget)->SetCoolTimeFlag(true);
					}
				}
				//自分の胴体コライダーが相手の胴体コライダーに当たった時
				if ((*itr)->GetName() == "bodyCollider" && (*itr)->GetHitCollider()->GetName() == "bodyCollider")
				{
					//通り抜けないようにする処理
				}
			}
		}
	}
}

void Character::Punch()
{
	//攻撃用コライダーのオブジェクト作成　攻撃の初めに一回だけ処理される
	bool punchColliderCreateFlag_ = true;
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end(); itr++)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			punchColliderCreateFlag_ = false;
		}
	}
	if (punchColliderCreateFlag_)
	{
		//構えの位置によって攻撃の生成位置が変わる
		if (stance_ == UP)
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, 2, 0), 0.5f);
			AddCollider(punchCollider_);
		}
		else
		{
			punchCollider_ = new SphereCollider("punchCollider", D3DXVECTOR3(0, -2, 0), 0.5f);
			AddCollider(punchCollider_);
		}

		//攻撃用コライダーが作られた時にポジション用の変数も攻撃発動初期位置にする
		punchColliderForwardPosition_ = 1.5;
	}

	//攻撃開始
	if (punchColliderForwardPosition_ <= PUNCH_COLLIDER_MOVE_MAX)
	{
		D3DXMATRIX m;
		punchColliderForwardPosition_ += PUNCH_COLLIDER_MOVE;

		D3DXMatrixRotationY(&m, D3DXToRadian(rotate_.y));
		//構えの位置によって攻撃の生成位置が変わる
		if (stance_ == UP)
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, 2, punchColliderForwardPosition_), &m);
		}
		else
		{
			D3DXVec3TransformCoord(&punchColliderPosition_, &D3DXVECTOR3(0, -2, punchColliderForwardPosition_), &m);
		}

		punchCollider_->SetCenter(punchColliderPosition_);
	}
	//攻撃終了
	else
	{
		//強攻撃だった場合ppを0にする
		if (state_ == STRONG_PUNCH)
		{
			pp_ = 0;
		}

		//クールタイム状態に変更
		state_ = COOLTIME;
	}
	
}

void Character::Charg()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1000)
	{
		state_ = STRONG_PUNCH;
		timeFlag_ = false;
	}
}

void Character::StrongPunch()
{
	Punch();
}

void Character::CoolTime()
{
	//攻撃用コライダーの削除
	for (auto itr = ColliderList_.begin(); itr != ColliderList_.end();)
	{
		if ((*itr)->GetName() == "punchCollider")
		{
			itr = ColliderList_.erase(itr);
			continue;
		}
		itr++;
	}

	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 250)
	{
		state_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::Counter()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1500)
	{
		state_ = WEAK;
		timeFlag_ = false;
	}
}

void Character::Weak()
{
	if (timeFlag_ == false)
	{
		time_ = clock();
		timeFlag_ = true;
	}

	if (clock() - time_ >= 1000)
	{
		state_ = STANDARD;
		timeFlag_ = false;
	}
}

void Character::StanceChange(STANCE stance)
{
	stance_ = stance;
}
