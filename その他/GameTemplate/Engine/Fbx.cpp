#include "Fbx.h"
#include "Direct3D.h"



Fbx::Fbx() :pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr),
pMaterial_(nullptr), pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr),
vertexCount_(0), polygonCount_(0), indexCount_(0), materialCount_(0), polygonCountOfMaterial_(nullptr)
{																								

}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);
	//SAFE_RELEASE(pTexture_);
	//newした分だけでリリースしないといけない
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char* imagePass)
{
	pManager_ = FbxManager::Create();
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	pImporter_->Initialize(imagePass);
	pImporter_->Import(pScene_);
	pImporter_->Destroy();
	//カレントディレクトリの取得
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(imagePass, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	FbxNode* rootNode = pScene_->GetRootNode();
	int childCount = rootNode->GetChildCount();

	for (int i = 0; childCount > i; i++)
    {
        //ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
    }

	SetCurrentDirectory(defaultCurrentDir);	//カレントディレクトリに戻す
}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//fbxが読み込めなければここで止まる
	assert(pVertexBuffer_);


	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);
		Direct3D::pDevice->SetMaterial(&pMaterial_[i]); 
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);
	}
}

void Fbx::CheckNode(FbxNode * pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();
		pMaterial_ = new D3DMATERIAL9[materialCount_];
		//
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];

		/////////////////////////////////////////////
		//for (int i = 0; i < materialCount_; i++)	/			
		//{											/
		//	pTexture_[i] = nullptr;					/
		//}											/
		/////////////////////////////////////////////

		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);

		for (int i = 0; i < materialCount_; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;	//拡散反射光
			FbxDouble3 ambient = lambert->Ambient;	//環境光

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;	//不透明度

			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];      //ファイル名
				char ext[_MAX_EXT];         //拡張子
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);
				
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
				D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);

				//ポインタの中身がcdcdcd...の場合は入っていない
				assert(pTexture_ != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh * pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	vertexCount_ = pMesh->GetControlPointsCount();
	Vertex* vertexList = new Vertex[vertexCount_];

	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];			
	}

	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);

	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	SAFE_DELETE_ARRAY(vertexList);

	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	polygonCountOfMaterial_ = new int[materialCount_];
	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		int count = 0;
		//インデックス情報を取得し、マテリアルの数だけインデックスバッファを作る
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンが何個目のマテリアルか確認
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				//今見ている三角形のポリゴンをインデックスリストに追加
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		//countを３で割ればポリゴンの数になる
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList);
	}
}
