#pragma once
#include "Collider.h"


//箱型の当たり判定
class BoxCollider :	public Collider
{
	friend class Collider;

	//判定サイズ（幅、高さ、奥行き）
	D3DXVECTOR3 size_;
	D3DXVECTOR3 m_NormaDirect[3];   // 方向ベクトル
	float m_fLength[3];             // 各軸方向の長さ

public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//引数：size	当たり判定のサイズ（幅、高さ、奥行き）
	BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size);

	D3DXVECTOR3 GetDirect(int elem)// 指定軸番号の方向ベクトルを取得
	{
		return m_NormaDirect[elem];
	}

	float GetLen_W(int elem)          // 指定軸方向の長さを取得
	{
		return m_fLength[elem];
	}

	void SetRotateRelation(D3DXVECTOR3 rotate) override
	{
		D3DXVECTOR3 rotateSum;
		
		rotateSum = rotate_ + rotate;

		D3DXMATRIX mat;
		D3DXMatrixIdentity(&mat);

		D3DXMATRIX mX;
		D3DXMatrixRotationX(&mX, D3DXToRadian(rotateSum.x));
		D3DXMATRIX mY;
		D3DXMatrixRotationY(&mY, D3DXToRadian(rotateSum.y));
		D3DXMATRIX mZ;
		D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotateSum.z));
		
		mat = mX * mY * mZ;

		m_NormaDirect[0] = D3DXVECTOR3(1, 0, 0);
		m_NormaDirect[1] = D3DXVECTOR3(0, 1, 0);
		m_NormaDirect[2] = D3DXVECTOR3(0, 0, 1);

		D3DXVec3TransformCoord(&m_NormaDirect[0], &m_NormaDirect[0], &mat);
		D3DXVec3TransformCoord(&m_NormaDirect[1], &m_NormaDirect[1], &mat);
		D3DXVec3TransformCoord(&m_NormaDirect[2], &m_NormaDirect[2], &mat);

		for (int i = 0; i < 3; i++)
		{
			D3DXVec3Normalize(&m_NormaDirect[i], &m_NormaDirect[i]);
		}
	}

	void SetRotate(D3DXVECTOR3 rotate)
	{
		rotate_ = rotate;
	}

private:
	//接触判定
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;

};

