#include <string>
#include "Button.h"
#include "Image.h"


//コンストラクタ
//名前と画像の原点をセット
//reactionFlagは基本true
Button::Button(IGameObject * parent, std::string fileName, std::string selectFileName,
	float originX, float originY)
	:IGameObject(parent, "Button"),display_(NORMAL), fileName_(fileName), selectFileName_(selectFileName),
	originX_(originX), originY_(originY), sceneChangeFlag_(false), selectFlag_(false),
	reactionFlag_(true)
{
	//画像データのコンストラクタ
	for (int i = 0; i < MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//デストラクタ
Button::~Button()
{
}

//初期化
void Button::Initialize()
{
	//画像データのロード
	hPict_[NORMAL] = Image::Load(fileName_);
	assert(hPict_[NORMAL] >= 0);

	//選択されてる時の画像データのロード
	hPict_[SELECT] = Image::Load(selectFileName_);
	assert(hPict_[SELECT] >= 0);

	//画像の原点変更
	position_ = D3DXVECTOR3(originX_, originY_, 0);
}

//更新
void Button::Update()
{
	//選択されている状態なら
	if (selectFlag_ == true)
	{
		//画像差し替え
		display_ = SELECT;

		//その時にEnterを押したら
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//シーン切り替えフラグを渡す
			sceneChangeFlag_ = true;
		}
	}
	//選択されていない状態なら
	else
	{
		//画像差し替え
		display_ = NORMAL;
	}
}

//描画
void Button::Draw()
{
	//display_の状態により変化
	Image::SetMatrix(hPict_[display_], worldMatrix_);
	Image::Draw(hPict_[display_]);
}

//開放
void Button::Release()
{
}