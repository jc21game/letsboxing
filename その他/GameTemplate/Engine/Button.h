#pragma once
#include "../Engine/IGameObject.h"

//ボタンを管理するクラス
class Button : public IGameObject
{
	enum STATE {
		NORMAL,
		SELECT,
		MAX
	};

	int hPict_[MAX];				//画像番号
	int display_;					//画像表示用
	std::string fileName_;			//ファイルの名前
	std::string selectFileName_;	//選択されてる時の名前
	float originX_;					//原点X
	float originY_;					//原点Y
	bool sceneChangeFlag_;			//シーンを切り替えるフラグ
	bool selectFlag_;				//選択されてる時のフラグ
	bool reactionFlag_;				//押したら反応するかフラグ（基本は使わないのでtrue）

public:
	//コンストラクタ
	Button(IGameObject* parent, std::string fileName, std::string selectFileName,
		float originX, float originY);

	//デストラクタ
	~Button();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//シーン切り替えのフラグを返す
	//戻値：bool フラグ
	bool GetSceneChangeFlag()
	{
		return sceneChangeFlag_;
	};

	//シーン切り替えのフラグをセットする
	//引数：sceneChangeFlag フラグを受け取る
	void SetSceneChangeFlag(bool sceneChangeFlag)
	{
		sceneChangeFlag_ = sceneChangeFlag;
	};

	//選択されるときのセット関数
	//引数：selectFlag フラグを受け取る
	void SetSelectFlag(bool selectFlag)
	{
		selectFlag_ = selectFlag;
	}

	//ボタンが反応したくない時に使うセット関数
	//引数：reactionFlag フラグを受け取る
	void SetReactionFlag(bool reactionFlag)
	{
		reactionFlag_ = reactionFlag;
	}
};