#include "Enemy.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy")
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	collision = new BoxCollider(D3DXVECTOR3(-5, 0, 0), D3DXVECTOR3(5, 1, 1));
	AddCollider(collision);

	BoxCollider* collision2 = new BoxCollider(D3DXVECTOR3(-3, 0, 0), D3DXVECTOR3(5, 1, 1));
	AddCollider(collision2);

	//SphereCollider* collision = new SphereCollider(D3DXVECTOR3(-5, 0, 0), 5.0f);
	//AddCollider(collision);

	int a = 1;
}

//更新
void Enemy::Update()
{
	rotate_ = D3DXVECTOR3(0, 50, 0);
	collision->SetRotate(D3DXVECTOR3(0, 50, 0));
	//collision->SetRotate(D3DXVECTOR3(0, 50, 0));
}

//描画
void Enemy::Draw()
{
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
}