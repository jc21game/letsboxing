#include "Player.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player")
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(3, 0, 0), D3DXVECTOR3(2, 1, 1));
	AddCollider(collision);

	//rotate_ = D3DXVECTOR3(0, 90, 0);
}

//更新
void Player::Update()
{
	//static D3DXVECTOR3 position = D3DXVECTOR3(3, 0, 0);

	if (Input::IsKey(DIK_LEFT))
	{
		position_.x -= 0.03;
		//(*_colliderList.begin())->SetCenter(position);
	}
	if (Input::IsKey(DIK_RIGHT))
	{
		position_.x += 0.03;
		//(*_colliderList.begin())->SetCenter(position);
	}
	if (Input::IsKey(DIK_UP))
	{
		position_.z += 0.03;
		//(*_colliderList.begin())->SetCenter(position);
	}
	if (Input::IsKey(DIK_DOWN))
	{
		position_.z -= 0.03;
		//(*_colliderList.begin())->SetCenter(position);
	}
}

//描画
void Player::Draw()
{
}

//開放
void Player::Release()
{
}

//何かに当たった
void Player::OnCollision(IGameObject * pTarget)
{
	//当たったときの処理
	//if(pHitCollider_ == pTarget->GetColliderList(0))
	//{ 
	//	auto a = pTarget->GetName();
	//	KillMe();
	//}

	if (GetColliderList(0)->GetHitCollider() == pTarget->GetColliderList(1))
	{
		KillMe();
	}
}