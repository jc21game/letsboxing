#pragma once
#include "Engine/global.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"

class Camera;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int hModel_;
	Camera* pCamera_;
	float camPosY_, camTarY_;

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //衝突
  void OnCollision(IGameObject *pTarget) override;
};