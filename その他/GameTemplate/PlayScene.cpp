#include "PlayScene.h"
#include "Player.h"
#include "Enemy.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")	//親のコンストラクタを引数ありで呼ぶ
	, pCamera_(nullptr), camPosY_(0), camTarY_(0), hModel_(-1)

{
}

//初期化
void PlayScene::Initialize()
{
	//BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(2, 1, 1));
	//AddCollider(collision);


	CreateGameObject<Player>(this);

	CreateGameObject<Enemy>(this);
}

//更新
void PlayScene::Update()
{
	//static D3DXVECTOR3 rotate = D3DXVECTOR3(0, 0, 0);
	//
	//if (Input::IsKey(DIK_LEFT))
	//{
	//	rotate.y += 0.1;
	//	((BoxCollider*)(*_colliderList.begin()))->SetRotate(rotate);
	//}
	

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

void PlayScene::OnCollision(IGameObject * pTarget)
{
}
